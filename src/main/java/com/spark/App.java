package com.spark;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;

import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;

import com.spark.Transform_Report.Fact_location_temp_report_load;
import com.spark.Transform_Report.Fact_user_temp_report_load;
import com.spark.Transform_Report.calculate_and_transform_sales_basic_table;
import com.spark.Upsell_Tracker_Report_Load.AllInOne;
import com.spark.Upsell_Tracker_Report_Load.ArrivalDataViewDailyTransformation;
import com.spark.Upsell_Tracker_Report_Load.ArrivalDataViewNonDailyTransformation;
import com.spark.Upsell_Tracker_Report_Load.TransformLocationMetricsFromLocationSalesTable;
import com.spark.Upsell_Tracker_Report_Load.TransformSalesToLocationSalesTable;
import com.spark.Upsell_Tracker_Report_Load.TransformTempTablesToMainTable;
import com.spark.Upsell_Tracker_Report_Load.TransformUpsellTrackerSalesTableMain;
import com.spark.Upsell_Tracker_Report_Load.TransformUpsellTrackerSalesTableTemp1;
import com.spark.Upsell_Tracker_Report_Load.TransformUpsellTrackerSalesTableTemp2;
import com.spark.Upsell_Tracker_Report_Load.TransformUpsellTrackerSalesTableTemp3;
import com.spark.Upsell_Tracker_Report_Load.TransformUpsellTrackerTable;
import com.spark.Upsell_Tracker_Report_Load.UpdateLocationMetricsFromLocationProfileTable;
import com.spark.Upsell_Tracker_Report_Load.Upsell_tracker_location_metrics_view_transformation;
import com.spark.updateProcedure.UpdateStoredProcedureInformation;

/**
 * Hello world!
 */
/**
 * @author PTG
 *
 */
public class App {
	public static void main(String[] args) throws AnalysisException {

		sqlConnection conn = new sqlConnection();
		KafkaReceiver receiver = new KafkaReceiver(conn);
		receiver.start();
//		AllInOne one=new AllInOne(conn);
//		one.start(1, LocalDate.now(), LocalDate.now());
//		UpdateStoredProcedureInformationAndIntervals(conn);
//
//		transform_report(conn);
		System.out.println("completed" + LocalDateTime.now());
	}

	private static void UpdateStoredProcedureInformationAndIntervals(sqlConnection conn) throws AnalysisException {
		UpdateStoredProcedureInformation storedProcedure = new UpdateStoredProcedureInformation(conn);
		storedProcedure.startPoint();
//		storedProcedure.WriteParqute();
	}

	private static void transform_report(sqlConnection conn) throws AnalysisException {
		Fact_location_temp_report_load Fact_location_temp = new Fact_location_temp_report_load();
		Fact_location_temp.fact_location_temp(1);
		Fact_user_temp_report_load Fact_user_temp = new Fact_user_temp_report_load();
		Fact_user_temp.start(1);
		upsell_tracker_report_load(conn);
		calculate_and_transform_sales_basic_table calAndTransformST = new calculate_and_transform_sales_basic_table();
		calAndTransformST.start(1);

	}

	private static void upsell_tracker_report_load(sqlConnection conn) throws AnalysisException {

		TransformUpsellTrackerTable upsellTracker = new TransformUpsellTrackerTable(conn);
		upsellTracker.upsell_tracker_table();
		Upsell_tracker_location_metrics_view_transformation locationMetricViewtrans = new Upsell_tracker_location_metrics_view_transformation(
				conn);
		locationMetricViewtrans.start();
		ArrivalDataViewDailyTransformation dailyTransformation = new ArrivalDataViewDailyTransformation(conn);
		dailyTransformation.start();
		ArrivalDataViewNonDailyTransformation nonDailyTransformation = new ArrivalDataViewNonDailyTransformation(conn);
		nonDailyTransformation.start();
		TransformUpsellTrackerSalesTableTemp3 salesTemp3 = new TransformUpsellTrackerSalesTableTemp3(conn);
		salesTemp3.transform_upsell_tracker_sales_table_temp3();
		TransformUpsellTrackerSalesTableTemp1 salesTemp1 = new TransformUpsellTrackerSalesTableTemp1(conn);
		salesTemp1.transform_upsell_tracker_sales_table_temp1();
		TransformUpsellTrackerSalesTableTemp2 salesTemp2 = new TransformUpsellTrackerSalesTableTemp2(conn);
		salesTemp2.start(Calendar.getInstance().get(Calendar.YEAR));
		TransformUpsellTrackerSalesTableMain salesMainTable = new TransformUpsellTrackerSalesTableMain(conn);
		salesMainTable.start();

		TransformTempTablesToMainTable mainTable = new TransformTempTablesToMainTable(conn);
		mainTable.start(1);
		TransformSalesToLocationSalesTable locationSales = new TransformSalesToLocationSalesTable(conn);
		locationSales.start();
		TransformLocationMetricsFromLocationSalesTable transformLMFromLST = new TransformLocationMetricsFromLocationSalesTable(
				conn);
		transformLMFromLST.start();
		UpdateLocationMetricsFromLocationProfileTable updateLMFromLPT = new UpdateLocationMetricsFromLocationProfileTable(
				conn);
		updateLMFromLPT.start();
	}

}
