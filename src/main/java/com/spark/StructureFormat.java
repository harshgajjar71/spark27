package com.spark;

import java.io.Serializable;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

public class StructureFormat implements Serializable {

	public StructType productMetricStruture() {
		StructType struct = new StructType()

				.add("ID", DataTypes.IntegerType).add("USER_ID", DataTypes.IntegerType)
				.add("LOCATION_GROUP_ID", DataTypes.IntegerType).add("TENANT_LOCATION_ID", DataTypes.IntegerType)
				.add("PRODUCT_ID", DataTypes.IntegerType).add("ACTIVE_STATUS", DataTypes.ShortType)
				.add("CREATED_BY", DataTypes.IntegerType).add("UPDATED_BY", DataTypes.IntegerType)
				.add("CREATED_ON", DataTypes.LongType).add("UPDATED_ON", DataTypes.LongType)
				.add("businessDate", DataTypes.IntegerType).add("arrivalDate", DataTypes.IntegerType)
				.add("departureDate", DataTypes.IntegerType).add("auditstatus", DataTypes.ShortType)
				.add("parentProductId", DataTypes.LongType).add("employeeID", DataTypes.StringType)

				.add("isError", DataTypes.ShortType).add("errorText", DataTypes.StringType)
				.add("errorCodes", DataTypes.StringType).add("errProductID", DataTypes.StringType)
				.add("errDepartmentID", DataTypes.StringType).add("errProductCategoryID", DataTypes.StringType)
				.add("productCategory", DataTypes.StringType).add("confirmationNumber", DataTypes.StringType)
				.add("marketSegment", DataTypes.StringType).add("roomNights", DataTypes.IntegerType)
				.add("finalRoomNights", DataTypes.DoubleType).add("upsellCharge", DataTypes.DoubleType)
				.add("totalUpsellAmount", DataTypes.DoubleType).add("finalUpsellCharge", DataTypes.DoubleType)
				.add("originalRate", DataTypes.DoubleType).add("newRate", DataTypes.DoubleType)

				.add("originalRoomType", DataTypes.StringType).add("newRoomType", DataTypes.StringType)
				.add("metricDate", DataTypes.IntegerType).add("memo", DataTypes.StringType)
				.add("reviewerID", DataTypes.LongType).add("reviewedDateTime", DataTypes.LongType)
				.add("roomNumber", DataTypes.StringType).add("metricImportType", DataTypes.ShortType)
				.add("column1", DataTypes.StringType).add("column2", DataTypes.StringType)
				.add("column3", DataTypes.StringType).add("column4", DataTypes.StringType)
				.add("column5", DataTypes.StringType).add("column6", DataTypes.StringType)
				.add("column7", DataTypes.StringType).add("column8", DataTypes.StringType)
				.add("column9", DataTypes.StringType).add("column10", DataTypes.StringType)
				.add("migrationHelper", DataTypes.StringType).add("purgeDate", DataTypes.LongType)
				.add("hidePurgedData", DataTypes.ShortType).add("callOrders", DataTypes.IntegerType)
				.add("ticketsSold", DataTypes.IntegerType)

				.add("marketTier", DataTypes.StringType).add("agentDesk", DataTypes.StringType)
				.add("srpCode", DataTypes.StringType).add("upsellFlag", DataTypes.IntegerType)
				.add("roomType", DataTypes.StringType).add("roomTypeCode", DataTypes.StringType)
				.add("incentiveDiscount", DataTypes.StringType).add("incentiveDiscountValue", DataTypes.StringType)
				.add("commissionableRevenue", DataTypes.DoubleType).add("incentiveUpdatedBy", DataTypes.StringType)
				.add("registerNumber", DataTypes.StringType).add("registertType", DataTypes.StringType)
				.add("tranType", DataTypes.StringType).add("redemptionPLU", DataTypes.StringType)
				.add("reprintPLU", DataTypes.StringType).add("resellerVoucherPLU", DataTypes.StringType)
				.add("ezPayPrice", DataTypes.DoubleType).add("itemCode", DataTypes.StringType)
				.add("itemName", DataTypes.StringType).add("upsellReservations", DataTypes.DoubleType)
				.add("innCode", DataTypes.StringType).add("hotelName", DataTypes.StringType)
				.add("actionCode", DataTypes.StringType)

				.add("walkInReservation", DataTypes.StringType).add("mealPeriod", DataTypes.StringType)
				.add("checkNo", DataTypes.IntegerType).add("openTime", DataTypes.LongType)
				.add("closeTime", DataTypes.LongType).add("duration", DataTypes.DoubleType)
				.add("checkCount", DataTypes.IntegerType).add("isRoomService", DataTypes.ShortType)
				.add("json_data", DataTypes.StringType).add("OLD_ID", DataTypes.IntegerType)
				.add("INDUSTRY_ID", DataTypes.IntegerType).add("isRecordChanged", DataTypes.ShortType)
				.add("invoiceId", DataTypes.StringType).add("invoiceType", DataTypes.StringType)
				.add("rentalWalkinFlg", DataTypes.StringType).add("agentIdDesk", DataTypes.StringType)
				.add("realCarCategory", DataTypes.StringType).add("marketSegment1", DataTypes.StringType)
				.add("marketSegment2", DataTypes.StringType).add("customerTierDesc", DataTypes.StringType)
				.add("transactionStartTime", DataTypes.LongType).add("transactionEndTime", DataTypes.LongType)
				.add("originalBookingChannel", DataTypes.StringType)

				.add("kmsInOut", DataTypes.StringType).add("marketSegmentCode", DataTypes.StringType)
				.add("isAutoCorrectRecords", DataTypes.ShortType).add("isManuallyAddedRecord", DataTypes.ShortType)
				.add("isReconciled", DataTypes.ShortType).add("uniqueId", DataTypes.StringType)
				.add("denyReason", DataTypes.StringType).add("checkType", DataTypes.StringType);
		return struct;
	}

	public StructType locationMetricStruture() {
		StructType struct = new StructType()

				.add("ID", DataTypes.IntegerType).add("TENANT_LOCATION_ID", DataTypes.IntegerType)
				.add("metricDate", DataTypes.IntegerType).add("inHouseRevenue", DataTypes.DoubleType)
				.add("roomsAvailable", DataTypes.IntegerType).add("roomsOccupied", DataTypes.IntegerType)
				.add("arrivals", DataTypes.IntegerType).add("upsellMainRevenue", DataTypes.DoubleType)
				.add("upsells", DataTypes.LongType).add("upsellNights", DataTypes.LongType)
				.add("otherRevenue", DataTypes.DoubleType).add("memo", DataTypes.StringType)
				.add("dailyRoomUpsellRevenue", DataTypes.DoubleType).add("dailyOtherRevenue", DataTypes.DoubleType)
				.add("dailyRoomUpsells", DataTypes.LongType).add("departureRoomUpsellRevenue", DataTypes.DoubleType)
				.add("departureOtherRevenue", DataTypes.DoubleType).add("departureRoomUpsells", DataTypes.LongType)
				.add("departureRoomUpsellNights", DataTypes.LongType).add("totalTime", DataTypes.StringType)
				.add("timeOnCalls", DataTypes.IntegerType).add("metricImportType", DataTypes.IntegerType)
				.add("column1", DataTypes.StringType).add("column2", DataTypes.StringType)
				.add("column3", DataTypes.StringType).add("column4", DataTypes.StringType)
				.add("column5", DataTypes.StringType).add("column6", DataTypes.StringType)
				.add("column7", DataTypes.StringType).add("column8", DataTypes.StringType)
				.add("column9", DataTypes.StringType).add("column10", DataTypes.StringType)

				.add("afterCallWorkTime", DataTypes.StringType).add("ticketsSold", DataTypes.IntegerType)
				.add("guests", DataTypes.IntegerType).add("departures", DataTypes.IntegerType)
				.add("mmsqqUnit", DataTypes.IntegerType).add("mmsqq", DataTypes.DoubleType)
				.add("dcoSwimUnit", DataTypes.IntegerType).add("dcoSwim", DataTypes.DoubleType)
				.add("dcoNonSwimUnit", DataTypes.IntegerType).add("dcoNonSwim", DataTypes.DoubleType)
				.add("reservedDiningUnit", DataTypes.IntegerType).add("reservedDining", DataTypes.DoubleType)
				.add("toursUnit", DataTypes.IntegerType).add("tours", DataTypes.DoubleType)
				.add("quickQueueUnit", DataTypes.IntegerType).add("quickQueue", DataTypes.DoubleType)
				.add("otherVIPAccessUnit", DataTypes.IntegerType).add("otherVIPAccess", DataTypes.DoubleType)
				.add("totalAncillary", DataTypes.DoubleType).add("upselFlag", DataTypes.IntegerType)
				.add("reservations", DataTypes.IntegerType).add("admissionUnits", DataTypes.IntegerType)
				.add("ancillaryUnits", DataTypes.IntegerType).add("ACTIVE_STATUS", DataTypes.IntegerType)
				.add("CREATED_BY", DataTypes.IntegerType).add("UPDATED_BY", DataTypes.IntegerType)
				.add("CREATED_ON", DataTypes.LongType).add("UPDATED_ON", DataTypes.LongType)
				.add("json_data", DataTypes.StringType).add("OLD_ID", DataTypes.IntegerType)
				.add("INDUSTRY_ID", DataTypes.IntegerType).add("duration", DataTypes.DoubleType)
				.add("checkCount", DataTypes.IntegerType).add("covers", DataTypes.IntegerType)
				.add("isUpdateFromPM", DataTypes.ByteType).add("avgCount", DataTypes.IntegerType)
				.add("groupMediaType", DataTypes.StringType).add("callYear", DataTypes.IntegerType)
				.add("offeredCount", DataTypes.IntegerType).add("handleCount", DataTypes.IntegerType)
				.add("abandonCount", DataTypes.IntegerType).add("queueWaitTime", DataTypes.DoubleType)
				.add("talkSecs", DataTypes.DoubleType).add("callDurationSec", DataTypes.DoubleType)
				.add("avgAnswerDurationSec", DataTypes.DoubleType).add("asa", DataTypes.DoubleType)
				.add("property", DataTypes.StringType).add("isManuallyAddedRecord", DataTypes.ShortType)
				.add("transientRoomsOccupied", DataTypes.IntegerType).add("groupRoomsOccupied", DataTypes.IntegerType)
				.add("permanentRoomsOccupied", DataTypes.IntegerType);
		return struct;
	}

	public StructType agentMetricStruture() {
		StructType struct = new StructType()

				.add("ID", DataTypes.IntegerType).add("TENANT_LOCATION_ID", DataTypes.IntegerType)
				.add("USER_ID", DataTypes.IntegerType).add("ACTIVE_STATUS", DataTypes.ShortType)
				.add("auditstatus", DataTypes.ShortType).add("employeeID", DataTypes.StringType)
				.add("isError", DataTypes.ShortType).add("errorText", DataTypes.StringType)
				.add("errorCodes", DataTypes.StringType).add("metricDate", DataTypes.IntegerType)
				.add("arrivals", DataTypes.IntegerType).add("checkedInNights", DataTypes.IntegerType)
				.add("upsellMainRevenue", DataTypes.DoubleType).add("otherRevenue", DataTypes.DoubleType)
				.add("memo", DataTypes.StringType).add("callTime", DataTypes.DoubleType)
				.add("callHoldTime", DataTypes.IntegerType).add("afterCallWorkTime", DataTypes.IntegerType)
				.add("reviewerID", DataTypes.IntegerType).add("reviewedDateTime", DataTypes.LongType)
				.add("metricImportType", DataTypes.IntegerType).add("column1", DataTypes.StringType)
				.add("column2", DataTypes.StringType).add("column3", DataTypes.StringType)
				.add("column4", DataTypes.StringType).add("column5", DataTypes.StringType)
				.add("column6", DataTypes.StringType).add("column7", DataTypes.StringType)
				.add("column8", DataTypes.StringType).add("column9", DataTypes.StringType)
				.add("column10", DataTypes.StringType).add("CREATED_BY", DataTypes.IntegerType)
				.add("UPDATED_BY", DataTypes.IntegerType).add("CREATED_ON", DataTypes.LongType)
				.add("UPDATED_ON", DataTypes.LongType).add("transfersToIVR", DataTypes.DoubleType)
				.add("transfersToHGV", DataTypes.DoubleType).add("transfersToHTS", DataTypes.DoubleType)
				.add("adrTarget", DataTypes.DoubleType).add("cnvTarget", DataTypes.DoubleType)
				.add("revenueAll", DataTypes.DoubleType).add("json_data", DataTypes.StringType)
				.add("OLD_ID", DataTypes.IntegerType).add("INDUSTRY_ID", DataTypes.IntegerType)
				.add("purgedDate", DataTypes.IntegerType).add("hidePurgedData", DataTypes.ShortType)
				.add("callYear", DataTypes.IntegerType).add("groupMediaType", DataTypes.StringType)
				.add("property", DataTypes.StringType).add("surveyDate", DataTypes.IntegerType)
				.add("vocScore", DataTypes.DoubleType).add("topBox", DataTypes.DoubleType)
				.add("vocScoreDen", DataTypes.DoubleType).add("sumVocScore", DataTypes.DoubleType)
				.add("isManuallyAddedRecord", DataTypes.ShortType).add("allSegmentRevenue", DataTypes.DoubleType)
				.add("denyReason", DataTypes.StringType);

		return struct;
	}


	public StructType custom() {
		StructType struct = new StructType().add("ID", DataTypes.IntegerType).add("question", DataTypes.StringType)
				.add("auditstatus", DataTypes.IntegerType).add("tenant_location_id", DataTypes.IntegerType);

		return struct;
	}
}