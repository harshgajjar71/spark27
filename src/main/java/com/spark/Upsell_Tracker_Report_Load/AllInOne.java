package com.spark.Upsell_Tracker_Report_Load;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class AllInOne implements Serializable {
	sqlConnection conn = new sqlConnection();
	SparkSession sparkSession = conn.getSpark();

	final static Logger logger = Logger.getLogger(AllInOne.class);

	public void start(Dataset<Row> productMetricList) {
		logger.warn("-------------------------------AllInOne started ----------------------");
		System.out.println("start time: " + LocalTime.now());
		Dataset<Row> lgproduct = conn.dbSession("location_group_product").persist(StorageLevel.MEMORY_AND_DISK_SER());

		Dataset<Row> tl = conn.dbSession("tenant_location").persist(StorageLevel.MEMORY_AND_DISK_SER());

		Dataset<Row> lgp = conn.dbSession("location_group_profile").persist(StorageLevel.MEMORY_AND_DISK_SER());
		Dataset<Row> lg = conn.dbSession("location_group").persist(StorageLevel.MEMORY_AND_DISK_SER());
		Dataset<Row> tpg = conn.dbSession("tenant_product_group").persist(StorageLevel.MEMORY_AND_DISK_SER());
		Dataset<Row> marketSeg = conn.dbSession("market_segment_code").persist(StorageLevel.MEMORY_AND_DISK_SER());

		Dataset<Row> stored_procedure_info = conn.dbSession("stored_procedure_info");

		Dataset<Row> lm = conn.dbSession("location_metric").persist(StorageLevel.MEMORY_ONLY_SER());
//		lm.createOrReplaceTempView("location_metric");
		Dataset<Row> date_intervals = conn.dbSession("date_intervals").persist(StorageLevel.MEMORY_ONLY_SER());

		
		productMetricList.collectAsList().forEach(row -> {
			Integer tenantLocationId = row.getInt(row.fieldIndex("tenant_location_id"));
			LocalDate minDate = LocalDate.ofEpochDay(Long.valueOf(row.getInt(row.fieldIndex("minDate"))));
			LocalDate maxDate = LocalDate.ofEpochDay(Long.valueOf(row.getInt(row.fieldIndex("maxDate"))));
			System.out.println("Tenant-location-id: " + tenantLocationId);
			System.out.println("minDate: " + minDate);
			System.out.println("maxDate: " + maxDate);
			String query = "select * from product_metric where tenant_location_id = " + tenantLocationId.toString()
					+ " and auditstatus not in (2,4,5)" + " and " + "(arrivalDate between '" + minDate.toString()
					+ "' and '" + maxDate.toString() + "' " + "	OR departureDate between '" + minDate.toString()
					+ "' and '" + maxDate.toString() + "' " + "	OR businessDate between '" + minDate.toString()
					+ "' and '" + maxDate.toString() + "' )";

			Dataset<Row> pm = conn.kafkaSession(query).persist(StorageLevel.MEMORY_AND_DISK_SER());
			System.out.println(" count" + pm.count());
			Dataset<Row> upsell_tracker_temp = TransformUpsellTrackerTable(tenantLocationId, minDate, maxDate, pm, row,
					lgproduct, tl, lgp, lg, tpg, marketSeg);

		});

		System.out.println("end time: " + LocalTime.now());
		logger.warn("-------------------------------AllInOne ended----------------------");
	}

	private Dataset<Row> TransformUpsellTrackerTable(Integer tenantLocationId, LocalDate minDate, LocalDate maxDate,
			Dataset<Row> pm, Row row, Dataset<Row> lgproduct, Dataset<Row> tl, Dataset<Row> lgp, Dataset<Row> lg,
			Dataset<Row> tpg, Dataset<Row> marketSeg) {
		logger.warn("-------------------------------Transform Upsell tracker table started ----------------------");

		Column ezPayPrice = pm.col("ezPayPrice");
		Column upsellCharge = pm.col("upsellCharge");

		Column numberOfUpsellNights = functions.when(lgproduct.col("isMajor").equalTo(0), 0)
				.when(lgproduct.col("isMajor").equalTo(1).and(pm.col("roomNights").isNull()), 1)
				.when(pm.col("industry_id").equalTo(5), 1).otherwise(pm.col("roomNights")).as("numberOfUpsellNights");
		Column ezPayColumnNull = functions.when(ezPayPrice.$less(0), ezPayPrice.$minus(1)).otherwise(0);
		Column upSellChargeNull = functions.when(upsellCharge.$less(0), upsellCharge.$minus(1)).otherwise(0);

		Column upsellChargePerNight = functions.when(lgproduct.col("isMajor").equalTo(0), 0)
				.when(lgproduct.col("isMajor").equalTo(1).and(pm.col("upsellCharge").isNull()), 0)
				.when(pm.col("industry_id").equalTo(5),
						functions
								.greatest(
										functions.when(functions.isnull(upsellCharge),
												functions.when(functions.isnull(ezPayPrice), 0)
														.otherwise(ezPayColumnNull)),
										functions.when(functions.isnull(ezPayPrice),
												functions.when(functions.isnull(upsellCharge), 0)
														.otherwise(upSellChargeNull))))
				.otherwise(upsellCharge).as("upsellChargePerNight");

		Column numberOfOtherNights = functions.when(lgproduct.col("isMajor").equalTo(1), 0)
				.when(lgproduct.col("isMajor").equalTo(0).and(pm.col("roomNights").isNull()), 1)
				.when(pm.col("industry_id").equalTo(5), 1).otherwise(pm.col("roomNights")).as("numberOfOtherNights");

		Column otherChargePerNight = functions.when(lgproduct.col("isMajor").equalTo(1), 0)
				.when(lgproduct.col("isMajor").equalTo(0).and(pm.col("upsellCharge").isNull()), 0)
				.when(pm.col("industry_id").equalTo(5),
						functions
								.greatest(
										functions.when(functions.isnull(upsellCharge),
												functions.when(functions.isnull(ezPayPrice), 0)
														.otherwise(ezPayColumnNull)),
										functions.when(functions.isnull(ezPayPrice),
												functions.when(functions.isnull(upsellCharge), 0)
														.otherwise(upSellChargeNull))))
				.otherwise(upsellCharge).as("otherChargePerNight");

		Column productGroupTypeVal = tpg.col("TYPE");

		Dataset<Row> filterProductMetric = pm
				.filter((functions.col("industry_id").equalTo("1").or(functions.col("industry_id").equalTo("5"))
						.and(functions.col("arrivalDate").cast("String").isNotNull()
								.and(functions.col("departureDate").cast("String").isNotNull())))
										.or(functions.col("industry_id").notEqual("1")
												.and(functions.col("industry_id").notEqual("5"))
												.and(functions.col("arrivalDate").cast("String").isNotNull().or(
														functions.col("departureDate").cast("String").isNotNull()))));

		Dataset<Row> joinResult = filterProductMetric
				.join(lgproduct, filterProductMetric.col("product_id").equalTo(lgproduct.col("id"))
						.and(lgproduct.col("location_group_id").equalTo(filterProductMetric.col("location_group_id"))
								.and(lgproduct.col("isBasicRateProduct").notEqual(1)
										.or(lgproduct.col("isBasicRateProduct").notEqual("1")))))
				.join(tl, filterProductMetric.col("tenant_location_id").equalTo(tl.col("id")))
				.join(lg, filterProductMetric.col("location_group_id").equalTo(lg.col("ID")))
				.join(lgp, lg.col("id").equalTo(lgp.col("id")))
				.join(tpg, lgproduct.col("PRODUCT_GROUP_ID").equalTo(tpg.col("ID")), "left")
				.join(marketSeg, marketSeg.col("CODE").equalTo(filterProductMetric.col("marketSegment")), "left");

		Dataset<Row> joinSelectedCol = joinResult
				.select(filterProductMetric.col("id"), filterProductMetric.col("user_id").as("USER_ID"),
						filterProductMetric.col("tenant_location_id"), filterProductMetric.col("location_group_id"),
						filterProductMetric.col("product_id"), filterProductMetric.col("industry_id"),
						tl.col("hotelMetricsDataType").as("tenantLocationHotelMetricsDataType"),
						tl.col("productMetricsValidationType").as("tenantLocationProductMetricsValidationType"),
						lgproduct.col("isMajor"), lgproduct.col("isRecurring"),
						lgp.col("IS_DEFAULT").as("locGroupIsDefault"), lgp.col("IS_RETAIL").as("locGroupIsRetail"),
						lgp.col("IS_FOR_MEETING_AND_EVENTS").as("locGroupIsMeetingEvents"),
						filterProductMetric.col("confirmationNumber"),
						functions
								.when(filterProductMetric.col("industry_id").equalTo(5),
										filterProductMetric.col("confirmationNumber"))
								.when(lgproduct.col("isMajor").equalTo("1"),
										functions.concat(lgproduct.col("isMajor"), functions.lit("_"),
												filterProductMetric.col("confirmationNumber")))
								.otherwise(null).as("majorConfirmationNumber"),

						functions
								.when(lgproduct.col("isMajor").equalTo("0"),
										functions.concat(lgproduct.col("isMajor"), functions.lit("_"),
												filterProductMetric.col("confirmationNumber")))
								.otherwise(null).as("minorConfirmationNumber"),

						functions.month(filterProductMetric.col("arrivalDate")).as("arrivalMonth"),

						functions.year(filterProductMetric.col("arrivalDate")).as("arrivalYear"),
						filterProductMetric.col("arrivalDate"),

						functions.month(filterProductMetric.col("departureDate")).as("departureMonth"),

						functions.year(filterProductMetric.col("departureDate")).as("departureYear")

						, filterProductMetric.col("departureDate"),
						functions.month(filterProductMetric.col("businessDate")).as("businessMonth"),
						functions.year(filterProductMetric.col("businessDate")).as("businessYear"),
						filterProductMetric.col("businessDate"), ezPayPrice, upsellCharge, numberOfUpsellNights,

						functions.when(marketSeg.col("TYPE").isNull().or(marketSeg.col("TYPE").equalTo("")),
								functions.lit("-")).otherwise(marketSeg.col("TYPE")).as("MARKET_SEGMENT_TYPE"),

						upsellChargePerNight, filterProductMetric.col("walkInReservation"),
						filterProductMetric.col("roomNights"), productGroupTypeVal

						, numberOfOtherNights, otherChargePerNight, upsellChargePerNight.as("totalDailyUpsellAmount"),
						otherChargePerNight.as("totalDailyOtherAmount"), filterProductMetric.col("id").as("entityID"),

						filterProductMetric.col("auditstatus").as("auditstatus"),
						filterProductMetric.col("originalRate").as("originalRate"),
						filterProductMetric.col("newRate").as("newRate"),
						filterProductMetric.col("originalRoomType").as("originalRoomType"),
						filterProductMetric.col("newRoomType").as("newRoomType"), tpg.col("ID").as("productGroupID"),
						productGroupTypeVal.as("productGroupType"),
						filterProductMetric.col("commissionableRevenue").as("commissionableRevenue"),
						filterProductMetric.col("callOrders").as("callOrders"),
						filterProductMetric.col("ticketsSold").as("ticketsSold"),
						filterProductMetric.col("upsellReservations").as("upsellReservations"),
						filterProductMetric.col("marketSegment1"), filterProductMetric.col("marketSegment2"))
				.withColumn("locGroupIsFoodBeverage", functions.lit(0)).withColumn("entityType", functions.lit("5"));

		Dataset<Row> agg1 = joinSelectedCol.groupBy(functions.col("USER_ID"), functions.col("TENANT_LOCATION_ID"),
				functions.col("LOCATION_GROUP_ID"), functions.col("PRODUCT_ID"), functions.col("INDUSTRY_ID"),
				functions.col("tenantLocationHotelMetricsDataType"),
				functions.col("tenantLocationProductMetricsValidationType"), functions.col("isMajor"),
				functions.col("isRecurring"), functions.col("locGroupIsDefault"), functions.col("locGroupIsRetail"),
				functions.col("locGroupIsMeetingEvents"), functions.col("locGroupIsFoodBeverage"),
				functions.col("confirmationNumber"), functions.col("majorConfirmationNumber"),
				functions.col("minorConfirmationNumber"), functions.col("arrivalMonth"), functions.col("arrivalYear"),
				functions.col("arrivalDate"), functions.col("departureMonth"), functions.col("departureYear"),
				functions.col("departureDate"), functions.col("businessMonth"), functions.col("businessYear"),
				functions.col("businessDate"), functions.col("ezPayPrice"), functions.col("upsellCharge"),
				functions.col("numberOfUpsellNights"), functions.col("upsellChargePerNight"),
				functions.col("numberOfOtherNights"), functions.col("otherChargePerNight"),
				functions.col("totalDailyUpsellAmount"), functions.col("totalDailyOtherAmount"),
				functions.col("entityID"), functions.col("entityType"), functions.col("auditstatus"),
				functions.col("originalRate"), functions.col("newRate"), functions.col("originalRoomType"),
				functions.col("newRoomType"), functions.col("marketSegment1"), functions.col("marketSegment2"),
				functions.col("walkInReservation")

		).agg(functions
				.sum(functions.when(functions.col("tenantLocationHotelMetricsDataType").equalTo(1), otherChargePerNight)
						.when(functions.col("isRecurring").equalTo(0), otherChargePerNight)
						.otherwise(numberOfOtherNights.multiply(otherChargePerNight)))
				.as("totalOtherAmount"),
				functions.sum(functions
						.when(functions.col("tenantLocationHotelMetricsDataType").equalTo(1), otherChargePerNight)
						.when(functions.col("isRecurring").equalTo(0), otherChargePerNight)
						.otherwise(numberOfUpsellNights.multiply(otherChargePerNight))).as("totalUpsellAmount"),

				functions.first("productGroupType").as("productGroupType"),
				functions.first("productGroupId").as("productGroupId"),
//								functions.first(pm.col("walkInReservation")),

//								functions.first(functions.when(result2.col("marketSegment1").isNull().or(result2.col("marketSegment1").equalTo("")), "NA")
//								.otherwise(result2.col("marketSegment1"))).as("marketSegment1"),
//						functions.first(functions.when(result2.col("marketSegment2").isNull().or(result2.col("marketSegment2").equalTo("")), "NA")
//								.otherwise(result2.col("marketSegment2"))).as("marketSegment2"),
				functions
						.first(functions.when(functions.col("walkInReservation").equalTo("Y"), 1)
								.when(functions.col("walkInReservation").equalTo("N"), 0).otherwise(-1))
						.as("reservationType"),

				// for arrival upsell-
				functions.first(functions.when(functions.col("isMajor").equalTo(1), functions.col("confirmationNumber"))
						.otherwise(null)).as("arrivalUpsellsCnf"),
				functions.first(
						functions.when(productGroupTypeVal.equalTo("VU").and(functions.col("isMajor").equalTo(1)),
								functions.col("confirmationNumber")).otherwise(null))
						.as("arrivalVuUpsellsCnf"),
				functions
						.first(functions
								.when(productGroupTypeVal.equalTo("PROT").and(functions.col("isMajor").equalTo(1)),
										functions.col("confirmationNumber"))
								.otherwise(null))
						.as("arrivalProtUpsellsCnf"),
				functions
						.first(functions
								.when(productGroupTypeVal.equalTo("OTH").and(functions.col("isMajor").equalTo(1)),
										functions.col("confirmationNumber"))
								.otherwise(null))
						.as("arrivalOthUpsellsCnf"),

				// for daily upsell
				functions.first(functions.when(functions.col("isMajor").equalTo(1), functions.col("confirmationNumber"))
						.otherwise(null)).as("dailyUpsellsCnf"),
				functions.first(
						functions.when(productGroupTypeVal.equalTo("VU").and(functions.col("isMajor").equalTo(1)),
								functions.col("confirmationNumber")).otherwise(null))
						.as("dailyVuUpsellsCnf"),
				functions.first(
						functions.when(productGroupTypeVal.equalTo("PROT").and(functions.col("isMajor").equalTo(1)),
								functions.col("confirmationNumber")).otherwise(null))
						.as("dailyProtUpsellsCnf"),
				functions.first(
						functions.when(productGroupTypeVal.equalTo("OTH").and(functions.col("isMajor").equalTo(1)),
								functions.col("confirmationNumber")).otherwise(null))
						.as("dailyOthUpsellsCnf"),

				// for depature

				functions.first(functions.when(functions.col("isMajor").equalTo(1), functions.col("confirmationNumber"))
						.otherwise(null)).as("departureUpsellsCnf"),
				functions
						.first(functions
								.when(productGroupTypeVal.equalTo("VU").and(functions.col("isMajor").equalTo(1)),
										functions.col("confirmationNumber"))
								.otherwise(null))
						.as("departureVuUpsellsCnf"),

				functions
						.first(functions
								.when(productGroupTypeVal.equalTo("PROT").and(functions.col("isMajor").equalTo(1)),
										functions.col("confirmationNumber"))
								.otherwise(null))
						.as("departureProtUpsellsCnf"),

				functions
						.first(functions
								.when(productGroupTypeVal.equalTo("OTH").and(functions.col("isMajor").equalTo(1)),
										functions.col("confirmationNumber"))
								.otherwise(null))
						.as("departureOthUpsellsCnf")

				, functions.first(functions.col("id")).as("PM_ID"),
				functions.first(functions.col("callOrders")).as("callOrders"),
				functions.first(functions.col("ticketsSold")).as("ticketsSold"),
				functions.first(functions.col("commissionableRevenue")).as("commissionableRevenue"),
				functions.first(functions.col("upsellReservations")).as("upsellReservations"),

				functions.first(functions.col("MARKET_SEGMENT_TYPE")).as("MARKET_SEGMENT_TYPE"));
		Dataset<Row> upsell_tracker_temp = agg1.withColumn("marketSegment1",
				functions.when(agg1.col("marketSegment1").isNull().or(agg1.col("marketSegment1").equalTo("")), "NA")
						.otherwise(agg1.col("marketSegment1")).as("marketSegment1")

		).withColumn("marketSegment2",
				functions.when(agg1.col("marketSegment2").isNull().or(agg1.col("marketSegment2").equalTo("")), "NA")
						.otherwise(agg1.col("marketSegment2")).as("marketSegment2"))
				.drop("walkInReservation");

		logger.warn("-------------------------------Transform Upsell tracker table ended----------------------");
		return upsell_tracker_temp;
	}

	private Dataset<Row> Upsell_tracker_location_metrics_view_transformation(Integer tenantLocationId,
			LocalDate minDate, LocalDate maxDate, Dataset<Row> pm, Row row, Dataset<Row> lgproduct, Dataset<Row> tl) {
		logger.warn(
				"-------------------------------Upsell_tracker_location_metrics_view_transformation started ----------------------");

	
//		List<Row> collectAsList = stored_procedure_info
//				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
//				.select("field_value").collectAsList();
//		String id[] = (collectAsList.size() == 0 ? ""
//				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
//						.split(",");
//		List<Row> lmDate = stored_procedure_info
//				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
//				.collectAsList();
//		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
//				.select("datetime").collectAsList();
//
//		// main block
//
//		Dataset<Row> main = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID"))).where(pm.col("auditStatus")
//				.notEqual(2).and(pm.col("auditStatus").notEqual(4)).and(pm.col("auditStatus").notEqual(5))
//				.and(tl.col("hotelMetricsDataType").isin("0", "1", "2"))
//				.and(collectAsList.isEmpty() ? tl.col("id").isin(id) : functions.when(tl.col("id").isNull(), true))
//				.and(pm.col("arrivalDate").$greater$eq(lmDate.get(0).toString().substring(1, 11)))
//				.or(pm.col("departureDate").$greater$eq(lmDate.get(0).toString().substring(1, 11)))
//				.or(pm.col("businessDate").$greater$eq(lmDate.get(0).toString().substring(1, 11))))
//				.select(pm.col("TENANT_LOCATION_ID"), pm.col("arrivalDate"), pm.col("departureDate"))
//				.persist(StorageLevel.MEMORY_ONLY_SER());
//		main.createOrReplaceTempView("main");
//
////		Exception added one column for join 
//
//		Dataset<Row> mainGroupBy = sparkSession.sql(
//				"select  TENANT_LOCATION_ID, arrivalDate, departureDate from main group by TENANT_LOCATION_ID, arrivalDate ,departureDate");
//
//		Dataset<Row> result1 = mainGroupBy.join(date_intervals).where(date_intervals.col("metricDate")
//				.between(mainGroupBy.col("arrivalDate"), mainGroupBy.col("departureDate")))
//				.select("TENANT_LOCATION_ID", "metricDate");
//// main done ready for union
////result 2 is for arrivalDate
//		Dataset<Row> result2 = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID")))
//
//				.where(pm.col("auditStatus").notEqual(2).and(pm.col("auditStatus").notEqual(4))
//						.and(pm.col("auditStatus").notEqual(5)).and(tl.col("hotelMetricsDataType").isin("0", "1", "2"))
//						.and(collectAsList.isEmpty() ? tl.col("id").isin(id)
//								: functions.when(tl.col("id").isNull(), true))
//						.and(pm.col("arrivalDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11)))
//						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11)))
//						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11))))
//				.select(pm.col("TENANT_LOCATION_ID"), pm.col("arrivalDate").as("metricDate"))
//				.persist(StorageLevel.MEMORY_ONLY_SER());
//		result2.createOrReplaceTempView("result2");
//
//		Dataset<Row> result2GroupBy = sparkSession
//				.sql("select * from result2 group by TENANT_LOCATION_ID, metricDate ");
//		// result2 done ready for union
//		// result 2 is for departureDate
//		Dataset<Row> result3 = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID")))
//
//				.where(pm.col("auditStatus").notEqual(2).and(pm.col("auditStatus").notEqual(4))
//						.and(pm.col("auditStatus").notEqual(5)).and(tl.col("hotelMetricsDataType").isin("0", "1", "2"))
//						.and(collectAsList.isEmpty() ? tl.col("id").isin(id)
//								: functions.when(tl.col("id").isNull(), true))
//						.and(pm.col("arrivalDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11)))
//						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11)))
//						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11))))
//				.select(pm.col("TENANT_LOCATION_ID"), pm.col("departureDate").as("metricDate"))
//				.persist(StorageLevel.MEMORY_ONLY_SER());
//		result3.createOrReplaceTempView("result3");
//		Dataset<Row> result3GroupBy = sparkSession
//				.sql("select * from result3 group by TENANT_LOCATION_ID, metricDate ");
//		// result 2 is for businessDate
//		Dataset<Row> result4 = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID")))
//
//				.where(pm.col("auditStatus").notEqual(2).and(pm.col("auditStatus").notEqual(4))
//						.and(pm.col("auditStatus").notEqual(5)).and(tl.col("hotelMetricsDataType").equalTo(1))
//						.and(collectAsList.isEmpty() ? tl.col("id").isin(id)
//								: functions.when(tl.col("id").isNull(), true))
//						.and(pm.col("arrivalDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11)))
//						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11)))
//						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
//								pmDate.get(1).toString().substring(1, 11))))
//				.select(pm.col("TENANT_LOCATION_ID"), pm.col("businessDate").as("metricDate"))
//				.persist(StorageLevel.MEMORY_ONLY_SER());
//		result4.createOrReplaceTempView("result4");
//		Dataset<Row> result4GroupBy = sparkSession
//				.sql("select * from result4 group by TENANT_LOCATION_ID, metricDate ");
//
//		result1.union(result2GroupBy).union(result3GroupBy).union(result4GroupBy).createOrReplaceTempView("pmfinal");
//
//		Dataset<Row> productmetricsdata = sparkSession
//				.sql("select * from pmfinal group by TENANT_LOCATION_ID, metricDate");
//
//		Dataset<Row> locationmetricdata = lm.select("TENANT_LOCATION_ID", "metricDate");
//
//		Dataset<Row> finalResult = productmetricsdata
//				.join(locationmetricdata,
//						locationmetricdata.col("TENANT_LOCATION_ID")
//								.equalTo(productmetricsdata.col("TENANT_LOCATION_ID"))
//								.and(locationmetricdata.col("metricDate")
//										.equalTo(productmetricsdata.col("metricDate"))))
//				.select(productmetricsdata.col("TENANT_LOCATION_ID"), locationmetricdata.col("metricDate"));
//
//		finalResult.createOrReplaceTempView("upsell_tracker_location_metrics_unique_view");
//
//		sparkSession.catalog().dropTempView("result2");
//		sparkSession.catalog().dropTempView("result3");
//		sparkSession.catalog().dropTempView("result4");
//		sparkSession.catalog().dropTempView("pmfinal");
//		sparkSession.catalog().dropTempView("main");
//		
//		logger.warn("-------------------------------Upsell_tracker_location_metrics_view_transformation ended ----------------------");
		return null;
		
	}
}
