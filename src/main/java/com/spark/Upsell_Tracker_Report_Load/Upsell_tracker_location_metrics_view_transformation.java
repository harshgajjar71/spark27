package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class Upsell_tracker_location_metrics_view_transformation {
	final static Logger logger = Logger.getLogger(Upsell_tracker_location_metrics_view_transformation.class);

	sqlConnection conn;
	SparkSession sparkSession;

	public Upsell_tracker_location_metrics_view_transformation(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void start() {
		logger.warn(
				"-------------------------------Upsell_tracker_location_metrics_view_transformation started ----------------------");

		Dataset<Row> pm = sparkSession.table("product_metric").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> tl = sparkSession.table("tenant_location").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> stored_procedure_info = conn.dbSession("stored_procedure_info");
// new db session
		Dataset<Row> lm = conn.dbSession("location_metric").persist(StorageLevel.MEMORY_ONLY_SER());
		lm.createOrReplaceTempView("location_metric");
		Dataset<Row> date_intervals = conn.dbSession("date_intervals").persist(StorageLevel.MEMORY_ONLY_SER());

		List<Row> collectAsList = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value").collectAsList();
		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");
		List<Row> lmDate = stored_procedure_info
				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
				.collectAsList();
		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
				.select("datetime").collectAsList();

		// main block

		Dataset<Row> main = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID"))).where(pm.col("auditStatus")
				.notEqual(2).and(pm.col("auditStatus").notEqual(4)).and(pm.col("auditStatus").notEqual(5))
				.and(tl.col("hotelMetricsDataType").isin("0", "1", "2"))
				.and(collectAsList.isEmpty() ? tl.col("id").isin(id) : functions.when(tl.col("id").isNull(), true))
				.and(pm.col("arrivalDate").$greater$eq(lmDate.get(0).toString().substring(1, 11)))
				.or(pm.col("departureDate").$greater$eq(lmDate.get(0).toString().substring(1, 11)))
				.or(pm.col("businessDate").$greater$eq(lmDate.get(0).toString().substring(1, 11))))
				.select(pm.col("TENANT_LOCATION_ID"), pm.col("arrivalDate"), pm.col("departureDate"))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		main.createOrReplaceTempView("main");

//		Exception added one column for join 

		Dataset<Row> mainGroupBy = sparkSession.sql(
				"select  TENANT_LOCATION_ID, arrivalDate, departureDate from main group by TENANT_LOCATION_ID, arrivalDate ,departureDate");

		Dataset<Row> result1 = mainGroupBy.join(date_intervals).where(date_intervals.col("metricDate")
				.between(mainGroupBy.col("arrivalDate"), mainGroupBy.col("departureDate")))
				.select("TENANT_LOCATION_ID", "metricDate");
// main done ready for union
//result 2 is for arrivalDate
		Dataset<Row> result2 = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID")))

				.where(pm.col("auditStatus").notEqual(2).and(pm.col("auditStatus").notEqual(4))
						.and(pm.col("auditStatus").notEqual(5)).and(tl.col("hotelMetricsDataType").isin("0", "1", "2"))
						.and(collectAsList.isEmpty() ? tl.col("id").isin(id)
								: functions.when(tl.col("id").isNull(), true))
						.and(pm.col("arrivalDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11))))
				.select(pm.col("TENANT_LOCATION_ID"), pm.col("arrivalDate").as("metricDate"))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		result2.createOrReplaceTempView("result2");

		Dataset<Row> result2GroupBy = sparkSession
				.sql("select * from result2 group by TENANT_LOCATION_ID, metricDate ");
		// result2 done ready for union
		// result 2 is for departureDate
		Dataset<Row> result3 = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID")))

				.where(pm.col("auditStatus").notEqual(2).and(pm.col("auditStatus").notEqual(4))
						.and(pm.col("auditStatus").notEqual(5)).and(tl.col("hotelMetricsDataType").isin("0", "1", "2"))
						.and(collectAsList.isEmpty() ? tl.col("id").isin(id)
								: functions.when(tl.col("id").isNull(), true))
						.and(pm.col("arrivalDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11))))
				.select(pm.col("TENANT_LOCATION_ID"), pm.col("departureDate").as("metricDate"))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		result3.createOrReplaceTempView("result3");
		Dataset<Row> result3GroupBy = sparkSession
				.sql("select * from result3 group by TENANT_LOCATION_ID, metricDate ");
		// result 2 is for businessDate
		Dataset<Row> result4 = pm.join(tl, pm.col("TENANT_LOCATION_ID").equalTo(tl.col("ID")))

				.where(pm.col("auditStatus").notEqual(2).and(pm.col("auditStatus").notEqual(4))
						.and(pm.col("auditStatus").notEqual(5)).and(tl.col("hotelMetricsDataType").equalTo(1))
						.and(collectAsList.isEmpty() ? tl.col("id").isin(id)
								: functions.when(tl.col("id").isNull(), true))
						.and(pm.col("arrivalDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11))))
				.select(pm.col("TENANT_LOCATION_ID"), pm.col("businessDate").as("metricDate"))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		result4.createOrReplaceTempView("result4");
		Dataset<Row> result4GroupBy = sparkSession
				.sql("select * from result4 group by TENANT_LOCATION_ID, metricDate ");

		result1.union(result2GroupBy).union(result3GroupBy).union(result4GroupBy).createOrReplaceTempView("pmfinal");

		Dataset<Row> productmetricsdata = sparkSession
				.sql("select * from pmfinal group by TENANT_LOCATION_ID, metricDate");

		Dataset<Row> locationmetricdata = lm.select("TENANT_LOCATION_ID", "metricDate");

		Dataset<Row> finalResult = productmetricsdata
				.join(locationmetricdata,
						locationmetricdata.col("TENANT_LOCATION_ID")
								.equalTo(productmetricsdata.col("TENANT_LOCATION_ID"))
								.and(locationmetricdata.col("metricDate")
										.equalTo(productmetricsdata.col("metricDate"))))
				.select(productmetricsdata.col("TENANT_LOCATION_ID"), locationmetricdata.col("metricDate"));

		finalResult.createOrReplaceTempView("upsell_tracker_location_metrics_unique_view");

		sparkSession.catalog().dropTempView("result2");
		sparkSession.catalog().dropTempView("result3");
		sparkSession.catalog().dropTempView("result4");
		sparkSession.catalog().dropTempView("pmfinal");
		sparkSession.catalog().dropTempView("main");

		result4.unpersist();
		result3.unpersist();
		result2.unpersist();
		main.unpersist();
		lm.unpersist();
		date_intervals.unpersist();
		
		logger.warn(
				"-------------------------------Upsell_tracker_location_metrics_view_transformation ended ----------------------");

	}
}
