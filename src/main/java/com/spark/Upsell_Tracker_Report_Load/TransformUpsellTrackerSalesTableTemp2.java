package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformUpsellTrackerSalesTableTemp2 {
	final static Logger logger = Logger.getLogger(TransformUpsellTrackerSalesTableTemp2.class);

	sqlConnection conn;
	SparkSession sparkSession;

	public TransformUpsellTrackerSalesTableTemp2(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

//	public void runAllYears() {
//		Dataset<Row> yr2015 = start(2015);
//		yr2015.printSchema();
//		Dataset<Row> yr2016 = start(2016);
//		yr2016.printSchema();
//		Dataset<Row> yr2017 = start(2017);
//		yr2017.printSchema();
//		Dataset<Row> yr2018 = start(2018);
//		yr2018.printSchema();
//		Dataset<Row> yr2019 = start(2019);
//		yr2019.printSchema();
//		Dataset<Row> finalData = yr2015.union(yr2016).union(yr2017).union(yr2018).union(yr2019);
//		finalData.printSchema();
//	}

	public void start(int Year) {
		logger.warn("-------------------------------Transform Upsell Tracker Sales Table Temp2 started with year "
				+ String.valueOf(Year) + "----------------------");
		Dataset<Row> stored_procedure_info = conn.dbSession("stored_procedure_info");
//				.persist(StorageLevel.MEMORY_ONLY_SER());
		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
				.select("datetime").collectAsList();
		Dataset<Row> utu = sparkSession.table("upsell_tracker_location_metrics_unique_view");
//				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> utt = sparkSession.table("upsell_tracker_temp");
//		.persist(StorageLevel.MEMORY_ONLY_SER());

		Column tenantLocationHotelMetricsDataType = utt.col("tenantLocationHotelMetricsDataType").cast("integer");

		Dataset<Row> selectDb = utt
				.join(utu,
						utu.col("TENANT_LOCATION_ID").equalTo(
								utt.col("TENANT_LOCATION_ID")).and(
										functions
												.when(tenantLocationHotelMetricsDataType.equalTo(1), utt
														.col("businessDate").equalTo(utu.col("metricDate")))
												.otherwise(functions
														.when(utt.col("isRecurring").equalTo(1),
																utt.col("arrivalDate").$less$eq(utu.col("metricDate"))
																		.and(utt.col("departureDate")
																				.$greater(utu.col("metricDate"))))
														.otherwise(utt.col("departureDate")
																.equalTo(utu.col("metricDate"))))))
				.select(utt.col("departureUpsellsCnf"), utt.col("departureVuUpsellsCnf"),
						utt.col("departureProtUpsellsCnf"), utt.col("departureOthUpsellsCnf"),
						utt.col("marketSegment1"), utt.col("marketSegment2"), utt.col("reservationType"),
						utt.col("commissionableRevenue"), utt.col("callOrders"), utt.col("ticketsSold"),
						utt.col("upsellReservations"), utt.col("majorConfirmationNumber"),
						utt.col("minorConfirmationNumber"),

						utt.col("productGroupType"), utt.col("isMajor"), utt.col("numberOfOtherNights"),
						utt.col("tenantLocationHotelMetricsDataType"), utt.col("totalDailyOtherAmount"),
						utt.col("totalDailyUpsellAmount"), utt.col("numberOfUpsellNights"), utt.col("INDUSTRY_ID"),
						utt.col("PRODUCT_ID"), utt.col("USER_ID"), utt.col("LOCATION_GROUP_ID"),
						utu.col("TENANT_LOCATION_ID"), utu.col("metricDate"), utt.col("dailyUpsellsCnf"),
						utt.col("dailyVuUpsellsCnf"), utt.col("dailyProtUpsellsCnf"), utt.col("dailyOthUpsellsCnf"),
						utt.col("MARKET_SEGMENT_TYPE"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> whereDb = null;
		if (Year < 2016) {
			whereDb = selectDb
					.where(utu.col("metricDate").$greater$eq(pmDate.get(0).toString().substring(1, 11))
							.and(functions.year(utu.col("metricDate")).cast("int").$less$eq(Year)))
					.persist(StorageLevel.MEMORY_ONLY_SER());
		} else {
			whereDb = selectDb
					.where(utu.col("metricDate").$greater$eq(pmDate.get(0).toString().substring(1, 11))
							.and(functions.year(utu.col("metricDate")).cast("int").$less$eq(Year)))
					.persist(StorageLevel.MEMORY_ONLY_SER());
		}
		selectDb.unpersist();

		Column dailyOtherNightsSum = functions.sum(functions
				.when(whereDb.col("tenantLocationHotelMetricsDataType").equalTo(1)
						.and(whereDb.col("INDUSTRY_ID").notEqual(2)).and(whereDb.col("INDUSTRY_ID").notEqual(11)),
						functions.when(whereDb.col("isMajor").equalTo(1), 0).otherwise(1))
				.otherwise(functions.when(whereDb.col("isMajor").equalTo(1), 0)
						.otherwise(whereDb.col("numberOfOtherNights"))));
		Column dailyUpsellNightsSum = functions.sum(functions
				.when(whereDb.col("tenantLocationHotelMetricsDataType").equalTo(1)
						.and(whereDb.col("INDUSTRY_ID").notEqual(2)).and(whereDb.col("INDUSTRY_ID").notEqual(11)),
						functions.when(whereDb.col("isMajor").equalTo(1), 1).otherwise(0))
				.otherwise(functions.when(whereDb.col("isMajor").equalTo(1), 0)
						.otherwise(whereDb.col("numberOfOtherNights"))));
		Column countDistinctMinor = functions.countDistinct(whereDb.col("minorConfirmationNumber"));
		Column countDistinctMajor = functions.countDistinct(whereDb.col("majorConfirmationNumber"));
		Column roundSumUpsell = functions.round(functions.sum(whereDb.col("totalDailyUpsellAmount")), 2);
		Column roundSumOther = functions.round(functions.sum(whereDb.col("totalDailyOtherAmount")), 2);
		Dataset<Row> finalResult = whereDb

				.groupBy(whereDb.col("USER_ID"), whereDb.col("TENANT_LOCATION_ID"), whereDb.col("LOCATION_GROUP_ID"),
						whereDb.col("PRODUCT_ID"), whereDb.col("metricDate"), whereDb.col("marketSegment1"),
						whereDb.col("marketSegment2"), whereDb.col("reservationType"),
						whereDb.col("MARKET_SEGMENT_TYPE"))
				.agg(functions.month(utu.col("metricDate")).as("month"),
						functions.year(utu.col("metricDate")).as("year"), functions.lit(0).as("arrivalUpsellRevenue"),

						roundSumUpsell.as("dailyUpsellRevenue"), roundSumOther.as("dailyOtherRevenue"),
						dailyOtherNightsSum.as("dailyOtherNights"), dailyUpsellNightsSum.as("dailyUpsellNights"),
						countDistinctMajor.as("dailyUpsells"), countDistinctMinor.as("dailyOtherUpsells"),

						functions.when(functions.first(whereDb.col("productGroupType").equalTo("VU")), roundSumUpsell)
								.otherwise(0).as("dailyVuUpsellRevenue"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("VU")), roundSumOther)
								.otherwise(0).as("dailyVuOtherRevenue"),

						functions.when(functions.first(whereDb.col("productGroupType").equalTo("VU")),
								dailyUpsellNightsSum).otherwise(0).as("dailyVuUpsellNights"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("VU")),
								dailyOtherNightsSum).otherwise(0).as("dailyVuOtherNights")

						,
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("VU")),
								countDistinctMajor).otherwise(0).as("dailyVuUpsells"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("VU")),
								countDistinctMinor).otherwise(0).as("dailyVuOtherUpsells")

						,
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("PROT")), roundSumUpsell)
								.otherwise(0).as("dailyProtUpsellRevenue"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("PROT")), roundSumOther)
								.otherwise(0).as("dailyProtOtherRevenue"),

						functions.when(functions.first(whereDb.col("productGroupType").equalTo("PROT")),
								dailyUpsellNightsSum).otherwise(0).as("dailyProtUpsellNights"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("PROT")),
								dailyOtherNightsSum).otherwise(0).as("dailyProtOtherNights"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("PROT")),
								countDistinctMajor).otherwise(0).as("dailyProtUpsells"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("PROT")),
								countDistinctMinor).otherwise(0).as("dailyProtOtherUpsells"),

						functions.when(functions.first(whereDb.col("productGroupType").equalTo("OTH")),
								countDistinctMajor).otherwise(0).as("dailyOthUpsells"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("OTH")),
								countDistinctMinor).otherwise(0).as("dailyOthOtherUpsells"),

						functions.when(functions.first(whereDb.col("productGroupType").equalTo("OTH")), roundSumUpsell)
								.otherwise(0).as("dailyOthUpsellRevenue"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("OTH")), roundSumOther)
								.otherwise(0).as("dailyOthOtherRevenue"),

						functions.when(functions.first(whereDb.col("productGroupType").equalTo("OTH")),
								dailyUpsellNightsSum).otherwise(0).as("dailyOthUpsellNights"),
						functions.when(functions.first(whereDb.col("productGroupType").equalTo("OTH")),
								dailyOtherNightsSum).otherwise(0).as("dailyOthOtherNights"),

						functions.round(functions.sum(whereDb.col("commissionableRevenue")), 2)
								.as("dailyCommissionableRevenue"),
						functions.round(functions.sum(whereDb.col("callOrders")), 2).as("dailyCallOrders"),
						functions.round(functions.sum(whereDb.col("ticketsSold")), 2).as("dailyTicketsSold"),
						functions.round(functions.sum(whereDb.col("upsellReservations")), 2)
								.as("dailyUpsellReservations"),

						functions.first(functions.concat_ws(" , ", whereDb.col("dailyUpsellsCnf")))
								.as("dailyUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", whereDb.col("dailyVuUpsellsCnf")))
								.as("dailyVuUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", whereDb.col("dailyProtUpsellsCnf")))
								.as("dailyProtUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", whereDb.col("dailyOthUpsellsCnf")))
								.as("dailyOthUpsellsCnf"),

						functions
								.when(functions.first(whereDb.col("MARKET_SEGMENT_TYPE").equalTo("T")),
										functions.round(functions.sum(whereDb.col("totalDailyUpsellAmount")), 2))
								.otherwise("0").as("dailyUpsellTranRevenue"),

						functions
								.when(functions.first(whereDb.col("MARKET_SEGMENT_TYPE").equalTo("T")),
										functions.round(functions.sum(whereDb.col("totalDailyOtherAmount")), 2))
								.otherwise("0").as("dailyOtherTranRevenue"),

						functions
								.when(functions.first(whereDb.col("MARKET_SEGMENT_TYPE").equalTo("G")),
										functions.round(functions.sum(whereDb.col("totalDailyUpsellAmount")), 2))
								.otherwise("0").as("dailyUpsellGroupRevenue"),

						functions
								.when(functions.first(whereDb.col("MARKET_SEGMENT_TYPE").equalTo("G")),
										functions.round(functions.sum(whereDb.col("totalDailyOtherAmount")), 2))
								.otherwise("0").as("dailyOtherGroupRevenue"),

						functions
								.when(functions.first(whereDb.col("MARKET_SEGMENT_TYPE").equalTo("P")),
										functions.round(functions.sum(whereDb.col("totalDailyUpsellAmount")), 2))
								.otherwise("0").as("dailyUpsellPerRevenue"),

						functions
								.when(functions.first(whereDb.col("MARKET_SEGMENT_TYPE").equalTo("P")),
										functions.round(functions.sum(whereDb.col("totalDailyOtherAmount")), 2))
								.otherwise("0").as("dailyOtherPerRevenue")

				).withColumn("arrivalOtherRevenue", functions.lit(0)).withColumn("arrivalOtherNights", functions.lit(0))
				.withColumn("arrivalOtherUpsells", functions.lit(0))
				.withColumn("arrivalVuOtherRevenue", functions.lit(0))
				.withColumn("arrivalVuOtherNights", functions.lit(0))
				.withColumn("arrivalVuOtherUpsells", functions.lit(0))
				.withColumn("arrivalProtOtherRevenue", functions.lit(0))
				.withColumn("arrivalProtOtherNights", functions.lit(0))
				.withColumn("arrivalProtOtherUpsells", functions.lit(0))
				.withColumn("arrivalOthOtherRevenue", functions.lit(0))
				.withColumn("arrivalOthUpsells", functions.lit(0))
				.withColumn("arrivalOthOtherUpsells", functions.lit(0))
				.withColumn("arrivalCommissionableRevenue", functions.lit(0))
				.withColumn("arrivalTicketsSold", functions.lit(0))
				.withColumn("departureCommissionableRevenue", functions.lit(0))
				.withColumn("departureCallOrders", functions.lit(0))
				.withColumn("departureUpsellReservations", functions.lit(0))
				.withColumn("arrivalUpsellTranRevenue", functions.lit(0))
				.withColumn("arrivalUpsellGroupRevenue", functions.lit(0))
				.withColumn("arrivalOtherGroupRevenue", functions.lit(0))
				.withColumn("arrivalOtherPerRevenue", functions.lit(0))
				.withColumn("arrivalUpsellNights", functions.lit(0)).withColumn("arrivalUpsells", functions.lit(0))
				.withColumn("arrivalVuUpsellRevenue", functions.lit(0))
				.withColumn("arrivalVuUpsellNights", functions.lit(0)).withColumn("arrivalVuUpsells", functions.lit(0))
				.withColumn("arrivalProtUpsellRevenue", functions.lit(0))
				.withColumn("arrivalProtUpsellNights", functions.lit(0))
				.withColumn("arrivalProtUpsells", functions.lit(0))
				.withColumn("arrivalOthUpsellRevenue", functions.lit(0))
				.withColumn("arrivalOthUpsellNights", functions.lit(0))
				.withColumn("arrivalOthOtherNights", functions.lit(0)).withColumn("arrivalCallOrders", functions.lit(0))
				.withColumn("arrivalUpsellReservations", functions.lit(0))

				.withColumn("departureTicketsSold", functions.lit(0))
				.withColumn("arrivalOtherTranRevenue", functions.lit(0))
				.withColumn("arrivalUpsellPerRevenue", functions.lit(0))
				.withColumn("departureUpsellTranRevenue", functions.lit(0))

				.withColumn("departureOtherTranRevenue", functions.lit(0))
				.withColumn("departureUpsellGroupRevenue", functions.lit(0))
				.withColumn("departureOtherGroupRevenue", functions.lit(0))
				.withColumn("departureUpsellPerRevenue", functions.lit(0))
				.withColumn("departureOtherPerRevenue", functions.lit(0))
				.withColumn("departureUpsellRevenue", functions.lit(0))
				.withColumn("departureOtherRevenue", functions.lit(0))
				.withColumn("departureUpsellNights", functions.lit(0))
				.withColumn("departureOtherNights", functions.lit(0)).withColumn("departureUpsells", functions.lit(0))
				.withColumn("departureOtherUpsells", functions.lit(0))
				.withColumn("departureVuUpsellRevenue", functions.lit(0))
				.withColumn("departureVuOtherRevenue", functions.lit(0))
				.withColumn("departureVuUpsellNights", functions.lit(0))
				.withColumn("departureVuOtherNights", functions.lit(0))
				.withColumn("departureVuUpsells", functions.lit(0))
				.withColumn("departureVuOtherUpsells", functions.lit(0))
				.withColumn("departureProtUpsellRevenue", functions.lit(0))
				.withColumn("departureProtOtherRevenue", functions.lit(0))
				.withColumn("departureProtUpsellNights", functions.lit(0))
				.withColumn("departureProtOtherNights", functions.lit(0))
				.withColumn("departureProtUpsells", functions.lit(0))
				.withColumn("departureProtOtherUpsells", functions.lit(0))
				.withColumn("departureOthUpsellRevenue", functions.lit(0))
				.withColumn("departureOthOtherRevenue", functions.lit(0))
				.withColumn("departureOthUpsellNights", functions.lit(0))
				.withColumn("departureOthOtherNights", functions.lit(0))
				.withColumn("departureOthUpsells", functions.lit(0))
				.withColumn("departureOthOtherUpsells", functions.lit(0))

				.withColumn("departureUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureVuUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureOthUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalVuUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalOthUpsellsCnf", functions.lit(null).cast("String")).drop("MARKET_SEGMENT_TYPE");

// view Created
		finalResult.createOrReplaceTempView("salesTemp_2");
//		finalResult.write().mode(SaveMode.Overwrite).parquet("salesTemp_2");
		logger.warn(
				"-------------------------------Transform Upsell Tracker Sales Table Temp2 ended----------------------");
	}

}
