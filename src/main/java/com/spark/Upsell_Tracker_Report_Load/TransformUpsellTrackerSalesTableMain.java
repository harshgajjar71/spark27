package com.spark.Upsell_Tracker_Report_Load;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformUpsellTrackerSalesTableMain {

//	private static final long serialVersionUID = 1L;
	sqlConnection conn;
	SparkSession sparkSession;
//	= spark.sparkSession();
	final static Logger logger = Logger.getLogger(TransformUpsellTrackerSalesTableMain.class);

	public TransformUpsellTrackerSalesTableMain(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void start() {
		logger.warn(
				"-------------------------------Transform_upsell_tracker_sales_table_main started ----------------------");

		Dataset<Row> temp1 = sparkSession.table("salesTemp_1");

		Dataset<Row> temp2 = sparkSession.table("salesTemp_2");

		Dataset<Row> temp3 = sparkSession.table("salesTemp_3");

//		Dataset<Row> temp1 = sparkSession.read().parquet("salesTemp_1");
//
//		Dataset<Row> temp2 = sparkSession.read().parquet("salesTemp_2");
//
//		Dataset<Row> temp3 = sparkSession.read().parquet("salesTemp_3");

		Dataset<Row> dm_user = conn.dbSession("dim_user").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dmTenLoc = conn.dbSession("dim_tenant_location").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dmLocGrp = conn.dbSession("dim_location_group").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dmProd = conn.dbSession("dim_product").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dim_calendar = conn.dbSession("dim_calendar").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> main = temp2.unionByName(temp3).unionByName(temp1);

		Dataset<Row> result1 = main
				.join(dm_user,
						dm_user.col("user_id").equalTo(main.col("USER_ID"))
								.and(dm_user.col("tenant_location_id").equalTo(main.col("TENANT_LOCATION_ID")))
								.and(dm_user.col("location_group_id").equalTo(main.col("LOCATION_GROUP_ID"))))
				.join(dmTenLoc, dmTenLoc.col("tenant_location_id").equalTo(main.col("TENANT_LOCATION_ID")))
				.join(dmLocGrp,
						dmLocGrp.col("tenant_location_id").equalTo(main.col("TENANT_LOCATION_ID"))
								.and(dmLocGrp.col("location_group_id").equalTo(main.col("LOCATION_GROUP_ID"))))
				.join(dmProd,
						dmProd.col("tenant_location_id").equalTo(main.col("TENANT_LOCATION_ID"))
								.and(dmProd.col("LOCATION_GROUP_ID").equalTo(main.col("location_group_id")))
								.and(dmProd.col("LOCATION_GROUP_PRODUCT_ID").equalTo(main.col("PRODUCT_ID"))))
				.join(dim_calendar, dim_calendar.col("FULL_DATE").equalTo(main.col("metricDate")))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		////// for id
		Long id = 0L;

		Dataset<Row> result = result1
				.groupBy(main.col("USER_ID"), main.col("TENANT_LOCATION_ID"), main.col("LOCATION_GROUP_ID"),
						main.col("PRODUCT_ID"), main.col("metricDate"), main.col("marketSegment1"),
						main.col("marketSegment2"), main.col("reservationType"), dm_user.col("ID").as("DIM_USER_ID"),
						dmTenLoc.col("ID").as("DIM_TENANT_LOCATION_ID"), dmLocGrp.col("ID").as("DIM_LOCATION_GROUP_ID"),
						dmProd.col("ID").as("DIM_PRODUCT_ID"), dim_calendar.col("id").as("DIM_METRIC_DATE_ID"))
				.agg(functions.sum(main.col("arrivalUpSellRevenue")).as("arrivalUpSellRevenue"),
						functions.sum(main.col("arrivalOtherRevenue")).as("arrivalOtherRevenue"),
						functions.sum(main.col("arrivalUpsellNights")).as("arrivalUpsellNights"),
						functions.sum(main.col("arrivalOtherNights")).as("arrivalOtherNights"),
						functions.sum(main.col("arrivalUpsells")).as("arrivalUpsells"),
						functions.sum(main.col("arrivalOtherUpsells")).as("arrivalOtherUpsells"),
						functions.sum(main.col("arrivalVuUpsellRevenue")).as("arrivalVuUpsellRevenue"),
						functions.sum(main.col("arrivalVuOtherRevenue")).as("arrivalVuOtherRevenue"),
						functions.sum(main.col("arrivalVuUpsellNights")).as("arrivalVuUpsellNights"),
						functions.sum(main.col("arrivalVuOtherNights")).as("arrivalVuOtherNights"),
						functions.sum(main.col("arrivalVuUpsells")).as("arrivalVuUpsells"),
						functions.sum(main.col("arrivalVuOtherUpsells")).as("arrivalVuOtherUpsells"),
						functions.sum(main.col("arrivalProtUpsellRevenue")).as("arrivalProtUpsellRevenue"),
						functions.sum(main.col("arrivalProtOtherRevenue")).as("arrivalProtOtherRevenue"),
						functions.sum(main.col("arrivalProtUpsellNights")).as("arrivalProtUpsellNights"),
						functions.sum(main.col("arrivalProtOtherNights")).as("arrivalProtOtherNights"),
						functions.sum(main.col("arrivalProtUpsells")).as("arrivalProtUpsells"),
						functions.sum(main.col("arrivalProtOtherUpsells")).as("arrivalProtOtherUpsells"),
						functions.sum(main.col("arrivalOthUpsellRevenue")).as("arrivalOthUpsellRevenue"),
						functions.sum(main.col("arrivalOthOtherRevenue")).as("arrivalOthOtherRevenue"),
						functions.sum(main.col("arrivalOthUpsellNights")).as("arrivalOthUpsellNights"),
						functions.sum(main.col("arrivalOthOtherNights")).as("arrivalOthOtherNights"),
						functions.sum(main.col("arrivalOthUpsells")).as("arrivalOthUpsells"),

						functions.sum(main.col("arrivalOthOtherUpsells")).as("arrivalOthOtherUpsells"),
						functions.sum(main.col("arrivalCommissionableRevenue")).as("arrivalCommissionableRevenue"),
						functions.sum(main.col("arrivalCallOrders")).as("arrivalCallOrders"),
						functions.sum(main.col("arrivalTicketsSold")).as("arrivalTicketsSold"),
						functions.sum(main.col("arrivalUpsellReservations")).as("arrivalUpsellReservations"),
						functions.sum(main.col("dailyUpsellRevenue")).as("dailyUpsellRevenue"),
						functions.sum(main.col("dailyOtherRevenue")).as("dailyOtherRevenue"),
						functions.sum(main.col("dailyUpsellNights")).as("dailyUpsellNights"),
						functions.sum(main.col("dailyOtherNights")).as("dailyOtherNights"),

						functions.sum(main.col("dailyOtherUpsells")).as("dailyOtherUpsells"),

						functions.sum(main.col("dailyVuUpsellRevenue")).as("dailyVuUpsellRevenue"),
						functions.sum(main.col("dailyVuOtherRevenue")).as("dailyVuOtherRevenue"),
						functions.sum(main.col("dailyVuUpsellNights")).as("dailyVuUpsellNights"),
						functions.sum(main.col("dailyVuOtherNights")).as("dailyVuOtherNights"),
						functions.sum(main.col("dailyUpsells")).as("dailyUpsells"),
						functions.sum(main.col("dailyVuUpsells")).as("dailyVuUpsells"),
						functions.sum(main.col("dailyVuOtherUpsells")).as("dailyVuOtherUpsells"),
						functions.sum(main.col("dailyProtUpsellRevenue")).as("dailyProtUpsellRevenue"),
						functions.sum(main.col("dailyProtOtherRevenue")).as("dailyProtOtherRevenue"),
						functions.sum(main.col("dailyProtUpsellNights")).as("dailyProtUpsellNights"),
						functions.sum(main.col("dailyProtOtherNights")).as("dailyProtOtherNights"),
						functions.sum(main.col("dailyProtUpsells")).as("dailyProtUpsells"),
						functions.sum(main.col("dailyProtOtherUpsells")).as("dailyProtOtherUpsells"),
						functions.sum(main.col("dailyOthUpsellRevenue")).as("dailyOthUpsellRevenue"),

						functions.sum(main.col("dailyOthOtherRevenue")).as("dailyOthOtherRevenue"),
						functions.sum(main.col("dailyOthUpsellNights")).as("dailyOthUpsellNights"),
						functions.sum(main.col("dailyOthOtherNights")).as("dailyOthOtherNights"),

						functions.sum(main.col("dailyOthUpsells")).as("dailyOthUpsells"),
						functions.sum(main.col("dailyOthOtherUpsells")).as("dailyOthOtherUpsells"),
						functions.sum(main.col("dailyCommissionableRevenue")).as("dailyCommissionableRevenue"),
						functions.sum(main.col("dailyCallOrders")).as("dailyCallOrders"),
						functions.sum(main.col("dailyTicketsSold")).as("dailyTicketsSold"),
						functions.sum(main.col("dailyUpsellReservations")).as("dailyUpsellReservations"),
						functions.sum(main.col("departureUpsellRevenue")).as("departureUpsellRevenue"),
						functions.sum(main.col("departureOtherRevenue")).as("departureOtherRevenue"),
						functions.sum(main.col("departureUpsellNights")).as("departureUpsellNights"),
						functions.sum(main.col("departureOtherNights")).as("departureOtherNights"),
						functions.sum(main.col("departureUpsells")).as("departureUpsells"),
						functions.sum(main.col("departureOtherUpsells")).as("departureOtherUpsells"),
						functions.sum(main.col("departureVuUpsellRevenue")).as("departureVuUpsellRevenue"),
						functions.sum(main.col("departureVuOtherRevenue")).as("departureVuOtherRevenue"),
						functions.sum(main.col("departureVuUpsellNights")).as("departureVuUpsellNights"),
						functions.sum(main.col("departureVuOtherNights")).as("departureVuOtherNights"),
						functions.sum(main.col("departureVuUpsells")).as("departureVuUpsells"),

						functions.sum(main.col("departureVuOtherUpsells")).as("departureVuOtherUpsells"),
						functions.sum(main.col("departureProtUpsellRevenue")).as("departureProtUpsellRevenue"),
						functions.sum(main.col("departureProtOtherRevenue")).as("departureProtOtherRevenue"),
						functions.sum(main.col("departureProtUpsellNights")).as("departureProtUpsellNights"),
						functions.sum(main.col("departureProtOtherNights")).as("departureProtOtherNights"),
						functions.sum(main.col("departureProtUpsells")).as("departureProtUpsells"),
						functions.sum(main.col("departureProtOtherUpsells")).as("departureProtOtherUpsells"),
						functions.sum(main.col("departureOthUpsellRevenue")).as("departureOthUpsellRevenue"),
						functions.sum(main.col("departureOthOtherRevenue")).as("departureOthOtherRevenue"),
						functions.sum(main.col("departureOthUpsellNights")).as("departureOthUpsellNights"),
						functions.sum(main.col("departureOthOtherNights")).as("departureOthOtherNights"),
						functions.sum(main.col("departureOthUpsells")).as("departureOthUpsells"),
						functions.sum(main.col("departureOthOtherUpsells")).as("departureOthOtherUpsells"),
						functions.sum(main.col("departureCommissionableRevenue")).as("departureCommissionableRevenue"),
						functions.sum(main.col("departureCallOrders")).as("departureCallOrders"),
						functions.sum(main.col("departureTicketsSold")).as("departureTicketsSold"),
						functions.sum(main.col("departureUpsellReservations")).as("departureUpsellReservations"),

						functions.sum(main.col("arrivalUpsellTranRevenue")).as("arrivalUpsellTranRevenue"),
						functions.sum(main.col("arrivalOtherTranRevenue")).as("arrivalOtherTranRevenue"),
						functions.sum(main.col("arrivalUpsellGroupRevenue")).as("arrivalUpsellGroupRevenue"),
						functions.sum(main.col("arrivalOtherGroupRevenue")).as("arrivalOtherGroupRevenue"),
						functions.sum(main.col("arrivalUpsellPerRevenue")).as("arrivalUpsellPerRevenue"),
						functions.sum(main.col("arrivalOtherPerRevenue")).as("arrivalOtherPerRevenue"),

						functions.sum(main.col("departureUpsellTranRevenue")).as("departureUpsellTranRevenue"),
						functions.sum(main.col("departureOtherTranRevenue")).as("departureOtherTranRevenue"),
						functions.sum(main.col("departureUpsellGroupRevenue")).as("departureUpsellGroupRevenue"),
						functions.sum(main.col("departureOtherGroupRevenue")).as("departureOtherGroupRevenue"),

						functions.sum(main.col("departureUpsellPerRevenue")).as("departureUpsellPerRevenue"),
						functions.sum(main.col("departureOtherPerRevenue")).as("departureOtherPerRevenue"),
						functions.sum(main.col("dailyUpsellTranRevenue")).as("dailyUpsellTranRevenue"),

						functions.sum(main.col("dailyOtherTranRevenue")).as("dailyOtherTranRevenue"),
						functions.sum(main.col("dailyUpsellGroupRevenue")).as("dailyUpsellGroupRevenue"),
						functions.sum(main.col("dailyOtherGroupRevenue")).as("dailyOtherGroupRevenue"),
						functions.sum(main.col("dailyUpsellPerRevenue")).as("dailyUpsellPerRevenue"),
						functions.sum(main.col("dailyOtherPerRevenue")).as("dailyOtherPerRevenue")

						,

						functions.first(dmTenLoc.col("REGION_ID")).as("REGION_ID"),
						functions.first(functions.concat_ws(",",
								main.select("arrivalUpsellsCnf").dropDuplicates("arrivalUpSellsCnf")
										.col("arrivalUpSellsCnf")))
								.as("arrivalUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("arrivalVuUpsellsCnf").dropDuplicates("arrivalVuUpsellsCnf")
										.col("arrivalVuUpsellsCnf")))
								.as("arrivalVuUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("arrivalProtUpsellsCnf").dropDuplicates("arrivalProtUpsellsCnf")
										.col("arrivalProtUpsellsCnf")))
								.as("arrivalProtUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("arrivalOthUpsellsCnf").dropDuplicates("arrivalOthUpsellsCnf")
										.col("arrivalOthUpsellsCnf")))
								.as("arrivalOthUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("dailyUpsellsCnf").dropDuplicates("dailyUpsellsCnf")
										.col("dailyUpsellsCnf")))
								.as("dailyUpsellsCnf"),

						functions.first(functions.concat_ws(",",
								main.select("dailyVuUpsellsCnf").dropDuplicates("dailyVuUpsellsCnf")
										.col("dailyVuUpsellsCnf")))
								.as("dailyVuUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("dailyOthUpsellsCnf").dropDuplicates("dailyOthUpsellsCnf")
										.col("dailyOthUpsellsCnf")))
								.as("dailyOthUpsellsCnf"),

						functions.first(functions.concat_ws(",",
								main.select("dailyProtUpsellsCnf").dropDuplicates("dailyProtUpsellsCnf")
										.col("dailyProtUpsellsCnf")))
								.as("dailyProtUpsellsCnf"),

						functions.first(functions.concat_ws(",",
								main.select("departureUpsellsCnf").dropDuplicates("departureUpsellsCnf")
										.col("departureUpsellsCnf")))
								.as("departureUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("departureVuUpsellsCnf").dropDuplicates("departureVuUpsellsCnf")
										.col("departureVuUpsellsCnf")))
								.as("departureVuUpsellsCnf"),
						functions.first(functions.concat_ws(",",
								main.select("departureProtUpsellsCnf").dropDuplicates("departureProtUpsellsCnf")
										.col("departureProtUpsellsCnf")))
								.as("departureProtUpsellsCnf"),
						functions
								.first(functions.concat_ws(",", main.select("departureOthUpsellsCnf")
										.dropDuplicates("departureOthUpsellsCnf").col("departureOthUpsellsCnf")))
								.as("departureOthUpsellsCnf")

				).withColumn("month", functions.month(main.col("metricDate")))
				.withColumn("year", functions.year(main.col("metricDate"))).withColumn("id", functions.lit(id));
		result.createOrReplaceTempView("salesTemp");

		logger.warn(main.count() + "after Union--------------------------");
		logger.warn(result.count() + "---------sales count");
		logger.warn(result1.count() + "---------After Join count");

		conn.sparkSqlWrite(result, "sales");
		
		sparkSession.catalog().dropTempView("salesTemp_1");
		sparkSession.catalog().dropTempView("salesTemp_2");
		sparkSession.catalog().dropTempView("salesTemp_3");
		sparkSession.catalog().dropTempView("upsell_tracker_location_metrics_unique_view");
		logger.warn(
				"-------------------------------Transform_upsell_tracker_sales_table_main ended ----------------------");
	}

}
