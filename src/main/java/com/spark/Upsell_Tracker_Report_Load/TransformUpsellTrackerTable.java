package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformUpsellTrackerTable {

	sqlConnection conn;

	SparkSession sparkSession;
	final static Logger logger = Logger.getLogger(TransformUpsellTrackerTable.class);

	public TransformUpsellTrackerTable(sqlConnection conn) {

		conn.dbSession("product_metric")

				.createOrReplaceTempView("product_metric");

		conn.dbSession("tenant_location").createOrReplaceTempView("tenant_location");
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void upsell_tracker_table() {
		logger.warn("-------------------------------Transform_upsell_tracker_table started ----------------------");
		Dataset<Row> pm = conn.dbSession("product_metric").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> lgproduct = conn.dbSession("location_group_product").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> tl = conn.dbSession("tenant_location").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> lgp = conn.dbSession("location_group_profile").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> lg = conn.dbSession("location_group").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> tpg = conn.dbSession("tenant_product_group").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> marketSeg = conn.dbSession("market_segment_code").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> stored_procedure_info = conn.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		stored_procedure_info.createOrReplaceTempView("stored_procedure_info");

		Dataset<Row> result = pm
				.join(lgproduct,
						pm.col("product_id").equalTo(lgproduct.col("id"))
								.and(lgproduct.col("location_group_id").equalTo(pm.col("location_group_id"))
										.and(lgproduct.col("isBasicRateProduct").notEqual(1)
												.or(lgproduct.col("isBasicRateProduct").notEqual("1")))))
				.join(tl, pm.col("tenant_location_id").equalTo(tl.col("id")))
				.join(lg, pm.col("location_group_id").equalTo(lg.col("ID")))
				.join(lgp, lg.col("id").equalTo(lgp.col("id")))
				.join(tpg, lgproduct.col("PRODUCT_GROUP_ID").equalTo(tpg.col("ID")), "left")
				.join(marketSeg, marketSeg.col("CODE").equalTo(pm.col("marketSegment")), "left");

		Column ezPayPrice = pm.col("ezPayPrice");
		Column upsellCharge = pm.col("upsellCharge");

		Column numberOfUpsellNights = functions.when(lgproduct.col("isMajor").equalTo(0), 0)
				.when(lgproduct.col("isMajor").equalTo(1).and(pm.col("roomNights").isNull()), 1)
				.when(pm.col("industry_id").equalTo(5), 1).otherwise(pm.col("roomNights")).as("numberOfUpsellNights");
		Column ezPayColumnNull = functions.when(ezPayPrice.$less(0), ezPayPrice.$minus(1)).otherwise(0);
		Column upSellChargeNull = functions.when(upsellCharge.$less(0), upsellCharge.$minus(1)).otherwise(0);

		Column upsellChargePerNight = functions.when(lgproduct.col("isMajor").equalTo(0), 0)
				.when(lgproduct.col("isMajor").equalTo(1).and(pm.col("upsellCharge").isNull()), 0)
				.when(pm.col("industry_id").equalTo(5),
						functions
								.greatest(
										functions.when(functions.isnull(upsellCharge),
												functions.when(functions.isnull(ezPayPrice), 0)
														.otherwise(ezPayColumnNull)),
										functions.when(functions.isnull(ezPayPrice),
												functions.when(functions.isnull(upsellCharge), 0)
														.otherwise(upSellChargeNull))))
				.otherwise(upsellCharge).as("upsellChargePerNight");

		Column numberOfOtherNights = functions.when(lgproduct.col("isMajor").equalTo(1), 0)
				.when(lgproduct.col("isMajor").equalTo(0).and(pm.col("roomNights").isNull()), 1)
				.when(pm.col("industry_id").equalTo(5), 1).otherwise(pm.col("roomNights")).as("numberOfOtherNights");

		Column otherChargePerNight = functions.when(lgproduct.col("isMajor").equalTo(1), 0)
				.when(lgproduct.col("isMajor").equalTo(0).and(pm.col("upsellCharge").isNull()), 0)
				.when(pm.col("industry_id").equalTo(5),
						functions
								.greatest(
										functions.when(functions.isnull(upsellCharge),
												functions.when(functions.isnull(ezPayPrice), 0)
														.otherwise(ezPayColumnNull)),
										functions.when(functions.isnull(ezPayPrice),
												functions.when(functions.isnull(upsellCharge), 0)
														.otherwise(upSellChargeNull))))
				.otherwise(upsellCharge).as("otherChargePerNight");

		Column productGroupTypeVal = tpg.col("TYPE");
		Dataset<Row> result1 = result
				.select(pm.col("id"), pm.col("user_id").as("USER_ID"), pm.col("tenant_location_id"),
						pm.col("location_group_id"), pm.col("product_id"), pm.col("industry_id"),
						tl.col("hotelMetricsDataType").as("tenantLocationHotelMetricsDataType"),
						tl.col("productMetricsValidationType").as("tenantLocationProductMetricsValidationType"),
						lgproduct.col("isMajor"), lgproduct.col("isRecurring"),
						lgp.col("IS_DEFAULT").as("locGroupIsDefault"), lgp.col("IS_RETAIL").as("locGroupIsRetail"),
						lgp.col("IS_FOR_MEETING_AND_EVENTS").as("locGroupIsMeetingEvents"),
						pm.col("confirmationNumber"),
						functions.when(pm.col("industry_id").equalTo(5), pm.col("confirmationNumber"))
								.when(lgproduct.col("isMajor").equalTo("1"), functions.concat(lgproduct.col("isMajor"),
										functions.lit("_"), pm.col("confirmationNumber")))
								.otherwise(null).as("majorConfirmationNumber"),

						functions
								.when(lgproduct.col("isMajor").equalTo("0"),
										functions.concat(lgproduct.col("isMajor"), functions.lit("_"),
												pm.col("confirmationNumber")))
								.otherwise(null).as("minorConfirmationNumber"),

						functions.month(pm.col("arrivalDate")).as("arrivalMonth"),

						functions.year(pm.col("arrivalDate")).as("arrivalYear"), pm.col("arrivalDate"),

						functions.month(pm.col("departureDate")).as("departureMonth"),

						functions.year(pm.col("departureDate")).as("departureYear")

						, pm.col("departureDate"), functions.month(pm.col("businessDate")).as("businessMonth"),
						functions.year(pm.col("businessDate")).as("businessYear"), pm.col("businessDate"), ezPayPrice,
						upsellCharge, numberOfUpsellNights,

						functions.when(marketSeg.col("TYPE").isNull().or(marketSeg.col("TYPE").equalTo("")),
								functions.lit("-")).otherwise(marketSeg.col("TYPE")).as("MARKET_SEGMENT_TYPE"),

						upsellChargePerNight, pm.col("walkInReservation"), pm.col("roomNights"), productGroupTypeVal

						, numberOfOtherNights, otherChargePerNight, upsellChargePerNight.as("totalDailyUpsellAmount"),
						otherChargePerNight.as("totalDailyOtherAmount"), pm.col("id").as("entityID"),

						pm.col("auditstatus").as("auditstatus"), pm.col("originalRate").as("originalRate"),
						pm.col("newRate").as("newRate"), pm.col("originalRoomType").as("originalRoomType"),
						pm.col("newRoomType").as("newRoomType"), tpg.col("ID").as("productGroupID"),
						productGroupTypeVal.as("productGroupType"),
						pm.col("commissionableRevenue").as("commissionableRevenue"),
						pm.col("callOrders").as("callOrders"), pm.col("ticketsSold").as("ticketsSold"),
						pm.col("upsellReservations").as("upsellReservations"), pm.col("marketSegment1"),

						pm.col("marketSegment2")

				);

		List<Row> collectAsList = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value").collectAsList();
		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");
		List<Row> lmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM_%_DATE"))
				.select("datetime").collectAsList();
		System.out.println(lmDate.toString());

		Column auditStatus = result1.col("auditStatus");
		Column industry_id = pm.col("industry_id");
		Dataset<Row> result2 = result1.filter(auditStatus.notEqual("2").and(auditStatus.notEqual(4))
				.and(auditStatus.notEqual(5))
				.and((pm.col("industry_id").equalTo("1").or(pm.col("industry_id").equalTo("5"))
						.and(pm.col("arrivalDate").cast("String").isNotNull()
								.and(pm.col("departureDate").cast("String").isNotNull())))
										.or(industry_id.notEqual(1).and(industry_id.notEqual(5))
												.and(pm.col("arrivalDate").cast("String").isNotNull()
														.or(pm.col("departureDate").cast("String").isNotNull())))

				).and(pm.col("tenant_location_id").isin(id).or(pm.col("tenant_location_id").isNull()))
				.and(pm.col("arrivalDate").between(lmDate.get(0).toString().substring(1, 11),
						lmDate.get(1).toString().substring(1, 11)))
				.or(pm.col("departureDate").between(lmDate.get(0).toString().substring(1, 11),
						lmDate.get(1).toString().substring(1, 11)))
				.or(pm.col("businessDate").between(lmDate.get(0).toString().substring(1, 11),
						lmDate.get(1).toString().substring(1, 11))))
				.withColumn("locGroupIsFoodBeverage", functions.lit(0)).withColumn("entityType", functions.lit("5"));

		Dataset<Row> agg1 = result2.groupBy(result2.col("USER_ID"), result2.col("TENANT_LOCATION_ID"),
				result2.col("LOCATION_GROUP_ID"), result2.col("PRODUCT_ID"), result2.col("INDUSTRY_ID"),
				result2.col("tenantLocationHotelMetricsDataType"),
				result2.col("tenantLocationProductMetricsValidationType"), result2.col("isMajor"),
				result2.col("isRecurring"), result2.col("locGroupIsDefault"), result2.col("locGroupIsRetail"),
				result2.col("locGroupIsMeetingEvents"), result2.col("locGroupIsFoodBeverage"),
				result2.col("confirmationNumber"), result2.col("majorConfirmationNumber"),
				result2.col("minorConfirmationNumber"), result2.col("arrivalMonth"), result2.col("arrivalYear"),
				result2.col("arrivalDate"), result2.col("departureMonth"), result2.col("departureYear"),
				result2.col("departureDate"), result2.col("businessMonth"), result2.col("businessYear"),
				result2.col("businessDate"), result2.col("ezPayPrice"), result2.col("upsellCharge"),
				result2.col("numberOfUpsellNights"), result2.col("upsellChargePerNight"),
				result2.col("numberOfOtherNights"), result2.col("otherChargePerNight"),
				result2.col("totalDailyUpsellAmount"), result2.col("totalDailyOtherAmount"), result2.col("entityID"),
				result2.col("entityType"), result2.col("auditstatus"), result2.col("originalRate"),
				result2.col("newRate"), result2.col("originalRoomType"), result2.col("newRoomType"),
				result2.col("marketSegment1"), result2.col("marketSegment2"), result2.col("walkInReservation")

		).agg(functions
				.sum(functions.when(result2.col("tenantLocationHotelMetricsDataType").equalTo(1), otherChargePerNight)
						.when(result2.col("isRecurring").equalTo(0), otherChargePerNight)
						.otherwise(numberOfOtherNights.multiply(otherChargePerNight)))
				.as("totalOtherAmount"),
				functions.sum(functions
						.when(result2.col("tenantLocationHotelMetricsDataType").equalTo(1), otherChargePerNight)
						.when(result2.col("isRecurring").equalTo(0), otherChargePerNight)
						.otherwise(numberOfUpsellNights.multiply(otherChargePerNight))).as("totalUpsellAmount"),

				functions.first("productGroupType").as("productGroupType"),
				functions.first("productGroupId").as("productGroupId"),
//								functions.first(pm.col("walkInReservation")),

//								functions.first(functions.when(result2.col("marketSegment1").isNull().or(result2.col("marketSegment1").equalTo("")), "NA")
//								.otherwise(result2.col("marketSegment1"))).as("marketSegment1"),
//						functions.first(functions.when(result2.col("marketSegment2").isNull().or(result2.col("marketSegment2").equalTo("")), "NA")
//								.otherwise(result2.col("marketSegment2"))).as("marketSegment2"),
				functions
						.first(functions.when(result2.col("walkInReservation").equalTo("Y"), 1)
								.when(result2.col("walkInReservation").equalTo("N"), 0).otherwise(-1))
						.as("reservationType"),

				// for arrival upsell-
				functions.first(functions.when(result2.col("isMajor").equalTo(1), result2.col("confirmationNumber"))
						.otherwise(null)).as("arrivalUpsellsCnf"),
				functions.first(functions.when(productGroupTypeVal.equalTo("VU").and(result2.col("isMajor").equalTo(1)),
						result2.col("confirmationNumber")).otherwise(null)).as("arrivalVuUpsellsCnf"),
				functions.first(
						functions.when(productGroupTypeVal.equalTo("PROT").and(result2.col("isMajor").equalTo(1)),
								result2.col("confirmationNumber")).otherwise(null))
						.as("arrivalProtUpsellsCnf"),
				functions
						.first(functions.when(productGroupTypeVal.equalTo("OTH").and(result2.col("isMajor").equalTo(1)),
								result2.col("confirmationNumber")).otherwise(null))
						.as("arrivalOthUpsellsCnf"),

				// for daily upsell
				functions.first(functions.when(result2.col("isMajor").equalTo(1), result2.col("confirmationNumber"))
						.otherwise(null)).as("dailyUpsellsCnf"),
				functions.first(functions.when(productGroupTypeVal.equalTo("VU").and(result2.col("isMajor").equalTo(1)),
						result2.col("confirmationNumber")).otherwise(null)).as("dailyVuUpsellsCnf"),
				functions.first(
						functions.when(productGroupTypeVal.equalTo("PROT").and(result2.col("isMajor").equalTo(1)),
								result2.col("confirmationNumber")).otherwise(null))
						.as("dailyProtUpsellsCnf"),
				functions
						.first(functions.when(productGroupTypeVal.equalTo("OTH").and(result2.col("isMajor").equalTo(1)),
								result2.col("confirmationNumber")).otherwise(null))
						.as("dailyOthUpsellsCnf"),

				// for depature

				functions.first(functions.when(result2.col("isMajor").equalTo(1), result2.col("confirmationNumber"))
						.otherwise(null)).as("departureUpsellsCnf"),
				functions.first(functions.when(productGroupTypeVal.equalTo("VU").and(result2.col("isMajor").equalTo(1)),
						result2.col("confirmationNumber")).otherwise(null)).as("departureVuUpsellsCnf"),

				functions
						.first(functions
								.when(productGroupTypeVal.equalTo("PROT").and(result2.col("isMajor").equalTo(1)),
										result2.col("confirmationNumber"))
								.otherwise(null))
						.as("departureProtUpsellsCnf"),

				functions
						.first(functions.when(productGroupTypeVal.equalTo("OTH").and(result2.col("isMajor").equalTo(1)),
								result2.col("confirmationNumber")).otherwise(null))
						.as("departureOthUpsellsCnf")

				, functions.first(result2.col("id")).as("PM_ID"),
				functions.first(result2.col("callOrders")).as("callOrders"),
				functions.first(result2.col("ticketsSold")).as("ticketsSold"),
				functions.first(result2.col("commissionableRevenue")).as("commissionableRevenue"),
				functions.first(result2.col("upsellReservations")).as("upsellReservations"),

				functions.first(result2.col("MARKET_SEGMENT_TYPE")).as("MARKET_SEGMENT_TYPE")

		);

		Dataset<Row> finalResult = agg1.withColumn("marketSegment1",
				functions.when(agg1.col("marketSegment1").isNull().or(agg1.col("marketSegment1").equalTo("")), "NA")
						.otherwise(agg1.col("marketSegment1")).as("marketSegment1")

		).withColumn("marketSegment2",
				functions.when(agg1.col("marketSegment2").isNull().or(agg1.col("marketSegment2").equalTo("")), "NA")
						.otherwise(agg1.col("marketSegment2")).as("marketSegment2"))
				.drop("walkInReservation");

		finalResult.createOrReplaceTempView("upsell_tracker_temp");

		lgproduct.unpersist();
		tl.unpersist();
		lgp.unpersist();
		lg.unpersist();
		tpg.unpersist();
		stored_procedure_info.unpersist();

		logger.warn("-------------------------------Transform_upsell_tracker_table Completed ----------------------");
	}
}
