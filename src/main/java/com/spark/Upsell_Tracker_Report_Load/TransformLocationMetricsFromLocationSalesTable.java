package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformLocationMetricsFromLocationSalesTable {
	final static Logger logger = Logger.getLogger(TransformLocationMetricsFromLocationSalesTable.class);

	sqlConnection conn;
	SparkSession sparkSession;

	public TransformLocationMetricsFromLocationSalesTable(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void start() throws AnalysisException {
		logger.warn(
				"-------------------------------Transform Location Metrics From Location Sales Table started ----------------------");
		Dataset<Row> storedInfo = sparkSession.table("stored_procedure_info");

		Dataset<Row> locationMetric = sparkSession.table("location_metric");
//				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> locationSale = sparkSession.table("location_sales");
//		.persist(StorageLevel.MEMORY_ONLY_SER());
		List<Row> listIds = storedInfo.filter("field_name = 'TENANT_LOCATION_IDs'").select("field_value")
				.collectAsList();
		String ids = listIds.size() == 0 ? ""
				: listIds.get(0).toString().substring(1, listIds.get(0).toString().length() - 1);
		String id[] = ids.split(",");

//		String tempIds[] = "126047,160365,177220,176253,13547,126048,120649".split(",");
//		locationMetric.where(locationMetric.col("id").isin(tempIds)).select("id", "upsellMainRevenue", "upsells",
//				"upsellNights", "otherRevenue", "dailyRoomUpsellRevenue", "dailyOtherRevenue", "dailyRoomUpsells")
//				.show();

		locationSale = locationSale.withColumnRenamed("dailyOtherRevenue", "newDailyOtherRevenue")
				.withColumnRenamed("departureOtherRevenue", "newDepartureOtherRevenue")
				.withColumnRenamed("id", "newID");

//		locationSale.printSchema();

		Dataset<Row> joined = locationMetric
				.where(ids.isEmpty() ? functions.when(locationMetric.col("ID").isNotNull(), true)
						: locationMetric.col("TENANT_LOCATION_ID").isin(id))
				.join(locationSale,
						locationMetric.col("TENANT_LOCATION_ID").equalTo(locationSale.col("TENANT_LOCATION_ID"))
								.and(locationSale.col("metricDate").equalTo(locationMetric.col("metricDate"))))
				.withColumn("upsellMainRevenue", locationSale.col("arrivalUpsellRevenue"))
				.withColumn("upsells", locationSale.col("arrivalUpsells"))
				.withColumn("upsellNights", locationSale.col("arrivalUpsellNights"))
				.withColumn("otherRevenue", locationSale.col("arrivalOtherRevenue"))
				.withColumn("dailyRoomUpsellRevenue", locationSale.col("dailyUpsellRevenue"))
				.withColumn("dailyOtherRevenue", locationSale.col("newDailyOtherRevenue"))
				.withColumn("dailyRoomUpsells", locationSale.col("dailyUpsells"))
				.withColumn("departureRoomUpsellRevenue", locationSale.col("departureUpsellRevenue"))
				.withColumn("departureOtherRevenue", locationSale.col("newDepartureOtherRevenue"))
				.withColumn("departureRoomUpsells", locationSale.col("departureUpsells"))
				.withColumn("departureRoomUpsellNights", locationSale.col("departureUpsellNights"))
				.select("ID", "upsellMainRevenue", "upsells", "upsellNights", "otherRevenue", "dailyRoomUpsellRevenue",
						"dailyOtherRevenue", "dailyRoomUpsells", "departureRoomUpsellRevenue", "departureOtherRevenue",
						"departureRoomUpsells", "departureRoomUpsellNights")
				.withColumnRenamed("ID", "joinID").withColumnRenamed("upsellMainRevenue", "joinupsellMainRevenue")
				.withColumnRenamed("upsells", "joinupsells").withColumnRenamed("upsellNights", "joinupsellNights")
				.withColumnRenamed("otherRevenue", "joinotherRevenue")
				.withColumnRenamed("dailyRoomUpsellRevenue", "joindailyRoomUpsellRevenue")
				.withColumnRenamed("dailyOtherRevenue", "joindailyOtherRevenue")
				.withColumnRenamed("dailyRoomUpsells", "joindailyRoomUpsells")
				.withColumnRenamed("departureRoomUpsellRevenue", "joindepartureRoomUpsellRevenue")
				.withColumnRenamed("departureOtherRevenue", "joindepartureOtherRevenue")
				.withColumnRenamed("departureRoomUpsells", "joindepartureRoomUpsells")
				.withColumnRenamed("departureRoomUpsellNights", "joindepartureRoomUpsellNights")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> finalResult = locationMetric.join(joined, locationMetric.col("ID").equalTo(joined.col("joinID")),
				"left_outer");

		finalResult = finalResult
				.withColumn("upsellMainRevenue",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joinupsellMainRevenue"))
								.otherwise(finalResult.col("upsellMainRevenue")))
				.withColumn("upsells",
						functions.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
								finalResult.col("joinupsells")).otherwise(finalResult.col("upsells")))

				.withColumn("upsellNights",
						functions.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
								finalResult.col("joinupsellNights")).otherwise(finalResult.col("upsellNights")))
				.withColumn("otherRevenue",
						functions.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
								finalResult.col("joinotherRevenue")).otherwise(finalResult.col("otherRevenue")))
				.withColumn("dailyRoomUpsellRevenue",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindailyRoomUpsellRevenue"))
								.otherwise(finalResult.col("dailyRoomUpsellRevenue")))
				.withColumn("dailyOtherRevenue",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindailyOtherRevenue"))
								.otherwise(finalResult.col("dailyOtherRevenue")))
				.withColumn("dailyRoomUpsells",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindailyRoomUpsells"))
								.otherwise(finalResult.col("dailyRoomUpsells")))
				.withColumn("departureRoomUpsellRevenue",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindepartureRoomUpsellRevenue"))
								.otherwise(finalResult.col("departureRoomUpsellRevenue")))
				.withColumn("departureOtherRevenue",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindepartureOtherRevenue"))
								.otherwise(finalResult.col("departureOtherRevenue")))
				.withColumn("departureRoomUpsells",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindepartureRoomUpsells"))
								.otherwise(finalResult.col("departureRoomUpsells")))
				.withColumn("departureRoomUpsellNights",
						functions
								.when(finalResult.col("ID").equalTo(finalResult.col("joinID")),
										finalResult.col("joindepartureRoomUpsellNights"))
								.otherwise(finalResult.col("departureRoomUpsellNights")))
				.drop("joinupsellMainRevenue", "joinupsells", "joinupsellNights", "joinotherRevenue",
						"joindailyRoomUpsellRevenue", "joindailyOtherRevenue", "joindailyRoomUpsells",
						"joindepartureRoomUpsellRevenue", "joindepartureOtherRevenue", "joindepartureRoomUpsells",
						"joindepartureRoomUpsellNights", "joinID");

		conn.sparkSqlWrite(finalResult, "location_metric");
		finalResult.createOrReplaceTempView("location_metric");

		logger.warn(
				"-------------------------------Transform Location Metrics From Location Sales Table ended ----------------------");
	}

}
