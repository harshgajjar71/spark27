package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformTempTablesToMainTable {
	final static Logger logger = Logger.getLogger(TransformTempTablesToMainTable.class);

	sqlConnection sparkSql;
	SparkSession sparkSession;

	public TransformTempTablesToMainTable(sqlConnection conn) {

		sparkSql = conn;
		sparkSession = conn.getSpark();

	}

	public void start(int ISFULLPROCESS) {
//		Dataset<Row> sales = sparkSession.table("salesTemp").persist(StorageLevel.MEMORY_ONLY_SER());
//		logger.warn(sales.count() + "sales count for UDF ");
//		sales.printSchema();
//		checkProcess(ISFULLPROCESS, sales);

		Dataset<Row> factLocation = sparkSession.table("fact_location_temp");
		sparkSql.sparkSqlWrite(factLocation, "fact_location");
		Dataset<Row> factUser = sparkSession.table("fact_user_temp");
		sparkSql.sparkSqlWrite(factUser, "fact_user");
		Dataset<Row> salesTemp = sparkSql.dbSession("sales").persist(StorageLevel.MEMORY_ONLY_SER());
		udf(ISFULLPROCESS, salesTemp);
	}

	public void checkProcess(int ISFULLPROCESS, Dataset<Row> salesTemp) {
		Dataset<Row> stored_procedure_info = sparkSql.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		List<Row> collectAsList = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value").collectAsList();
		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");

		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
				.select("datetime").collectAsList();

		if (ISFULLPROCESS == 1) {
			// do nothing
// if process is full then we can truncate sales and sales_upsell_cnt			

			sparkSql.sparkSqlWrite(salesTemp, "sales");
//			upSellCnt.createOrReplaceTempView("sales_upsell_cnt");

		}
//		else {
//			Dataset<Row> upSellCnt = sparkSession.table("sales_upsell_cnt");
//			Dataset<Row> partialDataUpSellCnt = upSellCnt
//					.join(sales, upSellCnt.col("sales_id").equalTo(sales.col("id")))
//					.where(functions.not(sales.col("metricDate")
//							.between(pmDate.get(0).toString().substring(1, 11),
//									pmDate.get(1).toString().substring(1, 11))
//							.and(collectAsList.isEmpty() ? functions.when(sales.col("id").isNotNull(), true)
//									: sales.col("tenant_location_id").isin(id))));
//
//			Dataset<Row> partialSales = sales.where(functions.not(sales.col("metricDate")
//					.between(pmDate.get(0).toString().substring(1, 11), pmDate.get(1).toString().substring(1, 11))
//					.and(collectAsList.isEmpty() ? functions.when(sales.col("id").isNotNull(), true)
//							: sales.col("tenant_location_id").isin(id))));
//			partialDataUpSellCnt.createOrReplaceTempView("sales_upsell_cnt");
//
//			// logic behind this :-
//			// DELETE FROM sales WHERE (metricDate BETWEEN GETMETRICSTARTDATE() AND
//			// GETMETRICENDDATE())
//			// AND (FIND_IN_SET(`TENANT_LOCATION_ID`, GETTENANTLOCATIONIDS())
//			// OR (GETTENANTLOCATIONIDS() = ''));
//
//			sales.unionByName(partialSales).createOrReplaceTempView("salesTemp");
//
//		}

	}

	private void udf(int ISFULLPROCESS, Dataset<Row> sales) {

		Dataset<Row> sales_udf = sales

				.select(sales.col("arrivalUpsellsCnf"), sales.col("dailyUpsellsCnf"), sales.col("departureUpsellSCnf"),
						sales.col("id"), sales.col("metricDate"), sales.col("arrivalVuUpsellsCnf"),
						sales.col("arrivalProtUpsellsCnf"), sales.col("arrivalOthUpsellsCnf"),
						sales.col("dailyVuUpsellsCnf"), sales.col("dailyProtUpsellsCnf"),
						sales.col("dailyOthUpsellsCnf"), sales.col("departureVuUpsellsCnf"),
						sales.col("departureProtUpsellsCnf"), sales.col("departureOthUpsellsCnf"), sales.col("USER_ID"),
						sales.col("month"))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		sales_udf.createOrReplaceTempView("sales_udf");

		Dataset<Row> sales_upsell_cnt_spark = sparkSql.spSession("sales_upsell_cnt_spark");
		sales_upsell_cnt_spark.createOrReplaceTempView("upsell_cnt");
		UDF udf = new UDF();

		for (int i = 1; i <= 12; i++) {
			if (i == 6) {
				break;
			} else {

				udf.UDFFULL("arrivalUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("arrivalVuUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("arrivalProtUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("arrivalOthUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("dailyUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("dailyVuUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("dailyProtUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("dailyOthUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("departureUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("departureVuUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("departureProtUpsells", ISFULLPROCESS, i);
				udf.UDFFULL("departureOthUpsells", ISFULLPROCESS, i);
			}
		}
		logger.warn("Transform_temp_tables_to_main_table end");

		sales_udf.unpersist();

		sparkSession.catalog().dropTempView("upsell_cnt");
		sparkSession.catalog().dropTempView("sales_udf");

	}
}
