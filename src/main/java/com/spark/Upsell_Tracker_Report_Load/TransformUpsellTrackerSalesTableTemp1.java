package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformUpsellTrackerSalesTableTemp1 {
	final static Logger logger = Logger.getLogger(TransformUpsellTrackerSalesTableTemp1.class);

	sqlConnection conn;
	SparkSession sparkSession;

	public TransformUpsellTrackerSalesTableTemp1(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void transform_upsell_tracker_sales_table_temp1() throws AnalysisException {
		logger.warn(
				"-------------------------------Transform Upsell Tracker Sales Table Temp1 started ----------------------");

		Dataset<Row> storedInfo = conn.dbSession("stored_procedure_info").persist(StorageLevel.MEMORY_ONLY_SER());
		;

		Dataset<Row> departureData = sparkSession.table("upsell_tracker_temp");
//			
		Dataset<Row> utuInner = sparkSession.table("upsell_tracker_location_metrics_unique_view");
//			

		List<Row> pmStartDate = storedInfo.where(storedInfo.col("field_name").equalTo("PM_START_DATE"))
				.select("datetime").collectAsList();

		Column departureUpsellsCnf = functions.concat_ws(" , ", departureData.select("departureUpsellsCnf")
				.dropDuplicates("departureUpsellsCnf").col("departureUpsellsCnf")).as("departureUpsellsCnf");
		Column departureVuUpsellsCnf = functions.concat_ws(" , ", departureData.select("departureVuUpsellsCnf")
				.dropDuplicates("departureVuUpsellsCnf").col("departureVuUpsellsCnf")).as("departureVuUpsellsCnf");

		// as mention in in_gauge-SP-FULL-PROCESS distinct removed
		Column departureProtUpsellsCnf = functions.concat_ws(" , ", departureData.col("departureProtUpsellsCnf"))
				.as("departureProtUpsellsCnf");
		Column departureOthUpsellsCnf = functions
				.concat_ws(" , ", departureData.select("departureOthUpsellsCnf")
						.dropDuplicates("departureOthUpsellsCnf").col("departureOthUpsellsCnf"))
				.as("departureOthUpsellsCnf");

		Dataset<Row> result1 = departureData
				.join(utuInner,
						utuInner.col("TENANT_LOCATION_ID").equalTo(departureData.col("TENANT_LOCATION_ID"))
								.and(departureData.col("departureDate").equalTo(utuInner.col("metricDate")))
								.and(departureData.col("locGroupIsRetail").equalTo("0")))
				.where(utuInner.col("metricDate").$greater$eq(pmStartDate.get(0).toString().substring(1, 11)));

		Dataset<Row> result2 = result1.select(departureData.col("USER_ID"), utuInner.col("TENANT_LOCATION_ID"),
				departureData.col("LOCATION_GROUP_ID"), departureData.col("PRODUCT_ID"), utuInner.col("metricDate"),
				departureData.col("marketSegment1"), departureData.col("marketSegment2"),
				departureData.col("reservationType"), departureData.col("tenantLocationHotelMetricsDataType"),
				departureData.col("upsellChargePerNight"), departureData.col("totalUpsellAmount"),
				departureData.col("otherChargePerNight"), departureData.col("totalOtherAmount"),
				departureData.col("isMajor"), departureData.col("numberOfUpsellNights"),
				departureData.col("numberOfOtherNights"), departureData.col("majorConfirmationNumber"),
				departureData.col("minorConfirmationNumber"), departureData.col("commissionableRevenue"),
				departureData.col("callOrders"), departureData.col("ticketsSold"),
				departureData.col("productGroupType"), departureData.col("upsellReservations"), departureOthUpsellsCnf,
				departureProtUpsellsCnf, departureUpsellsCnf, departureVuUpsellsCnf,
				departureData.col("MARKET_SEGMENT_TYPE"));

		Dataset<Row> result3 = result2
				.groupBy(departureData.col("USER_ID"), utuInner.col("TENANT_LOCATION_ID"),
						departureData.col("LOCATION_GROUP_ID"), departureData.col("PRODUCT_ID"),
						utuInner.col("metricDate"), departureData.col("marketSegment1"),
						departureData.col("marketSegment2"), departureData.col("reservationType"),
						result2.col("MARKET_SEGMENT_TYPE"))

				.agg(functions.month(result2.col("metricDate")).as("month"),
						functions.year(result2.col("metricDate")).as("year"),
						functions.first(result2.col("departureUpsellsCnf")).as("departureUpsellsCnf"),
						functions.first(result2.col("departureVuUpsellsCnf")).as("departureVuUpsellsCnf"),
						functions.first(result2.col("departureProtUpsellsCnf")).as("departureProtUpsellsCnf"),
						functions.first(result2.col("departureOthUpsellsCnf")).as("departureOthUpsellsCnf"),

						functions.round(functions.sum(functions
								.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
										result2.col("upsellChargePerNight"))
								.otherwise(result2.col("totalUpsellAmount"))), 2).as("departureUpsellRevenue"),
						functions
								.round(functions.sum(functions
										.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
												result2.col("otherChargePerNight"))
										.otherwise(result2.col("totalOtherAmount"))), 2)
								.as("departureOtherRevenue"),
						functions.sum(functions
								.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
										functions.when(result2.col("isMajor").equalTo("1"), "1").otherwise("0"))
								.otherwise(result2.col("numberOfUpsellNights"))).as("departureUpsellNights"),
						functions.sum(functions
								.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
										functions.when(result2.col("isMajor").equalTo("0"), "1").otherwise("0"))
								.otherwise(result2.col("numberOfOtherNights"))).as("departureOtherNights"),

						functions.countDistinct(result2.col("majorConfirmationNumber")).as("departureUpsells"),
						functions.countDistinct(result2.col("minorConfirmationNumber")).as("departureOtherUpsells"),

						functions.round(functions.sum(result2.col("commissionableRevenue")), 2)
								.as("departureCommissionableRevenue"),
						functions.round(functions.sum(result2.col("callOrders")), 2).as("departureCallOrders"),
						functions.round(functions.sum(result2.col("ticketsSold")), 2).as("departureTicketsSold"),
						functions.round(functions.sum(result2.col("upsellReservations")), 2)
								.as("departureUpsellReservations"),

						functions.when(functions.first(result2.col("productGroupType").equalTo("VU")),
								functions.round(
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("upsellChargePerNight"))
												.otherwise(result2.col("totalUpsellAmount"))),
										2))
								.otherwise("0").as("departureVuUpsellRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("otherChargePerNight"))
												.otherwise(result2.col("totalOtherAmount"))), 2))
								.otherwise("0").as("departureVuOtherRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														functions.when(result2.col("isMajor").equalTo("1"), "1")
																.otherwise("0"))
												.otherwise(result2.col("numberOfUpsellNights"))))
								.otherwise("0").as("departureVuUpsellNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														functions.when(result2.col("isMajor").equalTo("0"), "1")
																.otherwise("0"))
												.otherwise(result2.col("numberOfOtherNights"))))
								.otherwise("0").as("departureVuOtherNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.countDistinct(result2.col("majorConfirmationNumber")))
								.otherwise("0").as("departureVuUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.countDistinct(result2.col("minorConfirmationNumber")))
								.otherwise("0").as("departureVuOtherUpsells"),

						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("upsellChargePerNight"))
												.otherwise(result2.col("totalUpsellAmount"))), 2))
								.otherwise("0").as("departureProtUpsellRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("otherChargePerNight"))
												.otherwise(result2.col("totalOtherAmount"))), 2))
								.otherwise("0").as("departureProtOtherRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														functions.when(result2.col("isMajor").equalTo("1"), "1")
																.otherwise("0"))
												.otherwise(result2.col("numberOfUpsellNights"))))
								.otherwise("0").as("departureProtUpsellNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														functions.when(result2.col("isMajor").equalTo("0"), "1")
																.otherwise("0"))
												.otherwise(result2.col("numberOfOtherNights"))))
								.otherwise("0").as("departureProtOtherNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.countDistinct(result2.col("majorConfirmationNumber")))
								.otherwise("0").as("departureProtUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.countDistinct(result2.col("minorConfirmationNumber")))
								.otherwise("0").as("departureProtOtherUpsells"),
						functions.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
								functions.round(
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("upsellChargePerNight"))
												.otherwise(result2.col("totalUpsellAmount"))),
										2))
								.otherwise("0").as("departureOthUpsellRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("otherChargePerNight"))
												.otherwise(result2.col("totalOtherAmount"))), 2))
								.otherwise("0").as("departureOthOtherRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														functions.when(result2.col("isMajor").equalTo("1"), "1")
																.otherwise("0"))
												.otherwise(result2.col("numberOfUpsellNights"))))
								.otherwise("0").as("departureOthUpsellNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														functions.when(result2.col("isMajor").equalTo("0"), "1")
																.otherwise("0"))
												.otherwise(result2.col("numberOfOtherNights"))))
								.otherwise("0").as("departureOthOtherNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.countDistinct(result2.col("majorConfirmationNumber")))
								.otherwise("0").as("departureOthUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.countDistinct(result2.col("minorConfirmationNumber")))
								.otherwise("0").as("departureOthOtherUpsells"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("T")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("upsellChargePerNight"))
												.otherwise(result2.col("totalUpsellAmount"))), 2))
								.otherwise("0").as("departureUpsellTranRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("T")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("otherChargePerNight"))
												.otherwise(result2.col("totalOtherAmount"))), 2))
								.otherwise("0").as("departureOtherTranRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("G")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("upsellChargePerNight"))
												.otherwise(result2.col("totalUpsellAmount"))), 2))
								.otherwise("0").as("departureUpsellGroupRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("G")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("otherChargePerNight"))
												.otherwise(result2.col("totalOtherAmount"))), 2))
								.otherwise("0").as("departureOtherGroupRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("P")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("upsellChargePerNight"))
												.otherwise(result2.col("totalUpsellAmount"))), 2))
								.otherwise("0").as("departureUpsellPerRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("P")),
										functions.round(functions.sum(functions
												.when(result2.col("tenantLocationHotelMetricsDataType").equalTo("1"),
														result2.col("otherChargePerNight"))
												.otherwise(result2.col("totalOtherAmount"))), 2))
								.otherwise("0").as("departureOtherPerRevenue")

				).withColumn("arrivalUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalVuUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalOthUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyVuUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyOthUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("arrivalUpsellRevenue", functions.lit(0))
				.withColumn("arrivalOtherRevenue", functions.lit(0)).withColumn("arrivalUpsellNights", functions.lit(0))
				.withColumn("arrivalOtherNights", functions.lit(0)).withColumn("arrivalUpsells", functions.lit(0))
				.withColumn("arrivalOtherUpsells", functions.lit(0))
				.withColumn("arrivalVuUpsellRevenue", functions.lit(0))
				.withColumn("arrivalVuOtherRevenue", functions.lit(0))
				.withColumn("arrivalVuUpsellNights", functions.lit(0))
				.withColumn("arrivalVuOtherNights", functions.lit(0)).withColumn("arrivalVuUpsells", functions.lit(0))
				.withColumn("arrivalVuOtherUpsells", functions.lit(0))
				.withColumn("arrivalProtUpsellRevenue", functions.lit(0))
				.withColumn("arrivalProtOtherRevenue", functions.lit(0))
				.withColumn("arrivalProtUpsellNights", functions.lit(0))
				.withColumn("arrivalProtOtherNights", functions.lit(0))
				.withColumn("arrivalProtUpsells", functions.lit(0))
				.withColumn("arrivalProtOtherUpsells", functions.lit(0))
				.withColumn("arrivalOthUpsellRevenue", functions.lit(0))
				.withColumn("arrivalOthOtherRevenue", functions.lit(0))
				.withColumn("arrivalOthUpsellNights", functions.lit(0))
				.withColumn("arrivalOthOtherNights", functions.lit(0)).withColumn("arrivalOthUpsells", functions.lit(0))
				.withColumn("arrivalOthOtherUpsells", functions.lit(0))
				.withColumn("arrivalCommissionableRevenue", functions.lit(0))
				.withColumn("arrivalCallOrders", functions.lit(0)).withColumn("arrivalTicketsSold", functions.lit(0))
				.withColumn("arrivalUpsellReservations", functions.lit(0))
				.withColumn("dailyUpsellRevenue", functions.lit(0)).withColumn("dailyOtherRevenue", functions.lit(0))
				.withColumn("dailyUpsellNights", functions.lit(0)).withColumn("dailyOtherNights", functions.lit(0))
				.withColumn("dailyUpsells", functions.lit(0)).withColumn("dailyOtherUpsells", functions.lit(0))
				.withColumn("dailyVuUpsellRevenue", functions.lit(0))
				.withColumn("dailyVuOtherRevenue", functions.lit(0)).withColumn("dailyVuUpsellNights", functions.lit(0))
				.withColumn("dailyVuOtherNights", functions.lit(0)).withColumn("dailyVuUpsells", functions.lit(0))
				.withColumn("dailyVuOtherUpsells", functions.lit(0))
				.withColumn("dailyProtUpsellRevenue", functions.lit(0))
				.withColumn("dailyProtOtherRevenue", functions.lit(0))
				.withColumn("dailyProtUpsellNights", functions.lit(0))
				.withColumn("dailyProtOtherNights", functions.lit(0)).withColumn("dailyProtUpsells", functions.lit(0))
				.withColumn("dailyProtOtherUpsells", functions.lit(0))
				.withColumn("dailyOthUpsellRevenue", functions.lit(0))
				.withColumn("dailyOthOtherRevenue", functions.lit(0))
				.withColumn("dailyOthUpsellNights", functions.lit(0))
				.withColumn("dailyOthOtherNights", functions.lit(0)).withColumn("dailyOthUpsells", functions.lit(0))
				.withColumn("dailyOthOtherUpsells", functions.lit(0))
				.withColumn("dailyCommissionableRevenue", functions.lit(0))
				.withColumn("dailyCallOrders", functions.lit(0)).withColumn("dailyTicketsSold", functions.lit(0))
				.withColumn("dailyUpsellReservations", functions.lit(0))
				.withColumn("arrivalUpsellTranRevenue", functions.lit(0))
				.withColumn("arrivalOtherTranRevenue", functions.lit(0))
				.withColumn("arrivalUpsellGroupRevenue", functions.lit(0))
				.withColumn("arrivalOtherGroupRevenue", functions.lit(0))
				.withColumn("arrivalUpsellPerRevenue", functions.lit(0))
				.withColumn("arrivalOtherPerRevenue", functions.lit(0))

				.withColumn("dailyUpsellTranRevenue", functions.lit(0))
				.withColumn("dailyOtherTranRevenue", functions.lit(0))
				.withColumn("dailyUpsellGroupRevenue", functions.lit(0))
				.withColumn("dailyOtherGroupRevenue", functions.lit(0))
				.withColumn("dailyUpsellPerRevenue", functions.lit(0))
				.withColumn("dailyOtherPerRevenue", functions.lit(0)).drop("MARKET_SEGMENT_TYPE");

		result3.createOrReplaceTempView("salesTemp_1");

		logger.warn(
				"-------------------------------Transform Upsell Tracker Sales Table Temp1 ended ----------------------");
	}
}