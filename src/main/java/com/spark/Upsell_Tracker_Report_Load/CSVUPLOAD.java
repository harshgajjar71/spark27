package com.spark.Upsell_Tracker_Report_Load;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.time.LocalTime;

public class CSVUPLOAD {
	public void connect() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/testspark", "root",
					"root");

			Statement stmt = con.createStatement();

			Path path = Paths.get("D:\\sales3.csv");
			String trun = " SET UNIQUE_CHECKS=0; ";
			System.out.println("truncate sales");
			stmt.execute(trun);
			String query1 = "load data   infile '" + path + "' into table sales " + "FIELDS TERMINATED BY ','"
					+ " ENCLOSED BY '\"' LINES TERMINATED BY '\\n' ";
			System.out.println(query1 + LocalTime.now());
			

//			stmt.execute(query1);
//
			String loadQuery = "LOAD DATA  INFILE '" + "D:\\sales3.csv"
					+ "' INTO TABLE sales FIELDS TERMINATED BY ','"
					+ " LINES TERMINATED BY '\n' ("+ 
					"USER_ID," + 
					"DIM_USER_ID," + 
					"TENANT_LOCATION_ID," + 
					"REGION_ID," + 
					"DIM_TENANT_LOCATION_ID," + 
					"LOCATION_GROUP_ID," + 
					"DIM_LOCATION_GROUP_ID," + 
					"PRODUCT_ID," + 
					"DIM_PRODUCT_ID," + 
					"metricDate," + 
					"DIM_METRIC_DATE_ID," + 
					"arrivalUpsellRevenue," + 
					"arrivalOtherRevenue," + 
					"arrivalUpsellNights," + 
					"arrivalOtherNights," + 
					"arrivalUpsells," + 
					"arrivalOtherUpsells," + 
					"dailyUpsellRevenue," + 
					"dailyOtherRevenue," + 
					"dailyUpsellNights," + 
					"dailyOtherNights," + 
					"dailyUpsells," + 
					"dailyOtherUpsells," + 
					"departureUpsellRevenue," + 
					"departureOtherRevenue," + 
					"departureUpsellNights," + 
					"departureOtherNights," + 
					"departureUpsells," + 
					"departureOtherUpsells," + 
					"dailyVuUpsellRevenue," + 
					"dailyVuOtherRevenue," + 
					"dailyVuUpsellNights," + 
					"dailyVuOtherNights," + 
					"dailyVuUpsells," + 
					"dailyVuOtherUpsells," + 
					"dailyProtUpsellRevenue," + 
					"dailyProtOtherRevenue," + 
					"dailyProtUpsellNights," + 
					"dailyProtOtherNights," + 
					"dailyProtUpsells," + 
					"dailyProtOtherUpsells," + 
					"dailyOthUpsellRevenue," + 
					"dailyOthOtherRevenue," + 
					"dailyOthUpsellNights," + 
					"dailyOthOtherNights," + 
					"dailyOthUpsells," + 
					"dailyOthOtherUpsells," + 
					"dailyCommissionableRevenue," + 
					"arrivalVuUpsellRevenue," + 
					"arrivalVuOtherRevenue," + 
					"arrivalVuUpsellNights," + 
					"arrivalVuOtherNights," + 
					"arrivalVuUpsells," + 
					"arrivalVuOtherUpsells," + 
					"arrivalProtUpsellRevenue," + 
					"arrivalProtOtherRevenue," + 
					"arrivalProtUpsellNights," + 
					"arrivalProtOtherNights," + 
					"arrivalProtUpsells," + 
					"arrivalProtOtherUpsells," + 
					"arrivalOthUpsellRevenue," + 
					"arrivalOthOtherRevenue," + 
					"arrivalOthUpsellNights," + 
					"arrivalOthOtherNights," + 
					"arrivalOthUpsells," + 
					"arrivalOthOtherUpsells," + 
					"arrivalCommissionableRevenue," + 
					"departureVuUpsellRevenue," + 
					"departureVuOtherRevenue," + 
					"departureVuUpsellNights," + 
					"departureVuOtherNights," + 
					"departureVuUpsells," + 
					"departureVuOtherUpsells," + 
					"departureProtUpsellRevenue," + 
					"departureProtOtherRevenue," + 
					"departureProtUpsellNights," + 
					"departureProtOtherNights," + 
					"departureProtUpsells," + 
					"departureProtOtherUpsells," + 
					"departureOthUpsellRevenue," + 
					"departureOthOtherRevenue," + 
					"departureOthUpsellNights," + 
					"departureOthOtherNights," + 
					"departureOthUpsells," + 
					"departureOthOtherUpsells," + 
					"departureCommissionableRevenue," + 
					"arrivalCallOrders," + 
					"departureCallOrders," + 
					"dailyCallOrders," + 
					"arrivalTicketsSold," + 
					"departureTicketsSold," + 
					"dailyTicketsSold," + 
					"arrivalUpsellReservations," + 
					"departureUpsellReservations," + 
					"dailyUpsellReservations," + 
					"month," + 
					"year," + 
					"marketSegment1," + 
					"marketSegment2," + 
					"reservationType," + 
					"arrivalUpsellsCnf," + 
					"arrivalVuUpsellsCnf," + 
					"arrivalProtUpsellsCnf," + 
					"arrivalOthUpsellsCnf," + 
					"dailyUpsellsCnf," + 
					"dailyVuUpsellsCnf," + 
					"dailyProtUpsellsCnf," + 
					"dailyOthUpsellsCnf," + 
					"departureUpsellsCnf," + 
					"departureVuUpsellsCnf," + 
					"departureProtUpsellsCnf," + 
					"departureOthUpsellsCnf," + 
					"arrivalUpsellTranRevenue," + 
					"arrivalOtherTranRevenue," + 
					"departureUpsellTranRevenue," + 
					"departureOtherTranRevenue," + 
					"dailyUpsellTranRevenue," + 
					"dailyOtherTranRevenue," + 
					"arrivalUpsellGroupRevenue," + 
					"arrivalOtherGroupRevenue," + 
					"departureUpsellGroupRevenue," + 
					"departureOtherGroupRevenue," + 
					"dailyUpsellGroupRevenue," + 
					"dailyOtherGroupRevenue," + 
					"arrivalUpsellPerRevenue," + 
					"arrivalOtherPerRevenue," + 
					"departureUpsellPerRevenue," + 
					"departureOtherPerRevenue," + 
					"dailyUpsellPerRevenue," + 
					"dailyOtherPerRevenue) ";
			
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
