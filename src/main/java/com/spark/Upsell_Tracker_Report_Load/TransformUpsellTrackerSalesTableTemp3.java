package com.spark.Upsell_Tracker_Report_Load;

import static org.apache.spark.sql.functions.col;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformUpsellTrackerSalesTableTemp3 {
	final static Logger logger = Logger.getLogger(TransformUpsellTrackerSalesTableTemp3.class);

	sqlConnection conn;
	SparkSession sparkSession;

	public TransformUpsellTrackerSalesTableTemp3(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void transform_upsell_tracker_sales_table_temp3() throws AnalysisException {
		logger.warn(
				"-------------------------------Transform Upsell Tracker Sales Table Temp3 started ----------------------");

		Dataset<Row> storedInfo = conn.dbSession("stored_procedure_info").persist(StorageLevel.MEMORY_ONLY_SER());
		storedInfo.createOrReplaceTempView("stored_procedure_info");
		Dataset<Row> arrivalDailyData = sparkSession.table("arrival_data_view_daily")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> arrivalNonDailyData = sparkSession.table("arrival_data_view_non_daily")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		sparkSession.catalog().dropTempView("arrival_data_view_daily");
		sparkSession.catalog().dropTempView("arrival_data_view_non_daily");

		Dataset<Row> utuInner = sparkSession.table("upsell_tracker_location_metrics_unique_view")
				.withColumnRenamed("TENANT_LOCATION_ID", "tenant_location_id_temp")
				.withColumnRenamed("metricDate", "metricDate_temp").persist(StorageLevel.MEMORY_ONLY_SER());

		List<Row> pmStartDate = storedInfo.where(storedInfo.col("field_name").equalTo("PM_START_DATE"))
				.select("datetime").collectAsList();

		Dataset<Row> arrivalData = arrivalDailyData.union(arrivalNonDailyData);

		Column arrivalUpsellsCnf = functions.concat_ws(" , ",
				arrivalData.select("arrivalUpsellsCnf").dropDuplicates("arrivalUpsellsCnf").col("arrivalUpsellsCnf"))
				.as("arrivalUpsellsCnf");
		Column arrivalVuUpsellsCnf = functions.concat_ws(" , ", arrivalData.select("arrivalVuUpsellsCnf")
				.dropDuplicates("arrivalVuUpsellsCnf").col("arrivalVuUpsellsCnf")).as("arrivalVuUpsellsCnf");
		Column arrivalProtUpsellsCnf = functions.concat_ws(" , ", arrivalData.select("arrivalProtUpsellsCnf")
				.dropDuplicates("arrivalProtUpsellsCnf").col("arrivalProtUpsellsCnf")).as("arrivalProtUpsellsCnf");
		Column arrivalOthUpsellsCnf = functions.concat_ws(" , ", arrivalData.select("arrivalOthUpsellsCnf")
				.dropDuplicates("arrivalOthUpsellsCnf").col("arrivalOthUpsellsCnf")).as("arrivalOthUpsellsCnf");

		// exception
		// rename column Tenant_location_id, metricDate

		Dataset<Row> result1 = utuInner
				.where(utuInner.col("metricDate_temp").$greater$eq(pmStartDate.get(0).toString().substring(1, 11)))
				.join(arrivalData,
						utuInner.col("tenant_location_id_temp").equalTo(arrivalData.col("TENANT_LOCATION_ID"))
								.and(arrivalData.col("metricDate").equalTo(utuInner.col("metricDate_temp"))))
				.drop("tenant_location_id", "metricDate");

		Dataset<Row> result2 = result1.select(result1.col("USER_ID"),
				result1.col("tenant_location_id_temp").as("TENANT_LOCATION_ID"), result1.col("LOCATION_GROUP_ID"),
				result1.col("PRODUCT_ID"), result1.col("metricDate_temp").as("metricDate"),
				result1.col("marketSegment1"), result1.col("marketSegment2"), result1.col("reservationType"),
				result1.col("arrivalUpsellRevenue"), result1.col("arrivalOtherRevenue"),
				result1.col("arrivalUpsellNights"), result1.col("arrivalOtherNights"), result1.col("arrivalUpsells"),
				result1.col("arrivalOtherUpsells"), result1.col("productGroupType"), result1.col("ticketsSold"),
				result1.col("commissionableRevenue"), arrivalData.col("callOrders"), result1.col("upsellReservations"),
				arrivalUpsellsCnf, arrivalVuUpsellsCnf, arrivalProtUpsellsCnf, arrivalOthUpsellsCnf,

				result1.col("MARKET_SEGMENT_TYPE"));

		Dataset<Row> result3 = result2
				.groupBy(arrivalData.col("USER_ID"), result2.col("TENANT_LOCATION_ID"),
						arrivalData.col("LOCATION_GROUP_ID"), arrivalData.col("PRODUCT_ID"), result2.col("metricDate"),
						arrivalData.col("marketSegment1"), arrivalData.col("marketSegment2"),
						arrivalData.col("reservationType"), arrivalData.col("MARKET_SEGMENT_TYPE"))

				.agg(functions.month(result2.col("metricDate")).as("month"),
						functions.year(result2.col("metricDate")).as("year"),
						functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2)
								.as("arrivalUpsellRevenue"),
						functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2).as("arrivalOtherRevenue"),
						functions.sum(result2.col("arrivalUpsellNights")).as("arrivalUpsellNights"),
						functions.sum(result2.col("arrivalOtherNights")).as("arrivalOtherNights"),
						functions.sum(result2.col("arrivalUpsells")).as("arrivalUpsells"),
						functions.sum(result2.col("arrivalOtherUpsells")).as("arrivalOtherUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2))
								.otherwise("0").as("arrivalVuUpsellRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2))
								.otherwise("0").as("arrivalVuOtherRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.sum(result2.col("arrivalUpsellNights")))
								.otherwise("0").as("arrivalVuUpsellNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.sum(result2.col("arrivalOtherNights")))
								.otherwise("0").as("arrivalVuOtherNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.sum(result2.col("arrivalUpsells")))
								.otherwise("0").as("arrivalVuUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("VU")),
										functions.sum(result2.col("arrivalOtherUpsells")))
								.otherwise("0").as("arrivalVuOtherUpsells"),

						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2))
								.otherwise("0").as("arrivalProtUpsellRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2))
								.otherwise("0").as("arrivalProtOtherRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.sum(result2.col("arrivalUpsellNights")))
								.otherwise("0").as("arrivalProtUpsellNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.sum(result2.col("arrivalOtherNights")))
								.otherwise("0").as("arrivalProtOtherNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.sum(result2.col("arrivalUpsells")))
								.otherwise("0").as("arrivalProtUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("PROT")),
										functions.sum(result2.col("arrivalOtherUpsells")))
								.otherwise("0").as("arrivalProtOtherUpsells"),

						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2))
								.otherwise("0").as("arrivalOthUpsellRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2))
								.otherwise("0").as("arrivalOthOtherRevenue"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.sum(result2.col("arrivalUpsellNights")))
								.otherwise("0").as("arrivalOthUpsellNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.sum(result2.col("arrivalOtherNights")))
								.otherwise("0").as("arrivalOthOtherNights"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.sum(result2.col("arrivalUpsells")))
								.otherwise("0").as("arrivalOthUpsells"),
						functions
								.when(functions.first(result2.col("productGroupType").equalTo("OTH")),
										functions.sum(result2.col("arrivalOtherUpsells")))
								.otherwise("0").as("arrivalOthOtherUpsells"),

						functions.round(functions.sum(result2.col("commissionableRevenue")), 2)
								.as("arrivalCommissionableRevenue"),
						functions.round(functions.sum(result2.col("callOrders")), 2).as("arrivalCallOrders"),
						functions.round(functions.sum(result2.col("ticketsSold")), 2).as("arrivalTicketsSold"),
						functions.round(functions.sum(result2.col("upsellReservations")), 2)
								.as("arrivalUpsellReservations"),
						functions.first(functions.concat_ws(" , ", result2.col("arrivalUpsellsCnf")))
								.as("arrivalUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", result2.col("arrivalVuUpsellsCnf")))
								.as("arrivalVuUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", result2.col("arrivalProtUpsellsCnf")))
								.as("arrivalProtUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", result2.col("arrivalOthUpsellsCnf")))
								.as("arrivalOthUpsellsCnf"),

						functions.lit(0).as("departureUpsellTranRevenue"),
						functions.lit(0).as("departureOtherTranRevenue"),
						functions.lit(0).as("departureUpsellGroupRevenue"),
						functions.lit(0).as("departureOtherGroupRevenue"),
						functions.lit(0).as("departureUpsellPerRevenue"),
						functions.lit(0).as("departureOtherPerRevenue"),

						functions.lit(0).as("dailyUpsellTranRevenue"), functions.lit(0).as("dailyOtherTranRevenue"),
						functions.lit(0).as("dailyUpsellGroupRevenue"), functions.lit(0).as("dailyOtherGroupRevenue"),
						functions.lit(0).as("dailyUpsellPerRevenue"), functions.lit(0).as("dailyOtherPerRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("T")),
										functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2))
								.otherwise("0").as("arrivalUpsellTranRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("T")),
										functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2))
								.otherwise("0").as("arrivalOtherTranRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("G")),
										functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2))
								.otherwise("0").as("arrivalUpsellGroupRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("G")),
										functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2))
								.otherwise("0").as("arrivalOtherGroupRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("P")),
										functions.round(functions.sum(result2.col("arrivalUpsellRevenue")), 2))
								.otherwise("0").as("arrivalUpsellPerRevenue"),

						functions
								.when(functions.first(result2.col("MARKET_SEGMENT_TYPE").equalTo("P")),
										functions.round(functions.sum(result2.col("arrivalOtherRevenue")), 2))
								.otherwise("0").as("arrivalOtherPerRevenue"))
				.withColumn("dailyUpsellRevenue", functions.lit(0)).withColumn("dailyOtherRevenue", functions.lit(0))
				.withColumn("dailyUpsellNights", functions.lit(0)).withColumn("dailyOtherNights", functions.lit(0))
				.withColumn("dailyUpsells", functions.lit(0)).withColumn("dailyOtherUpsells", functions.lit(0))
				.withColumn("dailyVuUpsellRevenue", functions.lit(0))
				.withColumn("dailyVuOtherRevenue", functions.lit(0)).withColumn("dailyVuUpsellNights", functions.lit(0))
				.withColumn("dailyVuOtherNights", functions.lit(0)).withColumn("dailyVuUpsells", functions.lit(0))
				.withColumn("dailyVuOtherUpsells", functions.lit(0))

				.withColumn("dailyProtUpsellRevenue", functions.lit(0))
				.withColumn("dailyProtOtherRevenue", functions.lit(0))
				.withColumn("dailyProtUpsellNights", functions.lit(0))
				.withColumn("dailyProtOtherNights", functions.lit(0)).withColumn("dailyProtUpsells", functions.lit(0))
				.withColumn("dailyProtOtherUpsells", functions.lit(0))

				.withColumn("dailyOthUpsellRevenue", functions.lit(0))
				.withColumn("dailyOthOtherRevenue", functions.lit(0))
				.withColumn("dailyOthUpsellNights", functions.lit(0))
				.withColumn("dailyOthOtherNights", functions.lit(0)).withColumn("dailyOthUpsells", functions.lit(0))
				.withColumn("dailyOthOtherUpsells", functions.lit(0))

				.withColumn("dailyCommissionableRevenue", functions.lit(0))
				.withColumn("dailyCallOrders", functions.lit(0)).withColumn("dailyTicketsSold", functions.lit(0))
				.withColumn("dailyUpsellReservations", functions.lit(0))

				.withColumn("departureUpsellRevenue", functions.lit(0))
				.withColumn("departureOtherRevenue", functions.lit(0))
				.withColumn("departureUpsellNights", functions.lit(0))
				.withColumn("departureOtherNights", functions.lit(0)).withColumn("departureUpsells", functions.lit(0))
				.withColumn("departureOtherUpsells", functions.lit(0))

				.withColumn("departureVuUpsellRevenue", functions.lit(0))
				.withColumn("departureVuOtherRevenue", functions.lit(0))
				.withColumn("departureVuUpsellNights", functions.lit(0))
				.withColumn("departureVuOtherNights", functions.lit(0))
				.withColumn("departureVuUpsells", functions.lit(0))
				.withColumn("departureVuOtherUpsells", functions.lit(0))

				.withColumn("departureProtUpsellRevenue", functions.lit(0))
				.withColumn("departureProtOtherRevenue", functions.lit(0))
				.withColumn("departureProtUpsellNights", functions.lit(0))
				.withColumn("departureProtOtherNights", functions.lit(0))
				.withColumn("departureProtUpsells", functions.lit(0))
				.withColumn("departureProtOtherUpsells", functions.lit(0))

				.withColumn("departureOthUpsellRevenue", functions.lit(0))
				.withColumn("departureOthOtherRevenue", functions.lit(0))
				.withColumn("departureOthUpsellNights", functions.lit(0))
				.withColumn("departureOthOtherNights", functions.lit(0))
				.withColumn("departureOthUpsells", functions.lit(0))
				.withColumn("departureOthOtherUpsells", functions.lit(0))

				.withColumn("departureCommissionableRevenue", functions.lit(0))
				.withColumn("departureCallOrders", functions.lit(0))
				.withColumn("departureTicketsSold", functions.lit(0))
				.withColumn("departureUpsellReservations", functions.lit(0))

				.withColumn("dailyUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyVuUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("dailyOthUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureVuUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureProtUpsellsCnf", functions.lit(null).cast("String"))
				.withColumn("departureOthUpsellsCnf", functions.lit(null).cast("String")).drop("MARKET_SEGMENT_TYPE");

		result3.createOrReplaceTempView("salesTemp_3");

		logger.warn(
				"-------------------------------Transform Upsell Tracker Sales Table Temp3 ended ----------------------");
	}
}