package com.spark.Upsell_Tracker_Report_Load;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.RelationalGroupedDataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformSalesToLocationSalesTable {
	final static Logger logger = Logger.getLogger(TransformLocationMetricsFromLocationSalesTable.class);

	sqlConnection conn;
	SparkSession sparkSession;
	public TransformSalesToLocationSalesTable(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void start() throws AnalysisException {
		logger.warn(
				"-------------------------------Transform Sales To Location Sales Table started ----------------------");
		Dataset<Row> sales = conn.dbSession("sales").persist(StorageLevel.MEMORY_ONLY_SER());
//		Dataset<Row> sales =sparkSession.table("salesTemp");
		Dataset<Row> result = sales
				.groupBy(sales.col("TENANT_LOCATION_ID"), sales.col("DIM_TENANT_LOCATION_ID"), sales.col("metricDate"),
						sales.col("DIM_METRIC_DATE_ID"))
				.agg(functions.sum(sales.col("arrivalUpsellRevenue")).as("arrivalUpsellRevenue"),
						functions.sum(sales.col("arrivalOtherRevenue")).as("arrivalOtherRevenue"),
						functions.sum(sales.col("arrivalUpsellNights")).as("arrivalUpsellNights"),
						functions.sum(sales.col("arrivalOtherNights")).as("arrivalOtherNights"),
						functions.sum(sales.col("arrivalUpsells")).as("arrivalUpsells"),
						functions.sum(sales.col("arrivalOtherUpsells")).as("arrivalOtherUpsells"),
						functions.sum(sales.col("dailyUpsellRevenue")).as("dailyUpsellRevenue"),
						functions.sum(sales.col("dailyOtherRevenue")).as("dailyOtherRevenue"),
						functions.sum(sales.col("dailyUpsellNights")).as("dailyUpsellNights"),
						functions.sum(sales.col("dailyOtherNights")).as("dailyOtherNights"),
						functions.sum(sales.col("dailyUpsells")).as("dailyUpsells"),
						functions.sum(sales.col("dailyOtherUpsells")).as("dailyOtherUpsells"),
						functions.sum(sales.col("departureUpsellRevenue")).as("departureUpsellRevenue"),
						functions.sum(sales.col("departureOtherRevenue")).as("departureOtherRevenue"),
						functions.sum(sales.col("departureUpsellNights")).as("departureUpsellNights"),
						functions.sum(sales.col("departureOtherNights")).as("departureOtherNights"),
						functions.sum(sales.col("departureUpsells")).as("departureUpsells"),
						functions.sum(sales.col("departureOtherUpsells")).as("departureOtherUpsells"),
						functions.sum(sales.col("dailyVuUpsellRevenue")).as("dailyVuUpsellRevenue"),
						functions.sum(sales.col("dailyVuOtherRevenue")).as("dailyVuOtherRevenue"),
						functions.sum(sales.col("dailyVuUpsellNights")).as("dailyVuUpsellNights"),
						functions.sum(sales.col("dailyVuOtherNights")).as("dailyVuOtherNights"),
						functions.sum(sales.col("dailyVuUpsells")).as("dailyVuUpsells"),
						functions.sum(sales.col("dailyVuOtherUpsells")).as("dailyVuOtherUpsells"),
						functions.sum(sales.col("dailyProtUpsellRevenue")).as("dailyProtUpsellRevenue"),
						functions.sum(sales.col("dailyProtOtherRevenue")).as("dailyProtOtherRevenue"),
						functions.sum(sales.col("dailyProtUpsellNights")).as("dailyProtUpsellNights"),
						functions.sum(sales.col("dailyProtOtherNights")).as("dailyProtOtherNights"),
						functions.sum(sales.col("dailyProtUpsells")).as("dailyProtUpsells"),
						functions.sum(sales.col("dailyProtOtherUpsells")).as("dailyProtOtherUpsells"),
						functions.sum(sales.col("dailyOthUpsellRevenue")).as("dailyOthUpsellRevenue"),
						functions.sum(sales.col("dailyOthOtherRevenue")).as("dailyOthOtherRevenue"),
						functions.sum(sales.col("dailyOthUpsellNights")).as("dailyOthUpsellNights"),
						functions.sum(sales.col("dailyOthOtherNights")).as("dailyOthOtherNights"),
						functions.sum(sales.col("dailyOthUpsells")).as("dailyOthUpsells"),
						functions.sum(sales.col("dailyOthOtherUpsells")).as("dailyOthOtherUpsells"),
						functions.sum(sales.col("arrivalVuUpsellRevenue")).as("arrivalVuUpsellRevenue"),
						functions.sum(sales.col("arrivalVuOtherRevenue")).as("arrivalVuOtherRevenue"),
						functions.sum(sales.col("arrivalVuUpsellNights")).as("arrivalVuUpsellNights"),
						functions.sum(sales.col("arrivalVuOtherNights")).as("arrivalVuOtherNights"),
						functions.sum(sales.col("arrivalVuUpsells")).as("arrivalVuUpsells"),
						functions.sum(sales.col("arrivalVuOtherUpsells")).as("arrivalVuOtherUpsells"),
						functions.sum(sales.col("arrivalProtUpsellRevenue")).as("arrivalProtUpsellRevenue"),
						functions.sum(sales.col("arrivalProtOtherRevenue")).as("arrivalProtOtherRevenue"),
						functions.sum(sales.col("arrivalProtUpsellNights")).as("arrivalProtUpsellNights"),
						functions.sum(sales.col("arrivalProtOtherNights")).as("arrivalProtOtherNights"),
						functions.sum(sales.col("arrivalProtUpsells")).as("arrivalProtUpsells"),
						functions.sum(sales.col("arrivalProtOtherUpsells")).as("arrivalProtOtherUpsells"),
						functions.sum(sales.col("arrivalOthUpsellRevenue")).as("arrivalOthUpsellRevenue"),
						functions.sum(sales.col("arrivalOthOtherRevenue")).as("arrivalOthOtherRevenue"),
						functions.sum(sales.col("arrivalOthUpsellNights")).as("arrivalOthUpsellNights"),
						functions.sum(sales.col("arrivalOthOtherNights")).as("arrivalOthOtherNights"),
						functions.sum(sales.col("arrivalOthUpsells")).as("arrivalOthUpsells"),
						functions.sum(sales.col("arrivalOthOtherUpsells")).as("arrivalOthOtherUpsells"),
						functions.sum(sales.col("departureVuUpsellRevenue")).as("departureVuUpsellRevenue"),
						functions.sum(sales.col("departureVuOtherRevenue")).as("departureVuOtherRevenue"),
						functions.sum(sales.col("departureVuUpsellNights")).as("departureVuUpsellNights"),
						functions.sum(sales.col("departureVuOtherNights")).as("departureVuOtherNights"),
						functions.sum(sales.col("departureVuUpsells")).as("departureVuUpsells"),
						functions.sum(sales.col("departureVuOtherUpsells")).as("departureVuOtherUpsells"),
						functions.sum(sales.col("departureProtUpsellRevenue")).as("departureProtUpsellRevenue"),
						functions.sum(sales.col("departureProtOtherRevenue")).as("departureProtOtherRevenue"),
						functions.sum(sales.col("departureProtUpsellNights")).as("departureProtUpsellNights"),
						functions.sum(sales.col("departureProtOtherNights")).as("departureProtOtherNights"),
						functions.sum(sales.col("departureProtUpsells")).as("departureProtUpsells"),
						functions.sum(sales.col("departureProtOtherUpsells")).as("departureProtOtherUpsells"),
						functions.sum(sales.col("departureOthUpsellRevenue")).as("departureOthUpsellRevenue"),
						functions.sum(sales.col("departureOthOtherRevenue")).as("departureOthOtherRevenue"),
						functions.sum(sales.col("departureOthUpsellNights")).as("departureOthUpsellNights"),
						functions.sum(sales.col("departureOthOtherNights")).as("departureOthOtherNights"),
						functions.sum(sales.col("departureOthUpsells")).as("departureOthUpsells"),
						functions.sum(sales.col("departureOthOtherUpsells")).as("departureOthOtherUpsells"),
						functions.sum(sales.col("dailyCommissionableRevenue")).as("dailyCommissionableRevenue"),
						functions.sum(sales.col("arrivalCommissionableRevenue")).as("arrivalCommissionableRevenue"),
						functions.sum(sales.col("departureCommissionableRevenue")).as("departureCommissionableRevenue"),
						functions.sum(sales.col("dailyCallOrders")).as("dailyCallOrders"),
						functions.sum(sales.col("arrivalCallOrders")).as("arrivalCallOrders"),
						functions.sum(sales.col("departureCallOrders")).as("departureCallOrders"),
						functions.sum(sales.col("dailyTicketsSold")).as("dailyTicketsSold"),
						functions.sum(sales.col("arrivalTicketsSold")).as("arrivalTicketsSold"),
						functions.sum(sales.col("departureTicketsSold")).as("departureTicketsSold"),
						functions.sum(sales.col("dailyUpsellReservations")).as("dailyUpsellReservations"),
						functions.sum(sales.col("arrivalUpsellReservations")).as("arrivalUpsellReservations"),
						functions.sum(sales.col("departureUpsellReservations")).as("departureUpsellReservations"),

						functions.sum(sales.col("arrivalUpsellTranRevenue")).as("arrivalUpsellTranRevenue"),
						functions.sum(sales.col("arrivalOtherTranRevenue")).as("arrivalOtherTranRevenue"),

						functions.sum(sales.col("departureUpsellTranRevenue")).as("departureUpsellTranRevenue"),
						functions.sum(sales.col("departureOtherTranRevenue")).as("departureOtherTranRevenue"),

						functions.sum(sales.col("dailyUpsellTranRevenue")).as("dailyUpsellTranRevenue"),
						functions.sum(sales.col("dailyOtherTranRevenue")).as("dailyOtherTranRevenue"),

						functions.sum(sales.col("arrivalUpsellGroupRevenue")).as("arrivalUpsellGroupRevenue"),
						functions.sum(sales.col("arrivalOtherGroupRevenue")).as("arrivalOtherGroupRevenue"),
						functions.sum(sales.col("departureUpsellGroupRevenue")).as("departureUpsellGroupRevenue"),
						functions.sum(sales.col("departureOtherGroupRevenue")).as("departureOtherGroupRevenue"),
						functions.sum(sales.col("dailyUpsellGroupRevenue")).as("dailyUpsellGroupRevenue"),
						functions.sum(sales.col("dailyOtherGroupRevenue")).as("dailyOtherGroupRevenue"),
						functions.sum(sales.col("arrivalUpsellPerRevenue")).as("arrivalUpsellPerRevenue"),
						functions.sum(sales.col("arrivalOtherPerRevenue")).as("arrivalOtherPerRevenue"),
						functions.sum(sales.col("departureUpsellPerRevenue")).as("departureUpsellPerRevenue"),

						functions.sum(sales.col("departureOtherPerRevenue")).as("departureOtherPerRevenue"),
						functions.sum(sales.col("dailyUpsellPerRevenue")).as("dailyUpsellPerRevenue"),
						functions.sum(sales.col("dailyOtherPerRevenue")).as("dailyOtherPerRevenue")

				);


		result.createOrReplaceTempView("location_sales");
		
		sales.unpersist();
		logger.warn(
				"-------------------------------Transform Sales To Location Sales Table ended ----------------------");
	}
}
