package com.spark.Upsell_Tracker_Report_Load;

public class UDF_BACKUP {
//	package com.spark.Upsell_Tracker_Report_Load;
//
//	import static org.apache.spark.sql.functions.callUDF;
//
//	import java.io.Serializable;
//	import java.util.ArrayList;
//	import java.util.Date;
//	import java.util.List;
//	import java.util.stream.Collectors;
//
//	import org.apache.log4j.Logger;
//	import org.apache.spark.sql.Column;
//	import org.apache.spark.sql.Dataset;
//	import org.apache.spark.sql.Row;
//	import org.apache.spark.sql.RowFactory;
//	import org.apache.spark.sql.SparkSession;
//	import org.apache.spark.sql.functions;
//	import org.apache.spark.sql.api.java.UDF13;
//	import org.apache.spark.sql.types.DataTypes;
//	import org.apache.spark.sql.types.StructField;
//	import org.apache.spark.sql.types.StructType;
//	import org.apache.spark.storage.StorageLevel;
//	import org.spark_project.guava.collect.ImmutableList;
//
//	import com.spark.sqlConnection;
//
//	public class UDF implements Serializable {
//		/**
//		 * 
//		 */
//		final static Logger logger = Logger.getLogger(UDF.class);
//
//		final static String OPEN_BRAC = "(";
//
//		final static String CLOSE_BRAC = ")";
//
//		final static String COMMA = ",";
//
//		private static final long serialVersionUID = 1L;
//		static sqlConnection sql = new sqlConnection();
//		static SparkSession spark = sql.getSpark();
//		static Dataset<Row> dmProd = spark.sql("select * from sales_udf").limit(5).persist(StorageLevel.MEMORY_ONLY_SER());
//		static String field_name = "";
//		Dataset<Row> stored_procedure_info = sql.dbSession("stored_procedure_info").persist(StorageLevel.MEMORY_ONLY_SER());
//		Dataset<Row> lmIDs = stored_procedure_info
//				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
//				.select("field_value");
//		List<Row> collectAsList = lmIDs.collectAsList();
//		List<Row> lmDate = stored_procedure_info
//				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
//				.collectAsList();
//		String id[] = (collectAsList.size() == 0 ? ""
//				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1)).split(",");
//		static List<String> mlist = new ArrayList<String>();
//
//		public static class insert_upsell_cnt implements
//				UDF13<String, Date, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer,
//
//						String> {
//
//			private static final long serialVersionUID = 1L;
//
//			@SuppressWarnings("null")
//
//			@Override
//			public String call(String confirmationNumber, Date metricDate, Integer salesId, Integer USER_ID,
//					Integer DIM_USER_ID, Integer TENANT_LOCATION_ID, Integer REGION_ID, Integer LOCATION_GROUP_ID,
//					Integer DIM_TENANT_LOCATION_ID, Integer DIM_LOCATION_GROUP_ID, Integer PRODUCT_ID,
//					Integer DIM_PRODUCT_ID, Integer DIM_METRIC_DATE_ID
//
//			) throws Exception {
//
//				List<String> rows = new ArrayList<String>();
//				String[] confirmationNumbers = confirmationNumber.split(",");
//				for (int i = 0; i < confirmationNumbers.length; i++) {
////					COMMA + OPEN_BRAC +
//					String row =   OPEN_BRAC + salesId + COMMA + confirmationNumbers[i] + COMMA + "'" + field_name + "'"
////							+ COMMA + metricDate + COMMA + USER_ID + COMMA + DIM_USER_ID + COMMA + TENANT_LOCATION_ID
////							+ COMMA + REGION_ID + COMMA + DIM_TENANT_LOCATION_ID + COMMA + LOCATION_GROUP_ID + COMMA
////							+ DIM_LOCATION_GROUP_ID + COMMA + PRODUCT_ID + COMMA + DIM_PRODUCT_ID + COMMA
////							+ DIM_METRIC_DATE_ID; 
//							+ CLOSE_BRAC;
//					rows.add(row);
//
//					mlist.add(row);
//
//				}
//	System.out.println(rows);
//				return rows.stream().map(String::valueOf).collect(Collectors.joining(" "));
//			}
//		}
//
//		public void UDFFULL(String col, int ISFULLPROCESS) {
//			logger.warn("-------------------------UDF STARTED-----------------" + col);
//			spark.udf().registerJava("insert_upsell_cnt", insert_upsell_cnt.class.getName(), DataTypes.StringType);
//			field_name = col;
//			switch (col) {
//			case "arrivalUpsells":
//				col = "arrivalUpsellsCnf";
//				break;
//			case "arrivalVuUpsells":
//				col = "arrivalVuUpsellsCnf";
//				break;
//			case "arrivalProtUpsells":
//				col = "arrivalProtUpsellsCnf";
//				break;
//			case "arrivalOthUpsells":
//				col = "arrivalOthUpsellsCnf";
//				break;
//			case "dailyUpsells":
//				col = "dailyUpsellsCnf";
//				break;
//			case "dailyVuUpsells":
//				col = "dailyVuUpsellsCnf";
//				break;
//			case "dailyProtUpsells":
//				col = "dailyProtUpsellsCnf";
//				break;
//
//			case "dailyOthUpsells":
//				col = "dailyOthUpsellsCnf";
//				break;
//			case "departureUpsells":
//				col = "departureUpsellsCnf";
//				break;
//			case "departureVuUpsells":
//				col = "departureVuUpsellsCnf";
//				break;
//
//			case "departureProtUpsells":
//				col = "departureProtUpsellsCnf";
//				break;
//			case "departureOthUpsells":
//				col = "departureOthUpsellsCnf";
//				break;
//
//			}
//
//			System.out.println(col);
//			Column field_type = dmProd.col(col);
//			Dataset<Row> filterData = null;
//			if (ISFULLPROCESS == 1) {
//
//				// full
//				filterData = dmProd.where(dmProd.col(col).isNotNull())
//						.select(dmProd.col(col), dmProd.col("id"), dmProd.col("metricDate"), dmProd.col("USER_ID"),
//								dmProd.col("DIM_USER_ID"), dmProd.col("TENANT_LOCATION_ID"), dmProd.col("REGION_ID"),
//								dmProd.col("LOCATION_GROUP_ID"),
//
//								dmProd.col("DIM_TENANT_LOCATION_ID"), dmProd.col("DIM_LOCATION_GROUP_ID"),
//								dmProd.col("PRODUCT_ID"), dmProd.col("DIM_PRODUCT_ID"), dmProd.col("DIM_METRIC_DATE_ID"))
//						.persist(StorageLevel.MEMORY_ONLY_SER());
//			} else {
//				// partial
//				filterData = dmProd
//						.where(field_type.isNotNull()
//								.and(dmProd.col("metricDate").between(lmDate.get(0).toString().substring(1, 11),
//										lmDate.get(1).toString().substring(1, 11)))
//								.and(collectAsList.isEmpty() ? functions.when(dmProd.col("id").isNull(), true)
//										: dmProd.col("tenant_location_id").isin(id)))
//						.select(dmProd.col(col), dmProd.col("id"), dmProd.col("metricDate"))
//						.persist(StorageLevel.MEMORY_ONLY_SER());
//
//			}
//
//			Dataset<Row> row = filterData.select(
//
//					callUDF("insert_upsell_cnt", field_type, filterData.col("metricDate"), filterData.col("id"),
//							filterData.col("USER_ID"), filterData.col("DIM_USER_ID"), filterData.col("TENANT_LOCATION_ID"),
//							filterData.col("REGION_ID"), filterData.col("LOCATION_GROUP_ID"),
//
//							filterData.col("DIM_TENANT_LOCATION_ID"), filterData.col("DIM_LOCATION_GROUP_ID"),
//							filterData.col("PRODUCT_ID"), filterData.col("DIM_PRODUCT_ID"),
//							filterData.col("DIM_METRIC_DATE_ID")
//
//					).as("quersaddddddddddddddddddddddddddddddy"));
//
//			List<Row> cl = row.collectAsList();
//			row.show();
//			String query = cl.stream().map(String::valueOf).collect(Collectors.joining()).replaceAll("\\]\\[", "");
//			List<Row> resultRow = ImmutableList.of(RowFactory.create(mlist.toArray()));
//			Dataset<Row> cnt = spark.table("upsell_cnt");
//			System.out.println( resultRow.size());
//			
//			StructType schema = cnt.schema();
//			Row r1 = RowFactory.create(1,"name1", "value1");
//			Row r2 = RowFactory.create(2,"name2", "value2");
//			List<Row> rowList = ImmutableList.of(r1, r2);
//			
//			 spark.createDataFrame(resultRow, schema).show();		
//			
//			
//			//		spark.sql("insert  into  upsell_cnt     values " + query.substring(2, query.length() - 1));
//
//			logger.warn("------------------------- UDF END -----------------" + col + " " + cl.size());
//		}
//	}

}
