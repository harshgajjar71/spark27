package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class ArrivalDataViewDailyTransformation {

	sqlConnection conn;
	SparkSession sparkSession;

	public ArrivalDataViewDailyTransformation(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	final static Logger logger = Logger.getLogger(ArrivalDataViewDailyTransformation.class);

	public void start() {
		logger.warn(
				"----------------------- Arrival_data_view_daily_transformation started------------------------------");

		Dataset<Row> utt = sparkSession.table("upsell_tracker_temp").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> stored_procedure_info = sparkSession.table("stored_procedure_info");
		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
				.select("datetime").collectAsList();

		Dataset<Row> filter = utt.filter(utt.col("tenantLocationHotelMetricsDataType").equalTo(1)
				.and(utt.col("locGroupIsRetail").equalTo(0)).and(utt.col("arrivalDate").between(
						pmDate.get(0).toString().substring(1, 11), pmDate.get(1).toString().substring(1, 11))));

		Dataset<Row> upsell_tracker_tempGroupBy = filter
				.groupBy("USER_ID", "TENANT_LOCATION_ID", "LOCATION_GROUP_ID", "PRODUCT_ID", "confirmationNumber",
						"arrivalDate", "marketSegment1", "marketSegment2", "reservationType", "MARKET_SEGMENT_TYPE")
				.agg(functions.first(filter.col("PM_ID")).as("PM_ID"),

						functions.first(filter.col("departureDate")).as("departureDate"),
						functions.min(filter.col("businessDate")).as("businessDate"));

		Dataset<Row> agg = utt.join(upsell_tracker_tempGroupBy, upsell_tracker_tempGroupBy.col("USER_ID")
				.equalTo(utt.col("USER_ID"))
				.and(upsell_tracker_tempGroupBy.col("TENANT_LOCATION_ID").equalTo(utt.col("TENANT_LOCATION_ID")))
				.and(upsell_tracker_tempGroupBy.col("LOCATION_GROUP_ID").equalTo(utt.col("LOCATION_GROUP_ID")))
				.and(upsell_tracker_tempGroupBy.col("PRODUCT_ID").equalTo(utt.col("PRODUCT_ID")))
				.and(utt.col("arrivalDate").equalTo(upsell_tracker_tempGroupBy.col("arrivalDate")))
				.and(utt.col("businessDate").equalTo(upsell_tracker_tempGroupBy.col("businessDate")))
				.and(utt.col("confirmationNumber").equalTo(upsell_tracker_tempGroupBy.col("confirmationNumber")))
				.and(utt.col("marketSegment1").equalTo(upsell_tracker_tempGroupBy.col("marketSegment1")))
				.and(utt.col("marketSegment2").equalTo(upsell_tracker_tempGroupBy.col("marketSegment2")))
				.and(utt.col("reservationType").equalTo(upsell_tracker_tempGroupBy.col("reservationType")))
				.and(utt.col("MARKET_SEGMENT_TYPE").equalTo(upsell_tracker_tempGroupBy.col("MARKET_SEGMENT_TYPE")))
				.and(utt.col("locGroupIsRetail").equalTo(0)));

		Column arrivalUpsellsCnf = functions
				.concat_ws(" , ",
						agg.select("arrivalUpsellsCnf").dropDuplicates("arrivalUpsellsCnf").col("arrivalUpsellsCnf"))
				.as("arrivalUpsellsCnf");
		Column arrivalVuUpsellsCnf = functions.concat_ws(" , ",
				agg.select("arrivalVuUpsellsCnf").dropDuplicates("arrivalVuUpsellsCnf").col("arrivalVuUpsellsCnf"))
				.as("arrivalVuUpsellsCnf");
		Column arrivalProtUpsellsCnf = functions.concat_ws(" , ", agg.select("arrivalProtUpsellsCnf")
				.dropDuplicates("arrivalProtUpsellsCnf").col("arrivalProtUpsellsCnf"));
		Column arrivalOthUpsellsCnf = functions.concat_ws(" , ",
				agg.select("arrivalOthUpsellsCnf").dropDuplicates("arrivalOthUpsellsCnf").col("arrivalOthUpsellsCnf"));

		Dataset<Row> selectAgg = agg.select(utt.col("PM_ID"), utt.col("USER_ID"), utt.col("TENANT_LOCATION_ID"),
				utt.col("LOCATION_GROUP_ID"), utt.col("PRODUCT_ID"), utt.col("arrivalDate").as("metricDate"),

				utt.col("numberOfUpsellNights"), utt.col("numberOfOtherNights"), utt.col("upsellChargePerNight"),

				utt.col("otherChargePerNight"), utt.col("isMajor"), utt.col("isRecurring"),

				utt.col("totalUpsellAmount"), utt.col("totalOtherAmount"), utt.col("majorConfirmationNumber"),
				utt.col("minorConfirmationNumber"), utt.col("confirmationNumber"), utt.col("productGroupType"),
				utt.col("productGroupID"), utt.col("callOrders"), utt.col("ticketsSold"), utt.col("marketSegment1"),
				utt.col("marketSegment2"), utt.col("reservationType"), utt.col("arrivalUpsellsCnf"),
				utt.col("arrivalVuUpsellsCnf"), utt.col("arrivalProtUpsellsCnf"), utt.col("arrivalOthUpsellsCnf"),
				utt.col("numberOfUpsellNights"), utt.col("numberOfOtherNights"), utt.col("commissionableRevenue"),
				utt.col("upsellReservations"), utt.col("MARKET_SEGMENT_TYPE"));
		Dataset<Row> finalResult = selectAgg.groupBy("USER_ID", "TENANT_LOCATION_ID", "LOCATION_GROUP_ID", "PRODUCT_ID",
				"confirmationNumber", "metricDate", "marketSegment1", "marketSegment2", "reservationType",
				"MARKET_SEGMENT_TYPE").agg(

						functions.sum(selectAgg.col("numberOfUpsellNights")).as("arrivalUpsellNights"),
						functions.sum(selectAgg.col("numberOfOtherNights")).as("arrivalOtherNights"),
						functions.round(functions.sum(selectAgg.col("upsellChargePerNight")), 2)
								.as("arrivalUpsellChargePerNight"),
						functions.round(functions.sum(selectAgg.col("totalUpsellAmount")), 2)
								.as("arrivalUpsellRevenue"),
						functions.round(functions.sum(selectAgg.col("totalOtherAmount")), 2).as("arrivalOtherRevenue"),
						functions.countDistinct(selectAgg.col("minorConfirmationNumber")).as("arrivalUpsells"),
						functions.countDistinct(selectAgg.col("minorConfirmationNumber")).as("arrivalOtherUpsells"),
						functions.first(selectAgg.col("productGroupType")).as("productGroupType"),
						functions.first(selectAgg.col("productGroupID")).as("productGroupID"),
						functions.round(functions.sum(selectAgg.col("commissionableRevenue")), 2)
								.as("commissionableRevenue"),
						functions.round(functions.sum(selectAgg.col("callOrders")), 2).as("callOrders"),
						functions.round(functions.sum(selectAgg.col("ticketsSold")), 2).as("ticketsSold"),
						functions.round(functions.sum(selectAgg.col("upsellReservations")), 2).as("upsellReservations"),
						functions.first(functions.concat_ws(" , ", arrivalUpsellsCnf)).as("arrivalUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", arrivalVuUpsellsCnf)).as("arrivalVuUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", arrivalProtUpsellsCnf)).as("arrivalProtUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", arrivalOthUpsellsCnf)).as("arrivalOthUpsellsCnf"))
				.drop("confirmationNumber");

		finalResult.createOrReplaceTempView("arrival_data_view_daily");

		logger.warn(
				"----------------------- Arrival_data_view_daily_transformation ended------------------------------");
	}

}
