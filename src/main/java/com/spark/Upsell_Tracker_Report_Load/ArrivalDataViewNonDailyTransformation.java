package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class ArrivalDataViewNonDailyTransformation {
	sqlConnection conn;
	SparkSession sparkSession;

	public ArrivalDataViewNonDailyTransformation(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	final static Logger logger = Logger.getLogger(ArrivalDataViewNonDailyTransformation.class);

	public void start() {
		logger.warn(
				"----------------------- Arrival_data_view_non_daily_transformation started------------------------------");

		Dataset<Row> utu = sparkSession.table("upsell_tracker_location_metrics_unique_view")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		;
		Dataset<Row> utt = sparkSession.table("upsell_tracker_temp").persist(StorageLevel.MEMORY_ONLY_SER());
		;

		Dataset<Row> stored_procedure_info = sparkSession.table("stored_procedure_info");

		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
				.select("datetime").collectAsList();

		Dataset<Row> arrival_data_view_non_daily_temp = utt
				.join(utu,
						utu.col("TENANT_LOCATION_ID").equalTo(utt.col("TENANT_LOCATION_ID"))
								.and(utt.col("arrivalDate").equalTo(utu.col("metricDate"))
										.and(utt.col("tenantLocationHotelMetricsDataType").isin("0", "2"))
										.and(utt.col("locGroupIsRetail").equalTo(0))))
				.where(utu.col("metricDate").between(pmDate.get(0).toString().substring(1, 11),
						pmDate.get(1).toString().substring(1, 11)));

		Column arrivalUpsellsCnf = functions
				.concat_ws(" , ",
						utt.select("arrivalUpsellsCnf").dropDuplicates("arrivalUpsellsCnf").col("arrivalUpsellsCnf"))
				.as("arrivalUpsellsCnf");
		Column arrivalVuUpsellsCnf = functions.concat_ws(" , ",
				utt.select("arrivalVuUpsellsCnf").dropDuplicates("arrivalVuUpsellsCnf").col("arrivalVuUpsellsCnf"))
				.as("arrivalVuUpsellsCnf");
		Column arrivalProtUpsellsCnf = functions.concat_ws(" , ", utt.select("arrivalProtUpsellsCnf")
				.dropDuplicates("arrivalProtUpsellsCnf").col("arrivalProtUpsellsCnf"));
		Column arrivalOthUpsellsCnf = functions.concat_ws(" , ",
				utt.select("arrivalOthUpsellsCnf").dropDuplicates("arrivalOthUpsellsCnf").col("arrivalOthUpsellsCnf"));

		Dataset<Row> result0 = arrival_data_view_non_daily_temp.select(utt.col("USER_ID"),
				utt.col("TENANT_LOCATION_ID"), utt.col("LOCATION_GROUP_ID"), utt.col("PRODUCT_ID"),
				utu.col("metricDate"), utt.col("productGroupType"), utt.col("productGroupID"),
				utt.col("marketSegment1"), utt.col("marketSegment2"), utt.col("reservationType"),

				utt.col("numberOfUpsellNights"), utt.col("numberOfOtherNights"), utt.col("upsellChargePerNight"),
				utt.col("totalUpsellAmount"), utt.col("totalOtherAmount"), utt.col("minorConfirmationNumber"),
				utt.col("minorConfirmationNumber"), utt.col("commissionableRevenue"), utt.col("callOrders"),
				utt.col("ticketsSold"), utt.col("upsellReservations"), utt.col("arrivalUpsellsCnf"),
				utt.col("arrivalVuUpsellsCnf"), utt.col("arrivalProtUpsellsCnf"), utt.col("arrivalOthUpsellsCnf"),
				utt.col("MARKET_SEGMENT_TYPE"));

		Dataset<Row> resultByWhere = result0.filter(utu.col("metricDate")
				.between(pmDate.get(0).toString().substring(1, 11), pmDate.get(1).toString().substring(1, 11)));

		Dataset<Row> finalResult = resultByWhere
				.groupBy(utt.col("USER_ID"), utt.col("TENANT_LOCATION_ID"), utt.col("LOCATION_GROUP_ID"),
						utt.col("PRODUCT_ID"), utu.col("metricDate"), utt.col("marketSegment1"),
						utt.col("marketSegment2"), utt.col("reservationType"), utt.col("MARKET_SEGMENT_TYPE"))
				.agg(functions.sum(utt.col("numberOfUpsellNights")).as("arrivalUpsellNights"),
						functions.sum(utt.col("numberOfOtherNights")).as("arrivalOtherNights"),
						functions.round(functions.sum(utt.col("upsellChargePerNight")), 2)
								.as("arrivalUpsellChargePerNight"),
						functions.round(functions.sum(utt.col("totalUpsellAmount")), 2).as("arrivalUpsellRevenue"),
						functions.round(functions.sum(utt.col("totalOtherAmount")), 2).as("arrivalOtherRevenue"),
						functions.countDistinct(utt.col("minorConfirmationNumber")).as("arrivalUpsells"),
						functions.countDistinct(utt.col("minorConfirmationNumber")).as("arrivalOtherUpsells"),
						functions.first(utt.col("productGroupID")).as("productGroupID"),
						functions.first(utt.col("productGroupType")).as("productGroupType"),
						functions.round(functions.sum(utt.col("commissionableRevenue")), 2).as("commissionableRevenue"),
						functions.round(functions.sum(utt.col("callOrders")), 2).as("callOrders"),
						functions.round(functions.sum(utt.col("ticketsSold")), 2).as("ticketsSold"),
						functions.round(functions.sum(utt.col("upsellReservations")), 2).as("upsellReservations"),
						functions.first(functions.concat_ws(" , ", arrivalUpsellsCnf)).as("arrivalUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", arrivalVuUpsellsCnf)).as("arrivalVuUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", arrivalProtUpsellsCnf)).as("arrivalProtUpsellsCnf"),
						functions.first(functions.concat_ws(" , ", arrivalOthUpsellsCnf)).as("arrivalOthUpsellsCnf"));

		// view creation
		finalResult.createOrReplaceTempView("arrival_data_view_non_daily");
//		finalResult.write().mode(SaveMode.Overwrite).parquet("arrival_data_view_non_daily");
		logger.warn(
				"----------------------- Arrival_data_view_non_daily_transformation completed------------------------------");

	}
}