package com.spark.Upsell_Tracker_Report_Load;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class UpdateLocationMetricsFromLocationProfileTable {
	final static Logger logger = Logger.getLogger(UpdateLocationMetricsFromLocationProfileTable.class);

	sqlConnection conn;
	SparkSession sparkSession;

	public UpdateLocationMetricsFromLocationProfileTable(sqlConnection conn) {
		this.conn = conn;
		sparkSession = conn.getSpark();
	}

	public void start() throws AnalysisException {
		logger.warn(
				"-------------------------------Update Location Metrics From Location Profile Table started ----------------------");
		Dataset<Row> storedInfo = conn.dbSession("stored_procedure_info");
//				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> locationMetric = conn.dbSession("location_metric")
		.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> tenantLocationProfile = conn.dbSession("tenant_location_profile")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		List<Row> listIds = storedInfo.filter("field_name = 'TENANT_LOCATION_IDs'").select("field_value")
				.collectAsList();
		String ids = listIds.size() == 0 ? ""
				: listIds.get(0).toString().substring(1, listIds.get(0).toString().length() - 1);
		String id[] = ids.split(",");

		tenantLocationProfile = tenantLocationProfile.withColumnRenamed("id", "newID");
		Dataset<Row> joined = locationMetric
				.where((ids.isEmpty() ? functions.when(locationMetric.col("ID").isNotNull(), true)
						: locationMetric.col("TENANT_LOCATION_ID").isin(id))
								.and(locationMetric.col("roomsAvailable").isNull()))
				.join(tenantLocationProfile,
						locationMetric.col("TENANT_LOCATION_ID").equalTo(tenantLocationProfile.col("newID")))
				.withColumn("roomsAvailable", tenantLocationProfile.col("rooms")).select("ID", "roomsAvailable")
				.withColumnRenamed("ID", "joinID").withColumnRenamed("roomsAvailable", "joinroomsAvailable");

		Dataset<Row> finalResult = locationMetric.join(joined, locationMetric.col("ID").equalTo(joined.col("joinID")),
				"left_outer");
		finalResult = finalResult.withColumn("roomsAvailable", functions
				.when(finalResult.col("ID").equalTo(finalResult.col("joinID")), finalResult.col("joinroomsAvailable"))
				.otherwise(finalResult.col("roomsAvailable"))).drop("joinID","joinroomsAvailable");
		
		conn.sparkSqlWrite(finalResult, "location_metric");
//		finalResult.createOrReplaceTempView("location_metric");
//		result.write().mode(SaveMode.Overwrite).parquet("update_location_metrics_from_location_profile_table");
//		result.unpersist();
//		locationMetric.unpersist();
		tenantLocationProfile.unpersist();
//		storedInfo.unpersist();
		logger.warn(
				"-------------------------------Update Location Metrics From Location Profile Table  ended ----------------------");
	}
}
