package com.spark;

import java.io.Serializable;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

public class sqlConnection implements Serializable {
	SparkSession spark = SparkSession.builder().appName("SparkJavaExample").master("local[*]").getOrCreate();
	final static Logger logger = Logger.getLogger(sqlConnection.class);

	public sqlConnection() {

		spark.sparkContext().setLogLevel("WARN");
		// TODO Auto-generated constructor stub
		spark.conf().set("spark.debug.maxToStringFields", 10100);
		spark.conf().set("executors.memory", "7g");
		spark.conf().set("driver.memory", "5g");
		spark.conf().set("spark.write.option.mergeSchema", true);
		spark.conf().set("spark.sql.crossJoin.enabled", true);
		spark.conf().set("spark.sql.codegen", false);
		spark.conf().set("spark.sql.codegen.wholeStage", false);
		spark.conf().set("spark.rdd.compress", "true");
		spark.conf().set("spark.sql.parquet.compression.codec", "snappy");
		spark.conf().set("spark.serializer", " org.apache.spark.serializer.KryoSerializer");
		spark.conf().set("spark.databricks.io.cache.enabled", "true ");
		spark.conf().set("spark.databricks.io.cache.compression.enabled", "false");

	}

	public SparkSession getSpark() {
		return this.spark;
	}

	public void sparkSqlWrite(Dataset<Row> result, String table) {

		logger.warn("mysql write start into table " + table + " " + result.count());
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "synoverge");
		connectionProperties.put("password", "synoverge");
		logger.warn("mysql write start into table " + table);
		result.write().mode(SaveMode.Overwrite).option("queryTimeout", 20000).option("truncate", true)
				.jdbc("jdbc:mysql://192.168.3.158:3306/testspark", table, connectionProperties);
		logger.warn("mysql write end " + table);
	}

//	public void sparkSqlWrite(Dataset<Row> result, String table) {
//
//		logger.warn("mysql write start into table " + table + " " + result.count());
//		Properties connectionProperties = new Properties();
//		connectionProperties.put("user", "root");
//		connectionProperties.put("password", "ytY7Ddjy");
//		logger.warn("mysql write start into table " + table);
//		result.write().mode(SaveMode.Overwrite).option("batchsize", "10000").option("queryTimeout", 20000)
//				.option("truncate", true).jdbc("jdbc:mysql://10.200.28.69:3306/spark?autoReconnect=true&useSSL=false",
//						table, connectionProperties);
//		logger.warn("mysql write end " + table);
//	}

	public Dataset<Row> dbSession(String tableName) {
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "synoverge");
		connectionProperties.put("password", "synoverge");
		String database = "in_gauge_2018";

		Dataset<Row> table = spark.read().option("SparkEnv.conf.", 1100).option("mergeSchema", true).jdbc(
				"jdbc:mysql://192.168.3.158:3306/" + "in_gauge_2018?autoReconnect=true&useSSL=false", tableName,
				connectionProperties);
		return table;
	}

	public Dataset<Row> kafkaSession(String query) {
				
		Dataset<Row> table = spark.read().option("SparkEnv.conf.", 1100).option("mergeSchema", true)
		.format("jdbc")
		.option("url","jdbc:mysql://192.168.3.158:3306/" + "testkafka?user=synoverge&password=synoverge")
		.option("query",query).load();
		
		
//		Dataset<Row> table = spark.read().option("SparkEnv.conf.", 1100).option("mergeSchema", true).jdbc(
//				"jdbc:mysql://192.168.3.158:3306/" + "testkafka?autoReconnect=true&useSSL=false", tableName,
//				connectionProperties);
		return table;
	}

//	public Dataset<Row> dbSession(String tableName) {
//		Properties connectionProperties = new Properties();
//		connectionProperties.put("user", "root");
//		connectionProperties.put("password", "ytY7Ddjy");
//		String database = "in_gauge_2018";
//
//		Dataset<Row> table = spark.read().option("SparkEnv.conf.", 1100).option("mergeSchema", true).jdbc(
//				"jdbc:mysql://10.200.28.69:3306/" + "in_gauge_2018?autoReconnect=true&useSSL=false", tableName,
//				connectionProperties);
//		return table;
//	}

	public Dataset<Row> spSession(String tableName) {
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "ytY7Ddjy");
		String database = "spark";
		Dataset<Row> table = spark.read().option("SparkEnv.conf.", 1100).option("mergeSchema", true).jdbc(
				"jdbc:mysql://10.200.28.69:3306/" + database + "?autoReconnect=true&useSSL=false", tableName,
				connectionProperties);
		return table;
	}

//	public Dataset<Row> spSession(String tableName) {
//		Properties connectionProperties = new Properties();
//		connectionProperties.put("user", "synoverge");
//		connectionProperties.put("password", "synoverge");
//		String database = "testspark";
//		Dataset<Row> table = spark.read().option("SparkEnv.conf.", 1100).option("mergeSchema", true).jdbc(
//				"jdbc:mysql://192.168.3.158:3306/" + database + "?autoReconnect=true&useSSL=false", tableName,
//				connectionProperties);
//		return table;
//	}

	public void sparkSqlWriteToMainDB(Dataset<Row> result, String table) {

		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "ytY7Ddjy");
		logger.warn("mysql write start append into table " + table);
		result.write().mode(SaveMode.Append).option("batchsize", "10000").option("queryTimeout", 20000)
				.option("truncate", true)
				.jdbc("jdbc:mysql://10.200.28.69:3306/in_gauge_2018?autoReconnect=true&useSSL=false", table,
						connectionProperties);
		logger.warn("mysql write append end " + table);
	}

}
