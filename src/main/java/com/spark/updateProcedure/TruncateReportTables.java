package com.spark.updateProcedure;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TruncateReportTables {

	final static Logger logger = Logger.getLogger(TruncateReportTables.class);

	SparkSession sparkSession = SparkSession.builder().appName("in_gauge_2018JavaExample").master("local[*]")
			.getOrCreate();

	sqlConnection spark = new sqlConnection();

	public void startMethod() {

		Dataset<Row> information_schema = spark.dbSession( "information_schema.TABLES")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> schema = information_schema
				.where(information_schema.col("TABLE_SCHEMA").equalTo("in_gauge_2018"))
				.select(information_schema.col("TABLE_SCHEMA").as("TABLE_SCHEMA"), information_schema.col("TABLE_NAME").as("TABLE_NAME"),
						information_schema.col("TABLE_TYPE").as("TABLE_TYPE"))
				.persist(StorageLevel.MEMORY_ONLY_SER());
		
		schema.createOrReplaceTempView("schema123");
		
		String data = null;
		String data1 = null;
		String data2 = null;

		List upsell_tracker = sparkSession.sql("select count(*) from schema123 where TABLE_NAME = 'upsell_tracker' and TABLE_TYPE = 'BASE TABLE'").collectAsList();
		List upsell_tracker_temp = sparkSession.sql("select count(*) from schema123 where TABLE_NAME = 'upsell_tracker_temp' and TABLE_TYPE = 'BASE TABLE'").collectAsList();
		List upsell_tracker_sales_temp = sparkSession.sql("select count(*) from schema123 where TABLE_NAME = 'upsell_tracker_sales_temp' and TABLE_TYPE = 'BASE TABLE'").collectAsList();
		
		
//		if(upsell_tracker.size() > 0){
//			System.out.println(schema1.get(0).toString());
//		}
		
	}

}
