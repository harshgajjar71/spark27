package com.spark.updateProcedure;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class Populate_region_closure {

	final static Logger logger = Logger.getLogger(Populate_region_closure.class);

	sqlConnection spark = new sqlConnection();
	SparkSession sparkSession = spark.getSpark();

	public void startPoint() {
logger.debug("-----------------------Populate_region_closure started----------------------------");
		Dataset<Row> region = spark.dbSession("region").select("id", "parent_region_id")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		int i = 0;

		Dataset<Row> region_closure = region.select(region.col("id").as("parent_id"), region.col("id").as("region_id"))
				.withColumn("distance", functions.lit(i)).persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> updated;
		long count = 0;

		do {
			i++;

			updated = region_closure
					.join(region,
							region.col("parent_region_id").equalTo(region_closure.col("region_id"))
									.and(region_closure.col("distance").equalTo(i - 1)))
					.select(region_closure.col("parent_id"), region.col("id")).withColumn("distance", functions.lit(i));

			region_closure = region_closure.union(updated);

			count = updated.count();

		} while (count != 0.0);

		region_closure.createOrReplaceTempView("region_closure");
		logger.debug("-----------------------Populate_region_closure started----------------------------");
	
	}
}
