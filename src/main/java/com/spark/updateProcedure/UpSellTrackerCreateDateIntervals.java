package com.spark.updateProcedure;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.spark.sqlConnection;

public class UpSellTrackerCreateDateIntervals {

	final static Logger logger = Logger.getLogger(UpSellTrackerCreateDateIntervals.class);

	SparkSession sparkSession = SparkSession.builder().appName("in_gauge_2018JavaExample").master("local[*]")
			.getOrCreate();

	sqlConnection spark = new sqlConnection();

	public void startMethod(String startDate, String endDate, int intval, String unitval) throws ParseException {

		String thisDate = startDate;
		String nextDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(thisDate));
		Dataset<Row> updated = null;
		
		Date start = new Date(); 
		Date end = new Date();
		
		do {
			
			switch (unitval) {
			
//			case "MICROSECOND":
//				nextDate = thisDate.;
//				break;
//			case "SECOND":
//				nextDate = ;
//				break;
//			case "MINUTE":
//				nextDate = ;
//				break;
//			case "HOUR":
//				nextDate = ;
//				break;
			case "DAY":
				c.add(Calendar.DAY_OF_MONTH, intval);
				nextDate = sdf.format(c.getTime());
				System.out.println("nextdate ==> " + nextDate);
				break;
			case "WEEK":
				int week = intval*7;
				c.add(Calendar.DAY_OF_MONTH, week);
				nextDate = sdf.format(c.getTime());
				System.out.println("nextdate ==> " + nextDate);
				break;
			case "MONTH":
				c.add(Calendar.MONTH, intval);
				nextDate = sdf.format(c.getTime());
				System.out.println("nextdate ==> " + nextDate);
				break;
			case "QUARTER":
				int quater = intval*4;
				c.add(Calendar.MONTH, quater);
				nextDate = sdf.format(c.getTime());
				System.out.println("nextdate ==> " + nextDate);
				break;
			case "YEAR":
				c.add(Calendar.YEAR, intval);
				nextDate = sdf.format(c.getTime());
				System.out.println("nextdate ==> " + nextDate);
				break;

			}

			// insert into date_intervals
			
			thisDate = nextDate;
			start = new SimpleDateFormat("yyyy-mm-dd").parse(thisDate);; 
			end=new SimpleDateFormat("yyyy-mm-dd").parse(endDate);

		} while (end.compareTo(start) > 0);
	}

}
