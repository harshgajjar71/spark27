package com.spark.updateProcedure;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransferDataFromDimTemp2ToDimTemp {

	final static Logger logger = Logger.getLogger(TransferDataFromDimTemp2ToDimTemp.class);
	final TransformReportDimInsertionTemp procedure1 = new TransformReportDimInsertionTemp();

	sqlConnection spark = new sqlConnection();
	SparkSession sparkSession = spark.getSpark();

	public String isfullprocess() throws AnalysisException {

		Dataset<Row> stored_procedure_info = spark.dbSession("stored_procedure_info")
				.select("field_value", "field_name").persist(StorageLevel.MEMORY_ONLY_SER());

		stored_procedure_info.createOrReplaceTempView("stored_procedure_info");

		Dataset<Row> isfullprocess = sparkSession
				.sql("SELECT field_value FROM stored_procedure_info where field_name = 'IS_FULL_PROCESS'");
		return isfullprocess.col("field_value").toString();
	}

	public void mainEntryFunction() throws AnalysisException {

		Dataset<Row> dim_tenant_location_temp;

		Dataset<Row> dim_location_group_temp;

		Dataset<Row> dim_product_temp;

		Dataset<Row> dim_user_temp;

		Dataset<Row> dim_tenant_location_temp2 = sparkSession.sql("select * from dim_tenant_location_temp2");

		Dataset<Row> dim_location_group_temp2 = sparkSession.sql("select * from dim_location_group_temp2");

		Dataset<Row> dim_product_temp2 = sparkSession.sql("select * from dim_product_temp2");

		Dataset<Row> dim_user_temp2 = sparkSession.sql("select * from dim_user_temp2");

		if (isfullprocess().equalsIgnoreCase("1")) {

//			dim_tenant_location_temp = dim_tenant_location_temp2;
//
//			dim_location_group_temp = dim_location_group_temp2;
//
//			dim_product_temp = dim_product_temp2;
//
//			dim_user_temp = dim_user_temp2;
			
			dim_tenant_location_temp = spark.dbSession("dim_tenant_location");

			dim_location_group_temp = spark.dbSession("dim_location_group");

			dim_product_temp = spark.dbSession("dim_product");

			dim_user_temp = spark.dbSession("dim_user");


		} else {
			logger.warn("-------------------copy_dim_insertion_temp started ----------------------------------");

			dim_tenant_location_temp = spark.dbSession("dim_tenant_location");

			dim_location_group_temp = spark.dbSession("dim_location_group");

			dim_product_temp = spark.dbSession("dim_product");

			dim_user_temp = spark.dbSession("dim_user");

			dim_user_temp = dim_user_temp.withColumn("ROLE_ID",
					functions.when(dim_user_temp.col("ROLE_ID").isNull(), functions.lit(-1))
							.otherwise(dim_user_temp.col("ROLE_ID")));

			logger.warn("-------------------copy_dim_insertion_temp completed ----------------------------------");

			dim_tenant_location_temp = dim_tenant_location_temp2
					.join(dim_tenant_location_temp,
							dim_tenant_location_temp.col("TENANT_LOCATION_ID")
									.equalTo(dim_tenant_location_temp2.col("TENANT_LOCATION_ID")),
							// .and(dim_tenant_location_temp.col("REGION_ID")
							// .equalTo(dim_tenant_location_temp2.col("REGION_ID")))
							// .and(dim_tenant_location_temp.col("hotelMetricsDataType")
							// .equalTo(dim_tenant_location_temp2.col("hotelMetricsDataType"))),
							"left")
					.filter(dim_tenant_location_temp.col("ID").isNull())
					.select(dim_tenant_location_temp2.col("TENANT_LOCATION_ID"),
							dim_tenant_location_temp2.col("TENANT_LOCATION_NAME"),
							dim_tenant_location_temp2.col("TENANT_LOCATION_STATUS"),
							dim_tenant_location_temp2.col("TENANT_ID"), dim_tenant_location_temp2.col("TENANT_NAME"),
							dim_tenant_location_temp2.col("TENANT_STATUS"),
							dim_tenant_location_temp2.col("INDUSTRY_ID"),
							dim_tenant_location_temp2.col("INDUSTRY_NAME"), dim_tenant_location_temp2.col("REGION_ID"),
							dim_tenant_location_temp2.col("REGION_NAME"),
							dim_tenant_location_temp2.col("REGION_CURRENCY"),
							dim_tenant_location_temp2.col("LOCATION_CURRENCY"),
							dim_tenant_location_temp2.col("locationID"), dim_tenant_location_temp2.col("launchDate"),
							dim_tenant_location_temp2.col("hotelMetricsDataType"),
							dim_tenant_location_temp2.col("productMetricsValidationType"),
							dim_tenant_location_temp2.col("ACTIVE_STATUS"), dim_tenant_location_temp2.col("CREATED_ON"),
							dim_tenant_location_temp2.col("REGION_HIERARCHY"))
					.persist(StorageLevel.MEMORY_ONLY_SER());

			Dataset<Row> dim_tenant_location_temp1 = dim_tenant_location_temp.union(dim_tenant_location_temp2);

			// update started
			Dataset<Row> temp2 = dim_tenant_location_temp2
					.withColumnRenamed("TENANT_LOCATION_ID", "TENANT_LOCATION_ID_temp2")
					.withColumnRenamed("TENANT_LOCATION_NAME", "TENANT_LOCATION_NAME_temp2")
					.withColumnRenamed("TENANT_LOCATION_STATUS", "TENANT_LOCATION_STATUS_temp2")
					.withColumnRenamed("TENANT_ID", "TENANT_ID_temp2")
					.withColumnRenamed("TENANT_NAME", "TENANT_NAME_temp2")
					.withColumnRenamed("TENANT_STATUS", "TENANT_STATUS_temp2")
					.withColumnRenamed("INDUSTRY_ID", "INDUSTRY_ID_temp2")
					.withColumnRenamed("INDUSTRY_NAME", "INDUSTRY_NAME_temp2")
					.withColumnRenamed("REGION_ID", "REGION_ID_temp2")
					.withColumnRenamed("REGION_NAME", "REGION_NAME_temp2")
					.withColumnRenamed("REGION_CURRENCY", "REGION_CURRENCY_temp2")
					.withColumnRenamed("LOCATION_CURRENCY", "LOCATION_CURRENCY_temp2")
					.withColumnRenamed("locationID", "locationID_temp2")
					.withColumnRenamed("launchDate", "launchDate_temp2")
					.withColumnRenamed("hotelMetricsDataType", "hotelMetricsDataType_temp2")
					.withColumnRenamed("productMetricsValidationType", "productMetricsValidationType_temp2")
					.withColumnRenamed("ACTIVE_STATUS", "ACTIVE_STATUS_temp2")
					.withColumnRenamed("REGION_HIERARCHY", "REGION_HIERARCHY_temp2")
					.withColumnRenamed("CREATED_ON", "CREATED_ON_temp2");

			dim_tenant_location_temp1 = dim_tenant_location_temp1
					.join(temp2,
							temp2.col("TENANT_LOCATION_ID_temp2")
									.equalTo(dim_tenant_location_temp1.col("TENANT_LOCATION_ID")),
							"left")
					.withColumn("TENANT_LOCATION_ID", temp2.col("TENANT_LOCATION_ID_temp2"))
					.withColumn("TENANT_LOCATION_NAME", temp2.col("TENANT_LOCATION_NAME_temp2"))
					.withColumn("TENANT_LOCATION_STATUS", temp2.col("TENANT_LOCATION_STATUS_temp2"))
					.withColumn("TENANT_ID", temp2.col("TENANT_ID_temp2"))
					.withColumn("TENANT_NAME", temp2.col("TENANT_NAME_temp2"))
					.withColumn("TENANT_STATUS", temp2.col("TENANT_STATUS_temp2"))
					.withColumn("INDUSTRY_ID", temp2.col("INDUSTRY_ID_temp2"))
					.withColumn("INDUSTRY_NAME", temp2.col("INDUSTRY_NAME_temp2"))
					.withColumn("REGION_ID", temp2.col("REGION_ID_temp2"))
					.withColumn("REGION_NAME", temp2.col("REGION_NAME_temp2"))
					.withColumn("REGION_CURRENCY", temp2.col("REGION_CURRENCY_temp2"))
					.withColumn("LOCATION_CURRENCY", temp2.col("LOCATION_CURRENCY_temp2"))
					.withColumn("locationID", temp2.col("locationID_temp2"))
					.withColumn("launchDate", temp2.col("launchDate_temp2"))
					.withColumn("hotelMetricsDataType", temp2.col("hotelMetricsDataType_temp2"))
					.withColumn("productMetricsValidationType", temp2.col("productMetricsValidationType_temp2"))
					.withColumn("ACTIVE_STATUS", temp2.col("ACTIVE_STATUS_temp2"))
					.withColumn("CREATED_ON", temp2.col("CREATED_ON_temp2"))
					.withColumn("REGION_HIERARCHY", temp2.col("REGION_HIERARCHY_temp2"));

			dim_tenant_location_temp1 = dim_tenant_location_temp1.drop("TENANT_LOCATION_ID_temp2")
					.drop("TENANT_LOCATION_NAME_temp2").drop("TENANT_LOCATION_STATUS_temp2").drop("TENANT_ID_temp2")
					.drop("TENANT_NAME_temp2").drop("TENANT_STATUS_temp2").drop("INDUSTRY_ID_temp2")
					.drop("INDUSTRY_NAME_temp2").drop("REGION_ID_temp2").drop("REGION_NAME_temp2")
					.drop("REGION_CURRENCY_temp2").drop("LOCATION_CURRENCY_temp2").drop("locationID_temp2")
					.drop("launchDate_temp2").drop("hotelMetricsDataType_temp2")
					.drop("productMetricsValidationType_temp2").drop("ACTIVE_STATUS_temp2")
					.drop("REGION_HIERARCHY_temp2").drop("CREATED_ON_temp2");
			// update end

			dim_tenant_location_temp1.createOrReplaceTempView("dim_tenant_location_temp");

			dim_location_group_temp = dim_location_group_temp2
					.join(dim_location_group_temp,
							dim_location_group_temp.col("LOCATION_GROUP_ID")
									.equalTo(dim_location_group_temp2.col("LOCATION_GROUP_ID"))
									.and(dim_location_group_temp.col("PARENT_LOCATION_GROUP_ID")
											.equalTo(dim_location_group_temp2.col("PARENT_LOCATION_GROUP_ID")))
									.and(dim_location_group_temp.col("TENANT_LOCATION_ID")
											.equalTo(dim_location_group_temp2.col("TENANT_LOCATION_ID")))
									.and(dim_location_group_temp.col("GROUP_TYPE_ID")
											.equalTo(dim_location_group_temp2.col("GROUP_TYPE_ID"))),
							"left")
					.filter(dim_location_group_temp.col("ID").isNull())
					.select(dim_location_group_temp2.col("LOCATION_GROUP_ID"),
							dim_location_group_temp2.col("LOCATION_GROUP_NAME"),
							dim_location_group_temp2.col("LOCATION_GROUP_STATUS"),
							dim_location_group_temp2.col("PARENT_LOCATION_GROUP_ID"),
							dim_location_group_temp2.col("PARENT_LOCATION_GROUP_NAME"),
							dim_location_group_temp2.col("TENANT_LOCATION_ID"),
							dim_location_group_temp2.col("TENANT_LOCATION_NAME"),
							dim_location_group_temp2.col("TENANT_LOCATION_STATUS"),
							dim_location_group_temp2.col("TENANT_ID"), dim_location_group_temp2.col("TENANT_NAME"),
							dim_location_group_temp2.col("TENANT_STATUS"), dim_location_group_temp2.col("INDUSTRY_ID"),
							dim_location_group_temp2.col("INDUSTRY_NAME"), dim_location_group_temp2.col("REGION_ID"),
							dim_location_group_temp2.col("REGION_NAME"),
							dim_location_group_temp2.col("REGION_CURRENCY"),
							dim_location_group_temp2.col("LOCATION_CURRENCY"),
							dim_location_group_temp2.col("GROUP_TYPE_ID"),
							dim_location_group_temp2.col("GROUP_TYPE_NAME"),
							dim_location_group_temp2.col("IS_DEFAULT_TYPE"),
							dim_location_group_temp2.col("IS_DEFAULT_GROUP"),
							dim_location_group_temp2.col("LOCATION_GROUP_HIERARCHY"),
							dim_location_group_temp2.col("ACTIVE_STATUS"), dim_location_group_temp2.col("CREATED_ON"),
							dim_location_group_temp2.col("REGION_HIERARCHY"))
					.persist(StorageLevel.MEMORY_ONLY_SER());

			Dataset<Row> dim_location_group_temp1 = dim_location_group_temp.union(dim_location_group_temp2);

			dim_location_group_temp1.createOrReplaceTempView("dim_location_group_temp");

			dim_product_temp = dim_product_temp2
					.join(dim_product_temp,
							dim_product_temp.col("LOCATION_GROUP_PRODUCT_ID")
									.equalTo(dim_product_temp2.col("LOCATION_GROUP_PRODUCT_ID"))
									.and(dim_product_temp.col("TENANT_LOCATION_ID")
											.equalTo(dim_product_temp2.col("TENANT_LOCATION_ID")))
									.and(dim_product_temp.col("TENANT_PRODUCT_ID")
											.equalTo(dim_product_temp2.col("TENANT_PRODUCT_ID")))
									.and(dim_product_temp.col("LOCATION_GROUP_ID")
											.equalTo(dim_product_temp2.col("LOCATION_GROUP_ID"))),
							"left")
					.filter(dim_product_temp.col("ID").isNull())
					.select(dim_product_temp2.col("LOCATION_GROUP_PRODUCT_ID"),
							dim_product_temp2.col("TENANT_PRODUCT_ID"), dim_product_temp2.col("TENANT_PRODUCT_NAME"),
							functions
									.when(dim_product_temp2.col("LOCATION_GROUP_PRODUCT_NAME").isNull(),
											dim_product_temp2.col("TENANT_PRODUCT_NAME"))
									.otherwise(dim_product_temp2.col("LOCATION_GROUP_PRODUCT_NAME"))
									.as("LOCATION_GROUP_PRODUCT_NAME"),
							dim_product_temp2.col("LOCATION_GROUP_PRODUCT_STATUS"),
							dim_product_temp2.col("isMajor").cast("boolean").as("isMajor"),
							dim_product_temp2.col("isRecurring"), dim_product_temp2.col("TENANT_PRODUCT_CATEGORY_ID"),
							dim_product_temp2.col("TENANT_PRODUCT_CATEGORY_NAME"),
							dim_product_temp2.col("LOCATION_GROUP_ID"), dim_product_temp2.col("LOCATION_GROUP_NAME"),
							dim_product_temp2.col("LOCATION_GROUP_STATUS"), dim_product_temp2.col("TENANT_LOCATION_ID"),
							dim_product_temp2.col("TENANT_LOCATION_NAME"),
							dim_product_temp2.col("TENANT_LOCATION_STATUS"), dim_product_temp2.col("TENANT_ID"),
							dim_product_temp2.col("TENANT_NAME"), dim_product_temp2.col("TENANT_STATUS"),
							dim_product_temp2.col("INDUSTRY_ID"), dim_product_temp2.col("INDUSTRY_NAME"),
							dim_product_temp2.col("REGION_ID"), dim_product_temp2.col("REGION_NAME"),
							dim_product_temp2.col("REGION_CURRENCY"), dim_product_temp2.col("LOCATION_CURRENCY"),
							dim_product_temp2.col("ACTIVE_STATUS"), dim_product_temp2.col("CREATED_ON"),
							dim_product_temp2.col("REGION_HIERARCHY"))
					.persist(StorageLevel.MEMORY_ONLY_SER());

			Dataset<Row> dim_product_temp1 = dim_product_temp.union(dim_product_temp2);

			dim_product_temp1.createOrReplaceTempView("dim_product_temp");

			dim_user_temp2 = dim_user_temp2.withColumn("ROLE_ID",
					functions.when(dim_user_temp2.col("ROLE_ID").isNull(), functions.lit(-1))
							.otherwise(dim_user_temp2.col("ROLE_ID")));

			dim_user_temp = dim_user_temp2
					.join(dim_user_temp,
							dim_user_temp.col("USER_ID").equalTo(dim_user_temp2.col("USER_ID"))
									.and(dim_user_temp.col("ROLE_ID").equalTo(dim_user_temp2.col("ROLE_ID")))
									.and(dim_user_temp.col("LOCATION_GROUP_ID")
											.equalTo(dim_user_temp2.col("LOCATION_GROUP_ID"))),
							"left")
					.where(dim_user_temp.col("ID").isNull())
					.select(dim_user_temp2.col("LOCATION_GROUP_ID"), dim_user_temp2.col("IS_CONTRIBUTOR"),
							dim_user_temp2.col("IS_OPERATOR"), dim_user_temp2.col("ROLE_ID"),
							dim_user_temp2.col("USER_ID"), dim_user_temp2.col("EMAIL"),
							dim_user_temp2.col("FIRST_NAME"), dim_user_temp2.col("LAST_NAME"),
							dim_user_temp2.col("NUMBER"), dim_user_temp2.col("PULSE_ID"),
							dim_user_temp2.col("USER_STATUS"), dim_user_temp2.col("TENANT_ID"),
							dim_user_temp2.col("TENANT_NAME"), dim_user_temp2.col("TENANT_STATUS"),
							dim_user_temp2.col("ROLE_NAME"), dim_user_temp2.col("INDUSTRY_ID"),
							dim_user_temp2.col("INDUSTRY_NAME"), dim_user_temp2.col("REGION_ID"),
							dim_user_temp2.col("REGION_NAME"), dim_user_temp2.col("REGION_CURRENCY"),
							dim_user_temp2.col("LOCATION_CURRENCY"), dim_user_temp2.col("LOCATION_GROUP_NAME"),
							dim_user_temp2.col("LOCATION_GROUP_STATUS"), dim_user_temp2.col("IS_DEFAULT_GROUP"),
							dim_user_temp2.col("GROUP_TYPE_ID"), dim_user_temp2.col("GROUP_TYPE_NAME"),
							dim_user_temp2.col("IS_DEFAULT_TYPE"), dim_user_temp2.col("LOCATION_GROUP_HIERARCHY"),
							dim_user_temp2.col("TENANT_LOCATION_ID"), dim_user_temp2.col("TENANT_LOCATION_NAME"),
							dim_user_temp2.col("TENANT_LOCATION_STATUS"), dim_user_temp2.col("IS_CURRENTLY_ASSIGNED"),
							dim_user_temp2.col("ACTIVE_STATUS"), dim_user_temp2.col("CREATED_ON"),
							dim_user_temp2.col("REGION_HIERARCHY"))
					.persist(StorageLevel.MEMORY_ONLY_SER());

			Dataset<Row> dim_user_temp1 = dim_user_temp.union(dim_user_temp2);

			dim_user_temp1.createOrReplaceTempView("dim_user_temp");

		}

		update_dimusertemp();
		update_dimlocationgrouptemp();
		update_dimproducttemp();
		update_dimtenantlocationtemp();
		update_currencyforpartialsp();
	}

	public void update_dimusertemp() throws AnalysisException {

		Dataset<Row> finalRegionHierarchy = procedure1.commonupdate();

		Dataset<Row> dim_user_temp2data = sparkSession.sql("Select * from dim_user_temp");

		Dataset<Row> dim_user_temp2data123 = dim_user_temp2data
				.join(finalRegionHierarchy,
						finalRegionHierarchy.col("ID").equalTo(dim_user_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		dim_user_temp2data123.createOrReplaceTempView("dim_user_temp");
		logger.warn("-------------------------------update Complete----------------------");
	}

	public void update_dimlocationgrouptemp() throws AnalysisException {

		Dataset<Row> finalRegionHierarchy = procedure1.commonupdate();

		Dataset<Row> dim_locationgroup_temp2data = sparkSession.sql("Select * from dim_location_group_temp");

		Dataset<Row> dimlocationgrouptemp = dim_locationgroup_temp2data.join(finalRegionHierarchy,
				finalRegionHierarchy.col("ID").equalTo(dim_locationgroup_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		dimlocationgrouptemp.createOrReplaceTempView("dim_location_group_temp");
		logger.warn("-------------------------------update Complete----------------------");

	}

	public void update_dimproducttemp() throws AnalysisException {

		Dataset<Row> finalRegionHierarchy = procedure1.commonupdate();

		Dataset<Row> dim_product_temp2data = sparkSession.sql("Select * from dim_product_temp");

		Dataset<Row> dimproducttemp2data123 = dim_product_temp2data
				.join(finalRegionHierarchy,
						finalRegionHierarchy.col("ID").equalTo(dim_product_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		dimproducttemp2data123.createOrReplaceTempView("dim_product_temp");
		logger.warn("-------------------------------update Complete----------------------");

	}

	public void update_dimtenantlocationtemp() throws AnalysisException {
		Dataset<Row> finalRegionHierarchy = procedure1.commonupdate();

		Dataset<Row> dim_tenantlocation_temp2data = sparkSession.sql("Select * from dim_tenant_location_temp");

		Dataset<Row> dimtenantlocation2data123 = dim_tenantlocation_temp2data.join(finalRegionHierarchy,
				finalRegionHierarchy.col("ID").equalTo(dim_tenantlocation_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		dimtenantlocation2data123.createOrReplaceTempView("dim_tenant_location_temp");

		logger.warn(
				"-------------------------------update_dimtenantlocationtemp update Complete----------------------");

	}

	public void update_currencyforpartialsp() throws AnalysisException {

		logger.warn("-----------------------------update_currencyforpartialsp update Start----------------------");

		sparkSession.catalog().dropTempView("dim_tenant_location_temp2");
		sparkSession.catalog().dropTempView("dim_location_group_temp2");
		sparkSession.catalog().dropTempView("dim_product_temp2");
		sparkSession.catalog().dropTempView("dim_user_temp2");

		Dataset<Row> tenant_loc = spark.dbSession("tenant_location").withColumnRenamed("id", "tenant_loc_id")
				.select("tenant_loc_id", "local_currency_id").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> curr = spark.dbSession("currency").withColumnRenamed("id", "curr_id").select("curr_id", "code")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> dim_user_temp2 = sparkSession.sql("Select * from dim_user_temp");
		Dataset<Row> dim_location_group_temp2 = sparkSession.sql("Select * from dim_location_group_temp");
		Dataset<Row> dim_product_temp2 = sparkSession.sql("Select * from dim_product_temp");
		Dataset<Row> dim_tenant_location_temp2 = sparkSession.sql("Select * from dim_tenant_location_temp");

		dim_user_temp2 = dim_user_temp2
				.join(tenant_loc, tenant_loc.col("tenant_loc_id").equalTo(dim_user_temp2.col("tenant_location_id")),
						"left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_user_temp2 = dim_user_temp2.drop("tenant_loc_id").drop("local_currency_id").drop("curr_id").drop("code")
				.drop("ID").drop("LOCATION_ID").drop("VR_REGION_HIERARCHY").drop("FINAL_REGION_HIERARCHY");

		// dim_user_temp2.createOrReplaceTempView("dim_user_temp");

		Long id = 0L;
		spark.sparkSqlWriteToMainDB(dim_user_temp2.withColumn("ID", functions.lit(id)), "dim_user_temp");
		sparkSession.catalog().dropTempView("dim_user_temp");

		dim_location_group_temp2 = dim_location_group_temp2.join(tenant_loc,
				tenant_loc.col("tenant_loc_id").equalTo(dim_location_group_temp2.col("tenant_location_id")), "left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_location_group_temp2 = dim_location_group_temp2.drop("tenant_loc_id").drop("local_currency_id")
				.drop("curr_id").drop("code").drop("ID").drop("LOCATION_ID").drop("VR_REGION_HIERARCHY")
				.drop("FINAL_REGION_HIERARCHY");

		dim_location_group_temp2 = dim_location_group_temp2.select(dim_location_group_temp2.col("LOCATION_GROUP_ID"),
				dim_location_group_temp2.col("LOCATION_GROUP_NAME"),
				dim_location_group_temp2.col("LOCATION_GROUP_STATUS"),
				dim_location_group_temp2.col("PARENT_LOCATION_GROUP_ID").cast("int").as("PARENT_LOCATION_GROUP_ID"),
				dim_location_group_temp2.col("PARENT_LOCATION_GROUP_NAME"),
				dim_location_group_temp2.col("TENANT_LOCATION_ID"),
				dim_location_group_temp2.col("TENANT_LOCATION_NAME"),
				dim_location_group_temp2.col("TENANT_LOCATION_STATUS"), dim_location_group_temp2.col("TENANT_ID"),
				dim_location_group_temp2.col("TENANT_NAME"), dim_location_group_temp2.col("TENANT_STATUS"),
				dim_location_group_temp2.col("INDUSTRY_ID"), dim_location_group_temp2.col("INDUSTRY_NAME"),
				dim_location_group_temp2.col("GROUP_TYPE_ID"), dim_location_group_temp2.col("GROUP_TYPE_NAME"),
				dim_location_group_temp2.col("IS_DEFAULT_TYPE"), dim_location_group_temp2.col("IS_DEFAULT_GROUP"),
				dim_location_group_temp2.col("LOCATION_GROUP_HIERARCHY"), dim_location_group_temp2.col("ACTIVE_STATUS"),
				dim_location_group_temp2.col("REGION_HIERARCHY"), dim_location_group_temp2.col("CREATED_ON"),
				dim_location_group_temp2.col("REGION_CURRENCY"), dim_location_group_temp2.col("LOCATION_CURRENCY"),
				dim_location_group_temp2.col("REGION_ID"), dim_location_group_temp2.col("REGION_NAME"));

		spark.sparkSqlWriteToMainDB(dim_location_group_temp2.withColumn("ID", functions.lit(id)),
				"dim_location_group_temp");
		sparkSession.catalog().dropTempView("dim_location_group_temp");
		// dim_location_group_temp2.createOrReplaceTempView("dim_location_group_temp");

		dim_product_temp2 = dim_product_temp2
				.join(tenant_loc, tenant_loc.col("tenant_loc_id").equalTo(dim_product_temp2.col("tenant_location_id")),
						"left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_product_temp2 = dim_product_temp2.drop("tenant_loc_id").drop("local_currency_id").drop("curr_id")
				.drop("code").drop("ID").drop("LOCATION_ID").drop("VR_REGION_HIERARCHY").drop("FINAL_REGION_HIERARCHY");

		spark.sparkSqlWriteToMainDB(dim_product_temp2.withColumn("ID", functions.lit(id)), "dim_product_temp");
		sparkSession.catalog().dropTempView("dim_product_temp");
		// dim_product_temp2.createOrReplaceTempView("dim_product_temp");

		dim_tenant_location_temp2 = dim_tenant_location_temp2.join(tenant_loc,
				tenant_loc.col("tenant_loc_id").equalTo(dim_tenant_location_temp2.col("tenant_location_id")), "left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_tenant_location_temp2 = dim_tenant_location_temp2.drop("tenant_loc_id").drop("local_currency_id")
				.drop("curr_id").drop("code").drop("ID").drop("LOCATION_ID").drop("VR_REGION_HIERARCHY")
				.drop("FINAL_REGION_HIERARCHY");

		dim_tenant_location_temp2 = dim_tenant_location_temp2
				// .withColumn("ID",
				// functions.monotonically_increasing_id().cast("int"))
				.withColumn("launchDate",
						functions.when(
								dim_tenant_location_temp2.col("launchDate")
										.gt(functions.lit("1971-01-01 00:00:00").cast("timestamp")),
								dim_tenant_location_temp2.col("launchDate")).otherwise("1971-01-01 00:00:01"));

		// dim_tenant_location_temp2.printSchema();
		// dim_tenant_location_temp2.show();
		spark.sparkSqlWriteToMainDB(dim_tenant_location_temp2.withColumn("ID", functions.lit(id)),
				"dim_tenant_location_temp");
		// dim_tenant_location_temp2.createOrReplaceTempView("dim_tenant_location_temp");
		sparkSession.catalog().dropTempView("dim_tenant_location_temp");

		logger.warn("-----------------------------update_currencyforpartialsp update Complete----------------------");

	}

}