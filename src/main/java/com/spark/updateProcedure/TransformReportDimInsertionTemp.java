package com.spark.updateProcedure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class TransformReportDimInsertionTemp {
	final static Logger logger = Logger.getLogger(TransformReportDimInsertionTemp.class);

	sqlConnection spark = new sqlConnection();
	SparkSession sparkSession = spark.getSpark();

	Dataset<Row> dim_tenant_location = spark.dbSession("dim_tenant_location").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> dim_location_group = spark.dbSession("dim_location_group").persist(StorageLevel.MEMORY_ONLY_SER());

	Dataset<Row> tenant_location_profile = spark.dbSession("tenant_location_profile")
			.persist(StorageLevel.MEMORY_ONLY_SER());

	Dataset<Row> tenant = spark.dbSession("tenant").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> rr = spark.dbSession("region").withColumnRenamed("ID", "REGION_ID")
			.withColumnRenamed("NAME", "REGION_NAME").withColumnRenamed("currency", "REGION_CURRENCY")
			.withColumnRenamed("REGION_HIERARCHY", "REGION_HIERARCHY")
			.select("REGION_ID", "REGION_NAME", "REGION_CURRENCY", "REGION_HIERARCHY")
			.persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> industry = spark.dbSession("industry").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> tenant_location = spark.dbSession("tenant_location").persist(StorageLevel.MEMORY_ONLY_SER());

	Dataset<Row> user = spark.dbSession("user");
	Dataset<Row> location_group_type = spark.dbSession("location_group_type").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> lg = spark.dbSession("location_group").withColumnRenamed("ID", "LOCATION_GROUP_ID")
			.withColumnRenamed("ACTIVE_STATUS", "LOCATION_GROUP_STATUS")
			.withColumnRenamed("NAME", "LOCATION_GROUP_NAME")
			.select("LOCATION_GROUP_ID", "LOCATION_GROUP_STATUS", "LOCATION_GROUP_NAME", "LOCATION_GROUP_HIERARCHY",
					"GROUP_TYPE_ID", "TENANT_LOCATION_ID", "PARENT_GROUP_ID")
			.persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> plg = spark.dbSession("location_group").withColumnRenamed("ID", "LOCATION_GROUP_ID")
			.withColumnRenamed("ACTIVE_STATUS", "LOCATION_GROUP_STATUS")
			.withColumnRenamed("NAME", "LOCATION_GROUP_NAME")
			.select("LOCATION_GROUP_ID", "LOCATION_GROUP_STATUS", "LOCATION_GROUP_NAME", "LOCATION_GROUP_HIERARCHY",
					"GROUP_TYPE_ID", "TENANT_LOCATION_ID", "PARENT_GROUP_ID")
			.persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> user_profile = spark.dbSession("user_profile").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> currency = spark.dbSession("currency").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> location_group_product = spark.dbSession("location_group_product")
			.persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> location_group_profile = spark.dbSession("location_group_profile")
			.persist(StorageLevel.MEMORY_ONLY_SER());

	public void tenant_locations() throws AnalysisException {
		logger.warn("-------------------------------dim_tenant_location_temp2 started ----------------------");

		tenant_location.createOrReplaceTempView("tenant_location");
		location_group_product.createOrReplaceTempView("location_group_product");
		
		Dataset<Row> tenantLocationDataset = tenant_location
				.join(currency, tenant_location.col("LOCAL_CURRENCY_ID").equalTo(currency.col("ID")), "left")
				.join(tenant_location_profile, tenant_location.col("ID").equalTo(tenant_location_profile.col("ID")))
				.join(tenant, tenant_location.col("TENANT_ID").equalTo(tenant.col("ID")))
				.join(industry, tenant.col("INDUSTRY_ID").equalTo(industry.col("ID")))
				.join(rr, tenant_location.col("REGION_ID").equalTo(rr.col("REGION_ID")));

		Dataset<Row> selectedDataSet = tenantLocationDataset
				.select(tenant_location.col("ID").as("TENANT_LOCATION_ID"),
						tenant_location.col("NAME").as("TENANT_LOCATION_NAME"),
						tenant_location.col("ACTIVE_STATUS").as("TENANT_LOCATION_STATUS"),
						tenant.col("ID").as("TENANT_ID"), tenant.col("NAME").as("TENANT_NAME"),
						tenant.col("ACTIVE_STATUS").as("TENANT_STATUS"), industry.col("ID").as("INDUSTRY_ID"),
						industry.col("NAME").as("INDUSTRY_NAME"), rr.col("REGION_ID"), rr.col("REGION_NAME"),
						rr.col("REGION_CURRENCY"),
						functions.when(currency.col("CODE").isNull(), rr.col("REGION_CURRENCY"))
								.otherwise(currency.col("CODE")).as("LOCATION_CURRENCY"),
						tenant_location_profile.col("locationID"), tenant_location_profile.col("launchDate"),
						tenant_location.col("hotelMetricsDataType"),
						tenant_location.col("productMetricsValidationType"))
				.withColumn("ACTIVE_STATUS", functions.lit(0)).withColumn("CREATED_ON", functions.current_timestamp())
				.persist(StorageLevel.MEMORY_ONLY_SER());

		selectedDataSet.createOrReplaceTempView("dim_tenant_location_temp2");

		logger.warn("-------------------------------dim_tenant_location_temp2  Complete ----------------------");
		location_group_dimension();
		dim_product_temp2();
		dim_user_temp2();
	}

	public void location_group_dimension() throws AnalysisException {
		logger.warn("-------------------------------dim_location_group_temp2 started----------------------");
		Dataset<Row> op = spark.dbSession("location_group").cache();

		Dataset<Row> location_group_dim = lg
				.join(tenant_location, lg.col("TENANT_LOCATION_ID").equalTo(tenant_location.col("ID")))
				.join(currency, tenant_location.col("LOCAL_CURRENCY_ID").equalTo(currency.col("ID")), "left")
				.join(tenant, tenant_location.col("TENANT_ID").equalTo(tenant.col("ID")))
				.join(industry, tenant.col("INDUSTRY_ID").equalTo(industry.col("ID")))
				.join(rr, rr.col("REGION_ID").equalTo(tenant_location.col("REGION_ID")))
				.join(location_group_type, location_group_type.col("ID").equalTo(lg.col("GROUP_TYPE_ID")))
				.join(op, lg.col("PARENT_GROUP_ID").equalTo(op.col("ID")))
				.join(location_group_profile, lg.col("LOCATION_GROUP_ID").equalTo(location_group_profile.col("ID")))
				.join(plg, lg.col("PARENT_GROUP_ID").equalTo(plg.col("LOCATION_GROUP_ID")))
				.select(lg.col("LOCATION_GROUP_ID"), lg.col("LOCATION_GROUP_NAME"), lg.col("LOCATION_GROUP_STATUS"),
						plg.col("LOCATION_GROUP_ID").as("PARENT_LOCATION_GROUP_ID"),
						plg.col("LOCATION_GROUP_NAME").as("PARENT_LOCATION_GROUP_NAME"),
						tenant_location.col("ID").as("TENANT_LOCATION_ID"),
						tenant_location.col("NAME").as("TENANT_LOCATION_NAME"),
						tenant_location.col("ACTIVE_STATUS").as("TENANT_LOCATION_STATUS"),
						tenant.col("ID").as("TENANT_ID"), tenant.col("NAME").as("TENANT_NAME"),
						tenant.col("ACTIVE_STATUS").as("TENANT_STATUS"), industry.col("ID").as("INDUSTRY_ID"),
						industry.col("NAME").as("INDUSTRY_NAME"), rr.col("REGION_ID"), rr.col("REGION_NAME"),
						rr.col("REGION_CURRENCY"),
						functions.when(currency.col("CODE").isNull(), rr.col("REGION_CURRENCY"))
								.otherwise(currency.col("CODE")).as("LOCATION_CURRENCY"),
						location_group_type.col("ID").as("GROUP_TYPE_ID"),
						location_group_type.col("NAME").as("GROUP_TYPE_NAME"),
						location_group_type.col("IS_DEFAULT").as("IS_DEFAULT_TYPE"),
						location_group_profile.col("IS_DEFAULT").as("IS_DEFAULT_GROUP"),
						lg.col("LOCATION_GROUP_HIERARCHY"))
				.withColumn("ACTIVE_STATUS", functions.lit(0)).withColumn("CREATED_ON", functions.current_timestamp())
				.persist(StorageLevel.MEMORY_ONLY_SER());

		location_group_dim.createOrReplaceTempView("dim_location_group_temp2");

		logger.warn("-------------------------------dim_location_group_temp2 Complete----------------------");
	}

	public void dim_product_temp2() {
		logger.warn("-------------------------------dim_product_temp2 start----------------------");
		Dataset<Row> location_group_product = spark.dbSession("location_group_product");
		Dataset<Row> tenant_product = spark.dbSession("tenant_product");
		Dataset<Row> tenant_product_category = spark.dbSession("tenant_product_category");

		Dataset<Row> dim_pt = location_group_product
				.join(tenant_product, location_group_product.col("TENANT_PRODUCT_ID").equalTo(tenant_product.col("ID")))
				.join(tenant_product_category,
						tenant_product.col("TENANT_PRODUCT_CATEGORY_ID").equalTo(tenant_product_category.col("ID")))
				.join(lg, location_group_product.col("LOCATION_GROUP_ID").equalTo(lg.col("LOCATION_GROUP_ID")))
				.join(tenant_location, lg.col("TENANT_LOCATION_ID").equalTo(tenant_location.col("ID")))
				.join(currency, tenant_location.col("LOCAL_CURRENCY_ID").equalTo(currency.col("ID")), "left")
				.join(tenant,
						tenant_location.col("TENANT_ID").equalTo(tenant.col("ID"))
								.and(tenant_product_category.col("TENANT_ID").equalTo(tenant.col("ID"))))
				.join(industry, tenant.col("INDUSTRY_ID").equalTo(industry.col("ID")))
				.join(rr, rr.col("REGION_ID").equalTo(tenant_location.col("REGION_ID")))
				.select(location_group_product.col("ID").as("LOCATION_GROUP_PRODUCT_ID"),
						tenant_product.col("ID").as("TENANT_PRODUCT_ID"),
						tenant_product.col("NAME").as("TENANT_PRODUCT_NAME"),
						functions.when(location_group_product.col("NAME").isNull(), tenant_product.col("name"))
								.otherwise(location_group_product.col("NAME")).as("LOCATION_GROUP_PRODUCT_NAME"),
						location_group_product.col("ACTIVE_STATUS").as("LOCATION_GROUP_PRODUCT_STATUS"),
						location_group_product.col("isMajor"), location_group_product.col("isRecurring"),
						tenant_product_category.col("ID").as("TENANT_PRODUCT_CATEGORY_ID"),
						tenant_product_category.col("NAME").as("TENANT_PRODUCT_CATEGORY_NAME"),
						lg.col("LOCATION_GROUP_ID"), lg.col("LOCATION_GROUP_NAME"), lg.col("LOCATION_GROUP_STATUS"),
						tenant_location.col("ID").as("TENANT_LOCATION_ID"),
						tenant_location.col("NAME").as("TENANT_LOCATION_NAME"),
						tenant_location.col("ACTIVE_STATUS").as("TENANT_LOCATION_STATUS"),
						tenant.col("ID").as("TENANT_ID"), tenant.col("NAME").as("TENANT_NAME"),
						tenant.col("ACTIVE_STATUS").as("TENANT_STATUS"), industry.col("ID").as("INDUSTRY_ID"),
						industry.col("NAME").as("INDUSTRY_NAME"), rr.col("REGION_ID"), rr.col("REGION_NAME"),
						rr.col("REGION_CURRENCY"),
						functions.when(currency.col("CODE").isNull(), rr.col("REGION_CURRENCY"))
								.otherwise(currency.col("CODE")).as("LOCATION_CURRENCY"))
				.withColumn("ACTIVE_STATUS", functions.lit(0)).withColumn("CREATED_ON", functions.current_timestamp())
				.persist(StorageLevel.MEMORY_ONLY_SER());

		dim_pt.createOrReplaceTempView("dim_product_temp2");

		logger.warn("-------------------------------dim_product_temp2 ended----------------------");

	}

	public void dim_user_temp2() throws AnalysisException {
		logger.warn("-------------------------------dim_user_temp2 started----------------------");
		Dataset<Row> user_location_group = spark.dbSession("user_location_group");
		Dataset<Row> user = spark.dbSession("user");
		Dataset<Row> location_group_profile = spark.dbSession("location_group_profile");
		Dataset<Row> user_profile = spark.dbSession("user_profile");
		Dataset<Row> user_tenant_role = spark.dbSession("user_tenant_role");
		Dataset<Row> role = spark.dbSession("role");

		Dataset<Row> leftDataSet = user_tenant_role
				.join(tenant, user_tenant_role.col("TENANT_ID").equalTo(tenant.col("ID")))
				.join(industry, industry.col("ID").equalTo(tenant.col("INDUSTRY_ID")))
				.join(role,
						user_tenant_role.col("ROLE_ID").equalTo(role.col("ID"))
								.and(role.col("TENANT_ID").equalTo(tenant.col("ID"))))
				.select(user_tenant_role.col("USER_ID").as("userID"), tenant.col("ID").as("tenantID"),
						tenant.col("NAME").as("tenantName"), tenant.col("ACTIVE_STATUS").as("tenantStatus"),
						role.col("ID").as("roleID"), role.col("NAME").as("roleName"),
						industry.col("ID").as("industryID"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> dim_user_temp2 = user_location_group
				.join(user, user.col("id").equalTo(user_location_group.col("USER_ID")))
				.join(user_profile, user.col("ID").equalTo(user_profile.col("ID")))
				.join(lg, user_location_group.col("LOCATION_GROUP_ID").equalTo(lg.col("LOCATION_GROUP_ID")))
				.join(location_group_profile, location_group_profile.col("ID").equalTo(lg.col("LOCATION_GROUP_ID")))
				.join(location_group_type, lg.col("GROUP_TYPE_ID").equalTo(location_group_type.col("ID")))
				.join(tenant_location, lg.col("TENANT_LOCATION_ID").equalTo(tenant_location.col("ID")))
				.join(currency, tenant_location.col("LOCAL_CURRENCY_ID").equalTo(currency.col("ID")), "left")
				.join(tenant, tenant_location.col("TENANT_ID").equalTo(tenant.col("ID")))
				.join(industry, industry.col("ID").equalTo(tenant.col("INDUSTRY_ID")))
				.join(rr, rr.col("REGION_ID").equalTo(tenant_location.col("REGION_ID")))
				.join(leftDataSet,
						leftDataSet.col("userID").equalTo(user.col("ID"))
								.and(tenant.col("ID").equalTo(leftDataSet.col("tenantID"))
										.and(industry.col("id").equalTo(leftDataSet.col("industryID")))),
						"left")
				.select(user.col("ID").as("USER_ID"), user.col("EMAIL"), user_profile.col("FIRST_NAME"),
						user_profile.col("LAST_NAME"), user_profile.col("NUMBER"), user_profile.col("PULSE_ID"),
						user.col("ACTIVE_STATUS").alias("USER_STATUS"), tenant.col("ID").alias("TENANT_ID"),
						tenant.col("NAME").as("TENANT_NAME"), tenant.col("ACTIVE_STATUS").as("TENANT_STATUS"),
						// user.col("roleID").as("ROLE_ID"),
						leftDataSet.col("roleID").as("ROLE_ID"), leftDataSet.col("roleName").alias("ROLE_NAME"),
						industry.col("ID").as("INDUSTRY_ID"), industry.col("NAME").alias("INDUSTRY_NAME"),
						rr.col("REGION_ID"), rr.col("REGION_NAME"), rr.col("REGION_CURRENCY"),
						functions.when(currency.col("CODE").isNull(), rr.col("REGION_CURRENCY"))
								.otherwise(currency.col("CODE")).as("LOCATION_CURRENCY"),
						lg.col("LOCATION_GROUP_ID"), lg.col("LOCATION_GROUP_NAME"), lg.col("LOCATION_GROUP_STATUS"),
						location_group_profile.col("IS_DEFAULT").as("IS_DEFAULT_GROUP"),
						location_group_type.col("ID").as("GROUP_TYPE_ID"),
						location_group_type.col("NAME").as("GROUP_TYPE_NAME"),
						location_group_type.col("IS_DEFAULT").as("IS_DEFAULT_TYPE"),
						user_location_group.col("IS_CONTRIBUTOR"), user_location_group.col("IS_OPERATOR"),
						lg.col("LOCATION_GROUP_HIERARCHY").as("LOCATION_GROUP_HIERARCHY"),
						tenant_location.col("ID").as("TENANT_LOCATION_ID"),
						tenant_location.col("NAME").as("TENANT_LOCATION_NAME"),
						tenant_location.col("ACTIVE_STATUS").as("TENANT_LOCATION_STATUS"),
						user_location_group.col("USER_ID").as("user1"))
				.withColumn("IS_CURRENTLY_ASSIGNED", functions.lit(1)).withColumn("ACTIVE_STATUS", functions.lit(0))
				.withColumn("CREATED_ON", functions.current_timestamp())
				.groupBy("LOCATION_GROUP_ID", "user1", "IS_CONTRIBUTOR", "IS_OPERATOR", "ROLE_ID")
				.agg(functions.first("USER_ID").as("USER_ID"), functions.first("EMAIL").as("EMAIL"),
						functions.first("FIRST_NAME").as("FIRST_NAME"), functions.first("LAST_NAME").as("LAST_NAME"),
						functions.first("NUMBER").as("NUMBER"), functions.first("PULSE_ID").as("PULSE_ID"),
						functions.first("USER_STATUS").as("USER_STATUS"), functions.first("TENANT_ID").as("TENANT_ID"),
						functions.first("TENANT_NAME").as("TENANT_NAME"),
						functions.first("TENANT_STATUS").as("TENANT_STATUS"),
						functions.first("ROLE_NAME").as("ROLE_NAME"), functions.first("INDUSTRY_ID").as("INDUSTRY_ID"),
						functions.first("INDUSTRY_NAME").as("INDUSTRY_NAME"),
						functions.first("REGION_ID").as("REGION_ID"), functions.first("REGION_NAME").as("REGION_NAME"),
						functions.first("REGION_CURRENCY").as("REGION_CURRENCY"),
						functions.first("LOCATION_CURRENCY").as("LOCATION_CURRENCY"),
						functions.first("LOCATION_GROUP_NAME").as("LOCATION_GROUP_NAME"),
						functions.first("LOCATION_GROUP_STATUS").as("LOCATION_GROUP_STATUS"),
						functions.first("IS_DEFAULT_GROUP").as("IS_DEFAULT_GROUP"),
						functions.first("GROUP_TYPE_ID").as("GROUP_TYPE_ID"),
						functions.first("GROUP_TYPE_NAME").as("GROUP_TYPE_NAME"),
						functions.first("IS_DEFAULT_TYPE").as("IS_DEFAULT_TYPE"),
						functions.first("LOCATION_GROUP_HIERARCHY").as("LOCATION_GROUP_HIERARCHY"),
						functions.first("TENANT_LOCATION_ID").as("TENANT_LOCATION_ID"),
						functions.first("TENANT_LOCATION_NAME").as("TENANT_LOCATION_NAME"),
						functions.first("TENANT_LOCATION_STATUS").as("TENANT_LOCATION_STATUS"),
						functions.first("IS_CURRENTLY_ASSIGNED").as("IS_CURRENTLY_ASSIGNED"),
						functions.first("ACTIVE_STATUS").as("ACTIVE_STATUS"),
						functions.first("CREATED_ON").as("CREATED_ON"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> dim_user_temp = dim_user_temp2.drop("user1");
		dim_user_temp.createOrReplaceTempView("dim_user_temp2");

		logger.warn("-------------------------------dim_user_temp2 Complete----------------------");
		temp_table(dim_user_temp);
	}

	public void temp_table(Dataset<Row> dim_user_temp2) throws AnalysisException {
		Dataset<Row> temp1206 = dim_user_temp2
				.groupBy("USER_ID", "INDUSTRY_ID", "TENANT_ID", "ROLE_ID", "TENANT_LOCATION_ID", "LOCATION_GROUP_ID",
						"GROUP_TYPE_ID")
				.agg(functions.concat_ws("", dim_user_temp2.col("USER_ID"), functions.lit("~"),
						dim_user_temp2.col("GROUP_TYPE_ID"), functions.lit("~"), dim_user_temp2.col("INDUSTRY_ID"),
						functions.lit("~"), dim_user_temp2.col("LOCATION_GROUP_ID"), functions.lit("~"),
						dim_user_temp2.col("TENANT_ID"), functions.lit("~"), dim_user_temp2.col("TENANT_LOCATION_ID"))
						.as("mergeIDs"))
				.select("mergeIDs").persist(StorageLevel.MEMORY_AND_DISK_SER());

		logger.warn("-------------------------------dim_user_temp2 group By----------------------");
		temp1206merged(temp1206);
	}

	public void temp1206merged(Dataset<Row> temp1206) throws AnalysisException {

		temp1206 = temp1206.select(temp1206.col("mergeIDs")).distinct().as("mergeIDs");

		Dataset<Row> temp1206merged = temp1206
				.agg(functions.concat_ws(",", functions.collect_list(temp1206.col("mergeIDs"))).as("mergeIDs"));

		temp1206merged.createOrReplaceTempView("temp1206merged");
		temp1206.createOrReplaceTempView("temp1206");
		logger.warn("-------------------------------temp1206merged Complete----------------------");

		temp1206_main();
	}

	public void temp1206_main() throws AnalysisException {
		
		Dataset<Row> merged = sparkSession.sql("select mergeIDs FROM temp1206merged");
		List<Row> temp = new ArrayList<>();
		temp = merged.collectAsList();
		String ids = temp.size() == 0 ? "" : temp.get(0).toString().substring(1, temp.get(0).toString().length() - 1);
		Object id[] = ids.split(",");
		Iterable<Object> iterable = Arrays.asList(id);
		
		Dataset<Row> product_metric = spark.dbSession("product_metric").select("USER_ID", "LOCATION_GROUP_ID")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> user_tenant_role = spark.dbSession("user_tenant_role").select("USER_ID", "TENANT_ID", "ROLE_ID")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> role = spark.dbSession("role").select("NAME", "ID", "TENANT_ID")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> leftDataSet = user_tenant_role
				.join(tenant, user_tenant_role.col("TENANT_ID").equalTo(tenant.col("ID")))
				.join(industry, industry.col("ID").equalTo(tenant.col("INDUSTRY_ID")))
				.join(role,
						user_tenant_role.col("ROLE_ID").equalTo(role.col("ID"))
								.and(role.col("TENANT_ID").equalTo(tenant.col("ID"))))
				.select(user_tenant_role.col("USER_ID").as("userID"), tenant.col("ID").as("tenantID"),
						tenant.col("ACTIVE_STATUS").as("tenantStatus"), role.col("ID").as("roleID"),
						role.col("NAME").as("ROLE_NAME"), industry.col("ID").as("industryID"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> uu = user
				.select(user.col("EMAIL"), user.col("ACTIVE_STATUS").alias("USER_STATUS"), user.col("id").as("USER_ID"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> up = user_profile.select(user_profile.col("FIRST_NAME"), user_profile.col("LAST_NAME"),
				user_profile.col("NUMBER"), user_profile.col("PULSE_ID"), user_profile.col("ID"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> tt = tenant
				.select(tenant.col("ID").as("TENANT_ID"), tenant.col("NAME").as("TENANT_NAME"),
						tenant.col("ACTIVE_STATUS").as("TENANT_STATUS"), tenant.col("INDUSTRY_ID"))
				.repartition(150).persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> lgt = location_group_type
				.select(location_group_type.col("ID").as("GROUP_TYPE_ID"),
						location_group_type.col("NAME").as("GROUP_TYPE_NAME"),
						location_group_type.col("IS_DEFAULT").as("IS_DEFAULT_TYPE"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> tl = tenant_location.select(tenant_location.col("ID").as("TENANT_LOCATION_ID"),
				tenant_location.col("NAME").as("TENANT_LOCATION_NAME"),
				tenant_location.col("ACTIVE_STATUS").as("TENANT_LOCATION_STATUS"), tenant_location.col("TENANT_ID"),
				tenant_location.col("REGION_ID"), tenant_location.col("LOCAL_CURRENCY_ID"));

		Dataset<Row> lgp = location_group_profile.select(location_group_profile.col("ID"),
				location_group_profile.col("IS_DEFAULT"));
		Dataset<Row> ii = industry.select(industry.col("ID").as("INDUSTRY_ID"),
				industry.col("NAME").as("INDUSTRY_NAME"));
		
		Dataset<Row> finalData = product_metric.join(uu, uu.col("USER_ID").equalTo(product_metric.col("USER_ID")))
				.join(up, uu.col("USER_ID").equalTo(up.col("ID")))
				.join(lg, product_metric.col("LOCATION_GROUP_ID").equalTo(lg.col("LOCATION_GROUP_ID")))
				.join(lgp, lgp.col("ID").equalTo(lg.col("LOCATION_GROUP_ID")))
				.join(lgt, lg.col("GROUP_TYPE_ID").equalTo(lgt.col("GROUP_TYPE_ID")))
				.join(tl, lg.col("TENANT_LOCATION_ID").equalTo(tl.col("TENANT_LOCATION_ID")))
				.join(currency, tl.col("LOCAL_CURRENCY_ID").equalTo(currency.col("ID")), "left")
				.join(tt, tl.col("TENANT_ID").equalTo(tt.col("TENANT_ID")))
				.join(ii, ii.col("INDUSTRY_ID").equalTo(tt.col("INDUSTRY_ID")))
				.join(rr, rr.col("REGION_ID").equalTo(tl.col("REGION_ID"))).join(leftDataSet,

						leftDataSet.col("userID").equalTo(uu.col("USER_ID"))
								.and(tt.col("TENANT_ID").equalTo(leftDataSet.col("tenantID")))
								.and(ii.col("INDUSTRY_ID").as("indust").equalTo(leftDataSet.col("industryID"))),
						"left")
				.filter(functions.not(functions.concat_ws("", uu.col("USER_ID"), functions.lit("~"), ii.col("INDUSTRY_ID"),
						functions.lit("~"), tt.col("TENANT_ID"), functions.lit("~"), tl.col("TENANT_LOCATION_ID"),
						functions.lit("~"), lg.col("LOCATION_GROUP_ID"), functions.lit("~"), lgt.col("GROUP_TYPE_ID"))
						.as("mergeIDs1").isInCollection(iterable)))
				.select(functions.concat_ws("", uu.col("USER_ID"), functions.lit("~"), ii.col("INDUSTRY_ID"),
						functions.lit("~"), tt.col("TENANT_ID"), functions.lit("~"), tl.col("TENANT_LOCATION_ID"),
						functions.lit("~"), lg.col("LOCATION_GROUP_ID"), functions.lit("~"), lgt.col("GROUP_TYPE_ID"))
						.as("mergeIDs"), product_metric.col("USER_ID").as("USER_ID_GP"), uu.col("USER_ID"),
						ii.col("INDUSTRY_ID"), tt.col("TENANT_ID"), leftDataSet.col("roleId"), lgt.col("GROUP_TYPE_ID"),
						uu.col("EMAIL"), up.col("FIRST_NAME"), up.col("LAST_NAME"), lg.col("LOCATION_GROUP_ID"),
						up.col("NUMBER"), up.col("PULSE_ID"), uu.col("USER_STATUS"), tl.col("TENANT_LOCATION_ID"),
						tt.col("TENANT_NAME"), tt.col("TENANT_STATUS"), leftDataSet.col("ROLE_NAME"),
						ii.col("INDUSTRY_NAME"), rr.col("REGION_ID"), rr.col("REGION_NAME"), rr.col("REGION_CURRENCY"),
						functions.when(currency.col("CODE").isNull(), rr.col("REGION_CURRENCY"))
								.otherwise(currency.col("CODE")).as("LOCATION_CURRENCY"),
						lg.col("LOCATION_GROUP_NAME"), lg.col("LOCATION_GROUP_STATUS"),
						lgp.col("IS_DEFAULT").as("IS_DEFAULT_GROUP"), lgt.col("IS_DEFAULT_TYPE").as("IS_DEFAULT_TYPE"),
						lgt.col("GROUP_TYPE_NAME"), lg.col("LOCATION_GROUP_HIERARCHY"), tl.col("TENANT_LOCATION_NAME"),
						tl.col("TENANT_LOCATION_STATUS"))
				.withColumn("IS_CONTRIBUTOR", functions.lit(1)).withColumn("IS_OPERATOR", functions.lit(0))
				.withColumn("CREATED_ON", functions.current_timestamp())
				.withColumn("IS_CURRENTLY_ASSIGNED", functions.lit(0))
				.groupBy("LOCATION_GROUP_ID", "USER_ID_GP", "roleID")
				.agg(functions.first("mergeIDs").as("mergeIDs"), functions.first("USER_ID").as("USER_ID"),
						functions.first("INDUSTRY_ID").as("INDUSTRY_ID"), functions.first("TENANT_ID").as("TENANT_ID"),
						functions.first("GROUP_TYPE_ID").as("GROUP_TYPE_ID"), functions.first("EMAIL").as("EMAIL"),
						functions.first("FIRST_NAME").as("FIRST_NAME"), functions.first("LAST_NAME").as("LAST_NAME"),
						functions.first("NUMBER").as("NUMBER"), functions.first("PULSE_ID").as("PULSE_ID"),
						functions.first("USER_STATUS").as("USER_STATUS"),
						functions.first("TENANT_LOCATION_ID").as("TENANT_LOCATION_ID"),
						functions.first("TENANT_NAME").as("TENANT_NAME"),
						functions.first("TENANT_STATUS").as("TENANT_STATUS"),
						functions.first("ROLE_NAME").as("ROLE_NAME"),
						functions.first("INDUSTRY_NAME").as("INDUSTRY_NAME"),
						functions.first("REGION_ID").as("REGION_ID"), functions.first("REGION_NAME").as("REGION_NAME"),
						functions.first("REGION_CURRENCY").as("REGION_CURRENCY"),
						functions.first("LOCATION_CURRENCY").as("LOCATION_CURRENCY"),
						functions.first("LOCATION_GROUP_NAME").as("LOCATION_GROUP_NAME"),
						functions.first("LOCATION_GROUP_STATUS").as("LOCATION_GROUP_STATUS"),
						functions.first("IS_DEFAULT_GROUP").as("IS_DEFAULT_GROUP"),
						functions.first("IS_DEFAULT_TYPE").as("IS_DEFAULT_TYPE"),
						functions.first("GROUP_TYPE_NAME").as("GROUP_TYPE_NAME"),
						functions.first("LOCATION_GROUP_HIERARCHY").as("LOCATION_GROUP_HIERARCHY"),
						functions.first("TENANT_LOCATION_NAME").as("TENANT_LOCATION_NAME"),
						functions.first("TENANT_LOCATION_STATUS").as("TENANT_LOCATION_STATUS"),
						functions.first("IS_CONTRIBUTOR").as("IS_CONTRIBUTOR"),
						functions.first("IS_OPERATOR").as("IS_OPERATOR"),
						functions.first("CREATED_ON").as("CREATED_ON"),
						functions.first("IS_CURRENTLY_ASSIGNED").as("IS_CURRENTLY_ASSIGNED"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		finalData = finalData.drop("USER_ID_GP");
//		logger.warn("finalData count ==> "+finalData.count());
		finalData.createOrReplaceTempView("temp1206_main");

		dim_user_temp(finalData);
	}

	public void dim_user_temp(Dataset<Row> sql) throws AnalysisException {
		logger.warn("-------------------------------dim_user_temp started----------------------");

		Dataset<Row> merged = sparkSession.sql("select mergeIDs FROM temp1206merged");
		
//		List<Row> temp = new ArrayList<>();
//		temp = merged.collectAsList();
//		String ids = temp.size() == 0 ? "" : temp.get(0).toString().substring(1, temp.get(0).toString().length() - 1);
//		String id[] = ids.split(",");
//		Object ids1[] = ids.split(",");
//		Iterable<String> iterable = Arrays.asList(id);

		Dataset<Row> maintemp = sql
//				.filter(functions.not(sql.col("mergeIDs").isin(ids1)))
				.select(sql.col("mergeIDs"), sql.col("LOCATION_GROUP_ID"),
						sql.col("IS_CONTRIBUTOR").cast("boolean").as("IS_CONTRIBUTOR"),
						sql.col("IS_OPERATOR").cast("boolean").as("IS_OPERATOR"), sql.col("roleId").as("ROLE_ID"),
						sql.col("USER_ID"), sql.col("EMAIL"), sql.col("FIRST_NAME"), sql.col("LAST_NAME"),
						sql.col("NUMBER"), sql.col("PULSE_ID"), sql.col("USER_STATUS"), sql.col("TENANT_ID"),
						sql.col("TENANT_NAME"), sql.col("TENANT_STATUS"), sql.col("ROLE_NAME"), sql.col("INDUSTRY_ID"),
						sql.col("INDUSTRY_NAME"), sql.col("REGION_ID"), sql.col("REGION_NAME"),
						sql.col("REGION_CURRENCY"), sql.col("LOCATION_CURRENCY"), sql.col("LOCATION_GROUP_NAME"),
						sql.col("LOCATION_GROUP_STATUS"), sql.col("IS_DEFAULT_GROUP"), sql.col("GROUP_TYPE_ID"),
						sql.col("GROUP_TYPE_NAME"), sql.col("IS_DEFAULT_TYPE"), sql.col("LOCATION_GROUP_HIERARCHY"),
						sql.col("TENANT_LOCATION_ID"), sql.col("TENANT_LOCATION_NAME"),
						sql.col("TENANT_LOCATION_STATUS"), sql.col("IS_CURRENTLY_ASSIGNED"))
				.withColumn("ACTIVE_STATUS", functions.lit(0)).withColumn("CREATED_ON", functions.current_timestamp())
				.persist(StorageLevel.MEMORY_ONLY_SER());
		// maintemp.printSchema();
//		System.out.println("maintemp ==> " + maintemp.count());
		Dataset<Row> maintemp1 = maintemp.drop("mergeIDs");
		// maintemp1.printSchema();
//		System.out.println("maintemp1 ==> " + maintemp1.count());
		Dataset<Row> finalmaintemp1 = sparkSession.sql("Select * from dim_user_temp2");
		// finalmaintemp1.printSchema();
		// System.out.println("finalmaintemp1 ==> "+finalmaintemp1.count());
//		 System.out.println("finalmaintemp1 ==> "+finalmaintemp1.count());
//		 System.out.println("maintemp1 ==> "+maintemp1.count());

		Dataset<Row> finalmaintemp = maintemp1.union(finalmaintemp1);
		finalmaintemp.createOrReplaceTempView("dim_user_temp2");

		logger.warn("-------------------------------dim_user_temp Complete----------------------");

		// location_market_segment_temp2();
		update_dimusertemp();
		update_dimlocationgrouptemp();
		update_dimproducttemp();
		update_dimtenantlocationtemp();
		update_currencyforpartialsp();

		TransferDataFromDimTemp2ToDimTemp procedure2 = new TransferDataFromDimTemp2ToDimTemp();
		procedure2.mainEntryFunction();
	}

	/*
	 * public void location_market_segment_temp2() throws AnalysisException {
	 * logger.
	 * warn("-------------------------------location_market_segment_temp2 started----------------------"
	 * );
	 * 
	 * Dataset<Row> product_metric =
	 * spark.dbSession("product_metric").persist(StorageLevel.MEMORY_ONLY_SER())
	 * ;
	 * 
	 * Dataset<Row> location_market_segment_temp2 = product_metric
	 * .join(tenant_location,
	 * tenant_location.col("id").equalTo(product_metric.col("TENANT_LOCATION_ID"
	 * ))) .filter(product_metric.col("auditstatus").notEqual(functions.lit(2))
	 * .and(product_metric.col("auditstatus").notEqual(functions.lit(4)))
	 * .and(product_metric.col("auditstatus").notEqual(functions.lit(5))))
	 * .select(product_metric.col("TENANT_LOCATION_ID"), tenant_location
	 * .col("TENANT_ID"), functions
	 * .when(product_metric.col("marketSegment1").isNull()
	 * .or(product_metric.col("marketSegment1").equalTo("")),
	 * functions.lit("NA")) .otherwise( functions
	 * .upper(product_metric.col("marketSegment1"))) .as("marketSegment1"),
	 * functions .when(product_metric.col("marketSegment2").isNull()
	 * .or(product_metric.col("marketSegment2").equalTo("")),
	 * functions.lit("NA"))
	 * .otherwise(functions.upper(product_metric.col("marketSegment2"))).as(
	 * "marketSegment2")) .groupBy("TENANT_LOCATION_ID", "marketSegment1",
	 * "marketSegment2") .agg(functions.first("TENANT_ID").as("TENANT_ID"));
	 * 
	 * location_market_segment_temp2.createOrReplaceTempView(
	 * "location_market_segment_temp2");
	 * 
	 * logger.
	 * warn("-------------------------------location_market_segment_temp2 Complete----------------------"
	 * );
	 * 
	 * update_dimusertemp(); update_dimlocationgrouptemp();
	 * update_dimproducttemp(); update_dimtenantlocationtemp();
	 * update_currencyforpartialsp(); //
	 * Transfer_data_from_dim_temp2_to_dim_temp procedure2 = new //
	 * Transfer_data_from_dim_temp2_to_dim_temp(); //
	 * procedure2.mainEntryFunction(); }
	 */

	public Dataset<Row> commonupdate() throws AnalysisException {

		Dataset<Row> virtual_tenant_location = spark.dbSession("virtual_tenant_location")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> virtual_region = virtual_tenant_location
				.join(rr,
						rr.col("REGION_ID").equalTo(virtual_tenant_location.col("REGION_ID"))
								.and(virtual_tenant_location.col("ACTIVE_STATUS").equalTo(0)
										.and(rr.col("REGION_HIERARCHY").isNotNull())))
				.select(virtual_tenant_location.col("LOCATION_ID"), rr.col("REGION_HIERARCHY")).groupBy("LOCATION_ID")
				.agg(functions.concat_ws(",", functions.collect_list(rr.col("REGION_HIERARCHY")))
						.as("REGION_HIERARCHY"));

		Dataset<Row> finalRegionHierarchy = tenant_location
				.join(rr, rr.col("REGION_ID").equalTo(tenant_location.col("REGION_ID")), "left")
				.join(virtual_region, virtual_region.col("LOCATION_ID").equalTo(tenant_location.col("ID")),
						"left")
				.select(tenant_location.col("ID"), virtual_region.col("LOCATION_ID"),
						virtual_region.col("REGION_HIERARCHY").as("VR_REGION_HIERARCHY"),
						functions
								.when(functions.trim(virtual_region.col("REGION_HIERARCHY")).equalTo("").or(
										virtual_region.col("REGION_HIERARCHY").isNull()), rr.col("REGION_HIERARCHY"))
								.otherwise(functions.concat(rr.col("REGION_HIERARCHY"), functions.lit(","),
										virtual_region.col("REGION_HIERARCHY")))
								.as("FINAL_REGION_HIERARCHY"))
				.persist(StorageLevel.MEMORY_ONLY_SER());

		return finalRegionHierarchy;

	}

	public void update_dimusertemp() throws AnalysisException {

		Dataset<Row> finalRegionHierarchy = commonupdate();

		Dataset<Row> dim_user_temp2data = sparkSession.sql("Select * from dim_user_temp2");

		Dataset<Row> dim_user_temp2data123 = dim_user_temp2data
				.join(finalRegionHierarchy,
						finalRegionHierarchy.col("ID").equalTo(dim_user_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		Dataset<Row> dim_user_temp2 = dim_user_temp2data123.drop("ID").drop("LOCATION_ID").drop("VR_REGION_HIERARCHY")
				.drop("FINAL_REGION_HIERARCHY");

		dim_user_temp2.createOrReplaceTempView("dim_user_temp2");

		logger.warn("------------------------------ update_dimusertemp-update Complete----------------------");
	}

	public void update_dimlocationgrouptemp() throws AnalysisException {

		Dataset<Row> finalRegionHierarchy = commonupdate();

		Dataset<Row> dim_locationgroup_temp2data = sparkSession.sql("Select * from dim_location_group_temp2");

		Dataset<Row> dimlocationgrouptemp = dim_locationgroup_temp2data.join(finalRegionHierarchy,
				finalRegionHierarchy.col("ID").equalTo(dim_locationgroup_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		Dataset<Row> dimlocationgrouptemp1 = dimlocationgrouptemp.drop("ID").drop("LOCATION_ID")
				.drop("VR_REGION_HIERARCHY").drop("FINAL_REGION_HIERARCHY");

		dimlocationgrouptemp1.createOrReplaceTempView("dim_location_group_temp2");

		logger.warn(
				"------------------------------- update_dimlocationgrouptemp update Complete----------------------");

	}

	public void update_dimproducttemp() throws AnalysisException {

		Dataset<Row> finalRegionHierarchy = commonupdate();

		Dataset<Row> dim_product_temp2data = sparkSession.sql("Select * from dim_product_temp2");

		Dataset<Row> dimproducttemp2data123 = dim_product_temp2data
				.join(finalRegionHierarchy,
						finalRegionHierarchy.col("ID").equalTo(dim_product_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		Dataset<Row> dimproducttemp2 = dimproducttemp2data123.drop("ID").drop("LOCATION_ID").drop("VR_REGION_HIERARCHY")
				.drop("FINAL_REGION_HIERARCHY");

		dimproducttemp2.createOrReplaceTempView("dim_product_temp2");

		logger.warn("------------------------------- update_dimproducttemp update Complete----------------------");

	}

	public void update_dimtenantlocationtemp() throws AnalysisException {
		Dataset<Row> finalRegionHierarchy = commonupdate();

		Dataset<Row> dim_tenantlocation_temp2data = sparkSession.sql("Select * from dim_tenant_location_temp2");

		Dataset<Row> dimtenantlocation2data123 = dim_tenantlocation_temp2data.join(finalRegionHierarchy,
				finalRegionHierarchy.col("ID").equalTo(dim_tenantlocation_temp2data.col("TENANT_LOCATION_ID")), "left")
				.withColumn("REGION_HIERARCHY", finalRegionHierarchy.col("FINAL_REGION_HIERARCHY"));

		Dataset<Row> dimtenantlocation2 = dimtenantlocation2data123.drop("ID").drop("LOCATION_ID")
				.drop("VR_REGION_HIERARCHY").drop("FINAL_REGION_HIERARCHY");

		dimtenantlocation2.createOrReplaceTempView("dim_tenant_location_temp2");

		logger.warn(
				"------------------------------- update_dimtenantlocationtemp update Complete----------------------");

	}

	public void update_currencyforpartialsp() throws AnalysisException {

		logger.warn("-----------------------------update_currencyforpartialsp update Start----------------------");

		Dataset<Row> tenant_loc = spark.dbSession("tenant_location").withColumnRenamed("id", "tenant_loc_id")
				.select("tenant_loc_id", "local_currency_id").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> curr = spark.dbSession("currency").withColumnRenamed("id", "curr_id").select("curr_id", "code")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> dim_user_temp2 = sparkSession.sql("Select * from dim_user_temp2");
		Dataset<Row> dim_location_group_temp2 = sparkSession.sql("Select * from dim_location_group_temp2");
		Dataset<Row> dim_product_temp2 = sparkSession.sql("Select * from dim_product_temp2");
		Dataset<Row> dim_tenant_location_temp2 = sparkSession.sql("Select * from dim_tenant_location_temp2");

		dim_user_temp2 = dim_user_temp2
				.join(tenant_loc, tenant_loc.col("tenant_loc_id").equalTo(dim_user_temp2.col("tenant_location_id")),
						"left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_user_temp2 = dim_user_temp2.drop("tenant_loc_id").drop("local_currency_id").drop("curr_id").drop("code");

		dim_user_temp2.createOrReplaceTempView("dim_user_temp2");

		dim_location_group_temp2 = dim_location_group_temp2.join(tenant_loc,
				tenant_loc.col("tenant_loc_id").equalTo(dim_location_group_temp2.col("tenant_location_id")), "left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_location_group_temp2 = dim_location_group_temp2.drop("tenant_loc_id").drop("local_currency_id")
				.drop("curr_id").drop("code");

		dim_location_group_temp2.createOrReplaceTempView("dim_location_group_temp2");

		dim_product_temp2 = dim_product_temp2
				.join(tenant_loc, tenant_loc.col("tenant_loc_id").equalTo(dim_product_temp2.col("tenant_location_id")),
						"left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_product_temp2 = dim_product_temp2.drop("tenant_loc_id").drop("local_currency_id").drop("curr_id")
				.drop("code");

		dim_product_temp2.createOrReplaceTempView("dim_product_temp2");

		dim_tenant_location_temp2 = dim_tenant_location_temp2.join(tenant_loc,
				tenant_loc.col("tenant_loc_id").equalTo(dim_tenant_location_temp2.col("tenant_location_id")), "left")
				.join(curr, curr.col("curr_id").equalTo(tenant_loc.col("local_currency_id")), "left")
				.withColumn("location_currency", curr.col("code"));

		dim_tenant_location_temp2 = dim_tenant_location_temp2.drop("tenant_loc_id").drop("local_currency_id")
				.drop("curr_id").drop("code");

		dim_tenant_location_temp2.createOrReplaceTempView("dim_tenant_location_temp2");

		logger.warn("-----------------------------update_currencyforpartialsp update Complete----------------------");

	}

}