package com.spark.updateProcedure;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class Populate_location_group_closure {

	final static Logger logger = Logger.getLogger(Populate_location_group_closure.class);

	sqlConnection spark = new sqlConnection();
	SparkSession sparkSession = spark.getSpark();

	public void startPoint() {

		Dataset<Row> location_group = spark
				.dbSession("location_group")
				.select("id", "PARENT_GROUP_ID").persist(StorageLevel.MEMORY_ONLY_SER());

		int i = 0;

		Dataset<Row> location_group_closure = location_group
				.select(location_group.col("id").as("parent_id"), location_group.col("id").as("location_group_id"))
				.withColumn("distance", functions.lit(i)).persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> updated;
		long count = 0;

		do {
			i++;

			updated = location_group_closure.join(location_group,
					location_group.col("PARENT_GROUP_ID").equalTo(location_group_closure.col("location_group_id"))
							.and(location_group_closure.col("distance").equalTo(i - 1)))
					.select(location_group_closure.col("parent_id"), location_group.col("id"))
					.withColumn("distance", functions.lit(i));

			location_group_closure = location_group_closure.union(updated);

			count = updated.count();

		} while (count != 0.0);

		location_group_closure.createOrReplaceTempView("location_group_closure");
	}
}
