package com.spark.updateProcedure;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.RelationalGroupedDataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class UpdateStoredProcedureInformation {

	final static Logger logger = Logger.getLogger(UpdateStoredProcedureInformation.class);

	// SparkSession sparkSession =
	// SparkSession.builder().appName("in_gauge_2018JavaExample").master("local[*]")
	// .getOrCreate();
	//
	// sqlConnection spark = new sqlConnection();
	sqlConnection spark;
	SparkSession sparkSession;

	public UpdateStoredProcedureInformation(sqlConnection conn) {
		spark = conn;
		sparkSession = spark.getSpark();
	}

	public String getindustryTypeValue() throws AnalysisException {

		Dataset<Row> preference = spark.dbSession("preference").select("value", "name", "industry_ID")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		preference.createOrReplaceTempView("preference");

		List<Row> industryTypeValue = sparkSession
				.sql("SELECT value FROM preference WHERE name = 'system.industry.type' and industry_ID = 2")
				.collectAsList();
		String data;
		if (industryTypeValue.size() > 0) {
			data = industryTypeValue.get(0).toString();
		} else {
			data = null;
		}
		return data;
	}

	public String getdateTime() throws AnalysisException {

		Dataset<Row> stored_procedure_info = spark.dbSession("stored_procedure_info").select("datetime", "field_name")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		stored_procedure_info.createOrReplaceTempView("stored_procedure_info");

		List<Row> datetime = sparkSession
				.sql("SELECT datetime FROM stored_procedure_info WHERE field_name = 'REPORT_TABLES'").collectAsList();

		String data;
		if (datetime.size() > 0) {
			data = datetime.get(0).toString();
		} else {
			data = null;
		}
		return data;
	}

	public void startPoint() throws AnalysisException {
		
		Dataset<Row> location_metric = spark.dbSession("location_metric")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		location_metric.createOrReplaceTempView("location_metric");
		
		TransformReportDimInsertionTemp proc1 = new TransformReportDimInsertionTemp();
		String industryTypeValue = getindustryTypeValue();
		String fullProcesRunText = "8";
		String tableNameToProcess = null;
		String recordChangeOptions = null;
		String pmStartDate = null;
		String pmEndDate = null;
		LocalDateTime locationMetricDate = null;
		LocalDateTime agentMetricDate = null;

		List<String> recordList = new ArrayList<String>();

		Dataset<Row> PM_START_DATE;
		Dataset<Row> product_metric = spark.dbSession("product_metric")
//				.withColumnRenamed("id", "id")
//				.withColumnRenamed("user_id", "USER_ID").withColumnRenamed("tenant_location_id", "TENANT_LOCATION_ID")
//				.withColumnRenamed("location_group_id", "LOCATION_GROUP_ID")
//				.withColumnRenamed("product_id", "PRODUCT_ID").withColumnRenamed("industry_id", "INDUSTRY_ID")
//				.withColumnRenamed("metricDate", "metricDate").withColumnRenamed("businessDate", "businessDate")
//				.withColumnRenamed("arrivalDate", "arrivalDate").withColumnRenamed("departureDate", "departureDate")
//				.withColumnRenamed("isRecordChanged", "isRecordChanged")
//				.select("id", "auditstatus", "USER_ID", "TENANT_LOCATION_ID", "LOCATION_GROUP_ID", "PRODUCT_ID",
//						"INDUSTRY_ID", "metricDate", "businessDate", "arrivalDate", "departureDate", "isRecordChanged")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		
		product_metric.createOrReplaceTempView("product_metric");
		
		Dataset<Row> stored_procedure_info = spark.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		if (industryTypeValue != null && industryTypeValue.contains("CarRental"))
			fullProcesRunText = "6";

		String dateTime = getdateTime();

		dateTime = dateTime.substring(1, 20);

		DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime ld = LocalDateTime.parse(dateTime, dtf1);
		int day = ld.getDayOfMonth();

		LocalDateTime currDate = LocalDateTime.now();
		String cDate = currDate.format(dtf1);
		currDate = LocalDateTime.parse(cDate, dtf1);

		int curDate = currDate.getDayOfMonth();
		int weekday = 0;
		DayOfWeek dayofweek = currDate.getDayOfWeek();

		switch (dayofweek) {

		case MONDAY:
			weekday = 0;
			break;
		case TUESDAY:
			weekday = 1;
			break;
		case WEDNESDAY:
			weekday = 2;
			break;
		case THURSDAY:
			weekday = 3;
			break;
		case FRIDAY:
			weekday = 4;
			break;
		case SATURDAY:
			weekday = 5;
			break;
		case SUNDAY:
			weekday = 6;
			break;
		}

		// Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		// Calendar cal = Calendar.getInstance();
		// cal.setTimeInMillis(timestamp.getTime());

//		if (dateTime != null || (day != curDate && weekday == Integer.parseInt(fullProcesRunText.trim()))) {
		if (true) {

			recordChangeOptions = "";
			// LocalDate locationMetric = currDate.minusDays(10000);
			// cal.add(Calendar.DAY_OF_MONTH, -10000);

			locationMetricDate = LocalDateTime.from(currDate).minusDays(10000);
			agentMetricDate = LocalDateTime.from(currDate).minusDays(10000);

			String agentdate = agentMetricDate.format(dtf1);
			String locationdate = locationMetricDate.format(dtf1);

			agentMetricDate = LocalDateTime.parse(agentdate, dtf1);
			locationMetricDate = LocalDateTime.parse(locationdate, dtf1);

			stored_procedure_info = stored_procedure_info.withColumn("field_name",
					functions
							.when(stored_procedure_info.col("field_name").equalTo("IS_FULL_PROCESS"),
									functions.lit("IS_FULL_PROCESS"))
							.otherwise(stored_procedure_info.col("field_name")));
			stored_procedure_info = stored_procedure_info.withColumn("field_value", functions
					.when(stored_procedure_info.col("field_name").equalTo("IS_FULL_PROCESS"), functions.lit("1"))
					.otherwise(stored_procedure_info.col("field_value")));
			stored_procedure_info = stored_procedure_info.withColumn("datetime",
					functions
							.when(stored_procedure_info.col("field_name").equalTo("IS_FULL_PROCESS"),
									functions.lit(currDate.toString()))
							.otherwise(stored_procedure_info.col("datetime")));
			// stored_procedure_info.show();

			spark.sparkSqlWrite(stored_procedure_info, "stored_procedure_info");
			// write(stored_procedure_info, "stored_procedure_info");

			proc1.tenant_locations();

			tableNameToProcess = "product_metric";

			PM_START_DATE = product_metric;

		} else {

			stored_procedure_info = stored_procedure_info.withColumn("field_name",
					functions
							.when(stored_procedure_info.col("field_name").equalTo("IS_FULL_PROCESS"),
									functions.lit("IS_FULL_PROCESS"))
							.otherwise(stored_procedure_info.col("field_name")));
			stored_procedure_info = stored_procedure_info.withColumn("field_value", functions
					.when(stored_procedure_info.col("field_name").equalTo("IS_FULL_PROCESS"), functions.lit("0"))
					.otherwise(stored_procedure_info.col("field_value")));
			stored_procedure_info = stored_procedure_info.withColumn("datetime",
					functions
							.when(stored_procedure_info.col("field_name").equalTo("IS_FULL_PROCESS"),
									functions.lit(currDate.toString()))
							.otherwise(stored_procedure_info.col("datetime")));

			spark.sparkSqlWrite(stored_procedure_info, "stored_procedure_info");

			proc1.tenant_locations();

			recordChangeOptions = "1";
			String[] elements = recordChangeOptions.split(",");
			recordList = Arrays.asList(elements);
			// LocalDate locationMetric = currDate.minusDays(15000);
			// cal.add(Calendar.DAY_OF_MONTH, -15000);

			locationMetricDate = LocalDateTime.from(currDate).minusDays(10000);
			agentMetricDate = LocalDateTime.from(currDate).minusDays(10000);

			String agentdate = agentMetricDate.format(dtf1);
			String locationdate = locationMetricDate.format(dtf1);

			agentMetricDate = LocalDateTime.parse(agentdate, dtf1);
			locationMetricDate = LocalDateTime.parse(locationdate, dtf1);

			pmStartDate = "2014-01-01 00:00:00";
			pmEndDate = currDate.toString();

			Dataset<Row> tenant_location = spark.dbSession("tenant_location").select("ID", "hotelMetricsDataType")
					.persist(StorageLevel.MEMORY_ONLY_SER());
			
			
			Dataset<Row> result = product_metric
					.join(tenant_location, tenant_location.col("id").equalTo(product_metric.col("TENANT_LOCATION_ID")))
					.join(location_metric,
							location_metric.col("TENANT_LOCATION_ID").equalTo(tenant_location.col("ID")).and(functions
									.when(tenant_location.col("hotelMetricsDataType").equalTo(functions.lit(0)),
											tenant_location.col("metricDate").equalTo(product_metric.col("arrivalDate"))
													.and(product_metric.col("arrivalDate").between(pmStartDate,
															pmEndDate)))
									.when(tenant_location.col("hotelMetricsDataType").equalTo(functions.lit(1)),
											tenant_location.col("metricDate")
													.equalTo(product_metric.col("businessDate")).and(product_metric
															.col("businessDate").between(pmStartDate, pmEndDate)))
									.when(tenant_location.col("hotelMetricsDataType").equalTo(functions.lit(2)),
											tenant_location.col("metricDate")
													.equalTo(product_metric.col("departureDate")).and(product_metric
															.col("departureDate").between(pmStartDate, pmEndDate)))))
					.select(product_metric.col("id"), product_metric.col("user_id"),
							product_metric.col("tenant_location_id"), product_metric.col("location_group_id"),
							product_metric.col("product_id"), product_metric.col("industry_id"),
							product_metric.col("metricDate"), product_metric.col("businessDate"),
							product_metric.col("arrivalDate"), product_metric.col("departureDate"),
							product_metric.col("isRecordChanged"))
					.groupBy(product_metric.col("id"))
					.agg(functions.first("user_id").as("USER_ID"),
							functions.first("tenant_location_id").as("TENANT_LOCATION_ID"),
							functions.first("location_group_id").as("LOCATION_GROUP_ID"),
							functions.first("product_id").as("PRODUCT_ID"),
							functions.first("industry_id").as("INDUSTRY_ID"),
							functions.first("metricDate").as("metricDate"),
							functions.first("businessDate").as("businessDate"),
							functions.first("arrivalDate").as("arrivalDate"),
							functions.first("departureDate").as("departureDate"),
							functions.first("isRecordChanged").as("isRecordChanged"))
					.persist(StorageLevel.MEMORY_ONLY_SER());

			if (!recordChangeOptions.equals("")) {
				result = result.filter(product_metric.col("isRecordChanged").equalTo(recordChangeOptions)
						.and(product_metric.col("auditstatus").equalTo(functions.lit(4)))
						.and(product_metric.col("auditstatus").equalTo(functions.lit(5))));
			}

			PM_START_DATE = result;

			tableNameToProcess = "sp_product_metric_to_process";

		}

		Populate_region_closure closure = new Populate_region_closure();
		closure.startPoint();

		Populate_location_group_closure location_closure = new Populate_location_group_closure();
		location_closure.startPoint();

		List START_DATE = null;

		if (tableNameToProcess.equalsIgnoreCase("product_metric")) {
			START_DATE = PM_START_DATE
					.filter(PM_START_DATE.col("auditstatus")
							.notEqual(functions.lit(2)).and(
									PM_START_DATE.col("auditstatus").notEqual(functions.lit(4)))
							.and(PM_START_DATE.col("auditstatus").notEqual(functions.lit(5))))
					.select(functions
							.when(functions.min(PM_START_DATE.col("arrivalDate")).isNull(), currDate.toString())
							.otherwise(
									functions.min(PM_START_DATE.col("arrivalDate")))
							.as("min1"),
							functions
									.when(functions.min(PM_START_DATE.col("departureDate")).isNull(),
											currDate.toString())
									.otherwise(
											functions
													.min(PM_START_DATE
															.col("departureDate")))
									.as("min2"),
							functions
									.when(functions.min(PM_START_DATE.col("businessDate")).isNull(),
											currDate.toString())
									.otherwise(functions.min(PM_START_DATE.col("businessDate"))).as("min3"))
					.collectAsList();

		} else if (tableNameToProcess.equalsIgnoreCase("sp_product_metric_to_process")) {
			START_DATE = PM_START_DATE
					.filter(PM_START_DATE.col("isRecordChanged").isin(recordList)).select(
							functions
									.when(functions.min(PM_START_DATE.col("arrivalDate")).isNull(),
											currDate.toString())
									.otherwise(functions.min(PM_START_DATE.col("arrivalDate"))).as(
											"min1"),
							functions
									.when(functions.min(PM_START_DATE.col("departureDate")).isNull(),
											currDate.toString())
									.otherwise(
											functions
													.min(PM_START_DATE
															.col("departureDate")))
									.as("min2"),
							functions
									.when(functions.min(PM_START_DATE.col("businessDate")).isNull(),
											currDate.toString())
									.otherwise(functions.min(PM_START_DATE.col("businessDate"))).as("min3"))
					.collectAsList();
		}

		String[] data = START_DATE.get(0).toString().split(",");

		String mindate1 = data[0].substring(1, 11).toString() + " 00:00:00";
		String mindate2 = data[1].toString() + " 00:00:00";
		String mindate3 = data[2].substring(0, 10).toString() + " 00:00:00";

		LocalDateTime min1Date = LocalDateTime.parse(mindate1, dtf1);
		LocalDateTime min2Date = LocalDateTime.parse(mindate2, dtf1);
		LocalDateTime min3Date = LocalDateTime.parse(mindate3, dtf1);

		LocalDateTime earliest = least(least(min1Date, min2Date), min3Date);
		String pmstartdt = earliest.minusDays(40).toString();

		stored_procedure_info = stored_procedure_info.withColumn("field_name", functions
				.when(stored_procedure_info.col("field_name").equalTo("PM_START_DATE"), functions.lit("PM_START_DATE"))
				.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value", functions
				.when(stored_procedure_info.col("field_name").equalTo("PM_START_DATE"), functions.lit("PM_START_DATE"))
				.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime", functions
				.when(stored_procedure_info.col("field_name").equalTo("PM_START_DATE"), functions.lit(pmstartdt))
				.otherwise(stored_procedure_info.col("datetime")));

		if (tableNameToProcess.equalsIgnoreCase("product_metric")) {
			START_DATE = PM_START_DATE
					.filter(PM_START_DATE.col("auditstatus")
							.notEqual(functions.lit(2)).and(
									PM_START_DATE.col("auditstatus").notEqual(functions.lit(4)))
							.and(PM_START_DATE.col("auditstatus").notEqual(functions.lit(5))))
					.select(functions
							.when(functions.max(PM_START_DATE.col("arrivalDate")).isNull(), currDate.toString())
							.otherwise(
									functions.min(PM_START_DATE.col("arrivalDate")))
							.as("min1"),
							functions
									.when(functions.max(PM_START_DATE.col("departureDate")).isNull(),
											currDate.toString())
									.otherwise(
											functions
													.min(PM_START_DATE
															.col("departureDate")))
									.as("min2"),
							functions
									.when(functions.max(PM_START_DATE.col("businessDate")).isNull(),
											currDate.toString())
									.otherwise(functions.min(PM_START_DATE.col("businessDate"))).as("min3"))
					.collectAsList();

		} else if (tableNameToProcess.equalsIgnoreCase("sp_product_metric_to_process")) {
			START_DATE = PM_START_DATE
					.filter(PM_START_DATE.col("isRecordChanged").isin(recordList)).select(
							functions
									.when(functions.max(PM_START_DATE.col("arrivalDate")).isNull(),
											currDate.toString())
									.otherwise(functions.min(PM_START_DATE.col("arrivalDate"))).as(
											"min1"),
							functions
									.when(functions.max(PM_START_DATE.col("departureDate")).isNull(),
											currDate.toString())
									.otherwise(
											functions
													.min(PM_START_DATE
															.col("departureDate")))
									.as("min2"),
							functions
									.when(functions.max(PM_START_DATE.col("businessDate")).isNull(),
											currDate.toString())
									.otherwise(functions.min(PM_START_DATE.col("businessDate"))).as("min3"))
					.collectAsList();
		}

		data = START_DATE.get(0).toString().split(",");

		String maxdate1 = data[0].substring(1, 11).toString() + " 00:00:00";
		String maxdate2 = data[1].toString() + " 00:00:00";
		String maxdate3 = data[2].substring(0, 10).toString() + " 00:00:00";

		LocalDateTime max1Date = LocalDateTime.parse(maxdate1, dtf1);
		LocalDateTime max2Date = LocalDateTime.parse(maxdate2, dtf1);
		LocalDateTime max3Date = LocalDateTime.parse(maxdate3, dtf1);

		earliest = greatest(greatest(max1Date, max2Date), max3Date);

		String pmenddt = earliest.plusDays(40).toString();

		stored_procedure_info = stored_procedure_info.withColumn("field_name", functions
				.when(stored_procedure_info.col("field_name").equalTo("PM_END_DATE"), functions.lit("PM_END_DATE"))
				.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value", functions
				.when(stored_procedure_info.col("field_name").equalTo("PM_END_DATE"), functions.lit("PM_END_DATE"))
				.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime",
				functions.when(stored_procedure_info.col("field_name").equalTo("PM_END_DATE"), functions.lit(pmenddt))
						.otherwise(stored_procedure_info.col("datetime")));

		String metricstartdt = earliest.minusDays(40).toString();

		stored_procedure_info = stored_procedure_info
				.withColumn("field_name",
						functions
								.when(stored_procedure_info.col("field_name").equalTo("METRIC_START_DATE"),
										functions.lit("METRIC_START_DATE"))
								.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("METRIC_START_DATE"),
								functions.lit("METRIC_START_DATE"))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime",
				functions.when(stored_procedure_info.col("field_name").equalTo("METRIC_START_DATE"),
						functions.lit(metricstartdt)).otherwise(stored_procedure_info.col("datetime")));

		String metricenddt = earliest.plusDays(40).toString();

		stored_procedure_info = stored_procedure_info
				.withColumn("field_name",
						functions
								.when(stored_procedure_info.col("field_name").equalTo("METRIC_END_DATE"),
										functions.lit("METRIC_END_DATE"))
								.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info
				.withColumn("field_value",
						functions
								.when(stored_procedure_info.col("field_name").equalTo("METRIC_END_DATE"),
										functions.lit("METRIC_END_DATE"))
								.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime", functions
				.when(stored_procedure_info.col("field_name").equalTo("METRIC_END_DATE"), functions.lit(metricenddt))
				.otherwise(stored_procedure_info.col("datetime")));

		if (tableNameToProcess.equalsIgnoreCase("product_metric")) {
			Dataset<Row> tmp = PM_START_DATE
					.filter(PM_START_DATE.col("auditstatus").notEqual(2)
							.and(PM_START_DATE.col("auditstatus").notEqual(4))
							.and(PM_START_DATE.col("auditstatus").notEqual(5)))
					.select("TENANT_LOCATION_ID").distinct().as("TENANT_LOCATION_ID");

			Dataset<Row> tmp1 = PM_START_DATE
					.join(tmp, tmp.col("TENANT_LOCATION_ID").equalTo(PM_START_DATE.col("TENANT_LOCATION_ID")))
					.select(tmp.col("TENANT_LOCATION_ID")).distinct().as("TENANT_LOCATION_ID");
			START_DATE = tmp1.select(functions.concat_ws(",", functions.collect_list(tmp1.col("TENANT_LOCATION_ID"))))
					.collectAsList();

		} else if (tableNameToProcess.equalsIgnoreCase("sp_product_metric_to_process")) {
			Dataset<Row> tmp = PM_START_DATE.filter(PM_START_DATE.col("isRecordChanged").isin(recordList))
					.select("TENANT_LOCATION_ID").distinct().as("TENANT_LOCATION_ID");

			Dataset<Row> tmp1 = PM_START_DATE
					.join(tmp, tmp.col("TENANT_LOCATION_ID").equalTo(PM_START_DATE.col("TENANT_LOCATION_ID")))
					.select(tmp.col("TENANT_LOCATION_ID")).distinct().as("TENANT_LOCATION_ID");

			START_DATE = tmp1.select(functions.concat_ws(",", functions.collect_list(tmp1.col("TENANT_LOCATION_ID"))))
					.collectAsList();
		}

		String TENANT_LOCATION_ID = START_DATE.get(0).toString().substring(1,
				START_DATE.get(0).toString().length() - 1);

		stored_procedure_info = stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("TENANT_LOCATION_IDs"),
								functions.lit("TENANT_LOCATION_IDs"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info
				.withColumn("field_value",
						functions
								.when(stored_procedure_info.col("field_name").equalTo("TENANT_LOCATION_IDs"),
										functions.lit(TENANT_LOCATION_ID))
								.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info
				.withColumn("datetime",
						functions
								.when(stored_procedure_info.col("field_name").equalTo("TENANT_LOCATION_IDs"),
										functions.lit(currDate.toString()))
								.otherwise(stored_procedure_info.col("datetime")));

		RelationalGroupedDataset sp_pm_ids_to_process = null;

		if (tableNameToProcess.equalsIgnoreCase("product_metric")) {
			sp_pm_ids_to_process = PM_START_DATE.select(PM_START_DATE.col("id")).groupBy(PM_START_DATE.col("id"));

		} else if (tableNameToProcess.equalsIgnoreCase("sp_product_metric_to_process")) {
			sp_pm_ids_to_process = PM_START_DATE.select(PM_START_DATE.col("id")).groupBy(PM_START_DATE.col("id"));
		}

		// write();

		String locationMetric = locationMetricDate.toString();

		location_metric = location_metric.withColumn("locationMetricDate", functions.lit(locationMetric));

		START_DATE = location_metric
				.filter(location_metric.col("CREATED_ON")
						.gt(location_metric.col("locationMetricDate").cast("timestamp"))
						.or(location_metric.col("UPDATED_ON")
								.gt(location_metric.col("locationMetricDate").cast("timestamp"))))
				.select(functions.min(location_metric.col("metricDate")).as("min1"),
						functions.max(location_metric.col("metricDate")).as("max1"))
				.collectAsList();

		data = START_DATE.get(0).toString().split(",");

		LocalDateTime minDate = LocalDateTime.parse(data[0].substring(1, 11) + " 00:00:00", dtf1);
		LocalDateTime maxDate = LocalDateTime.parse(data[1].substring(0, 10) + " 00:00:00", dtf1);

		String locmatstartdt = minDate.minusDays(10).toString();

		String locmatenddt = maxDate.plusDays(10).toString();

		stored_procedure_info = stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_START_DATE"),
								functions.lit("LOCATION_METRIC_START_DATE"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_START_DATE"),
								functions.lit("LOCATION_METRIC_START_DATE"))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime",
				functions.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_START_DATE"),
						functions.lit(locmatstartdt)).otherwise(stored_procedure_info.col("datetime")));

		stored_procedure_info = stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_END_DATE"),
								functions.lit("LOCATION_METRIC_END_DATE"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_END_DATE"),
								functions.lit("LOCATION_METRIC_END_DATE"))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime",
				functions.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_END_DATE"),
						functions.lit(locmatenddt)).otherwise(stored_procedure_info.col("datetime")));

		Dataset<Row> location_metrictmp = location_metric
				.filter(location_metric.col("CREATED_ON")
						.gt(location_metric.col("locationMetricDate").cast("timestamp"))
						.or(location_metric.col("UPDATED_ON")
								.gt(location_metric.col("locationMetricDate").cast("timestamp"))))
				.select("TENANT_LOCATION_ID").distinct().as("TENANT_LOCATION_ID");

		Dataset<Row> tmplocmat = location_metrictmp
				.join(location_metrictmp,
						location_metrictmp.col("TENANT_LOCATION_ID").equalTo(location_metric.col("TENANT_LOCATION_ID")))
				.select(location_metrictmp.col("TENANT_LOCATION_ID")).distinct().as("TENANT_LOCATION_ID");

		START_DATE = tmplocmat
				.select(functions.concat_ws(",", functions.collect_list(tmplocmat.col("TENANT_LOCATION_ID"))))
				.collectAsList();

		String LOCATION_METRIC_TENANT_LOCATION_IDs = START_DATE.get(0).toString().substring(1,
				START_DATE.get(0).toString().length() - 1);

		stored_procedure_info = stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"),
								functions.lit("LOCATION_METRIC_TENANT_LOCATION_IDs"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"),
								functions.lit(LOCATION_METRIC_TENANT_LOCATION_IDs))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info
				.withColumn("datetime",
						functions
								.when(stored_procedure_info.col("field_name").equalTo(
										"LOCATION_METRIC_TENANT_LOCATION_IDs"), functions.lit(currDate.toString()))
								.otherwise(stored_procedure_info.col("datetime")));

		Dataset<Row> agent_metric = spark.dbSession("agent_metric").persist(StorageLevel.MEMORY_ONLY_SER());

		String agentMetric = agentMetricDate.toString();

		agent_metric = agent_metric.withColumn("agentMetricDate", functions.lit(agentMetric));

		START_DATE = agent_metric
				.filter(agent_metric.col("auditstatus").notEqual(functions.lit(2))
						.and(agent_metric.col("auditstatus").notEqual(functions.lit(4)))
						.and(agent_metric.col("auditstatus").notEqual(functions.lit(5)))
						.and(agent_metric.col("CREATED_ON").cast("timestamp")
								.gt(agent_metric.col("agentMetricDate").cast("timestamp")).or(
										agent_metric.col("UPDATED_ON").cast("timestamp")
												.gt(agent_metric.col("agentMetricDate").cast("timestamp")))))
				.select(functions.min(agent_metric.col("metricDate")).as("min1"),
						functions.max(agent_metric.col("metricDate")).as("max1"))
				.collectAsList();

		data = START_DATE.get(0).toString().split(",");

		LocalDateTime minagentDate = LocalDateTime.parse(data[0].substring(1, 11) + " 00:00:00", dtf1);
		LocalDateTime maxagentDate = LocalDateTime.parse(data[1].substring(0, 10) + " 00:00:00", dtf1);

		String agentmatstartdt = minagentDate.minusDays(10).toString();

		String agentmatenddt = maxagentDate.plusDays(10).toString();

		stored_procedure_info = stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_START_DATE"),
								functions.lit("AGENT_METRIC_START_DATE"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_START_DATE"),
								functions.lit("AGENT_METRIC_START_DATE"))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info
				.withColumn("datetime",
						functions
								.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_START_DATE"),
										functions.lit(agentmatstartdt))
								.otherwise(stored_procedure_info.col("datetime")));

		stored_procedure_info = stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_END_DATE"),
								functions.lit("AGENT_METRIC_END_DATE"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_END_DATE"),
								functions.lit("AGENT_METRIC_END_DATE"))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info.withColumn("datetime",
				functions.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_END_DATE"),
						functions.lit(agentmatenddt)).otherwise(stored_procedure_info.col("datetime")));

		Dataset<Row> agent_metrictmp = agent_metric
				.filter(agent_metric.col("auditstatus").notEqual(functions.lit(2))
						.and(agent_metric.col("auditstatus").notEqual(functions.lit(4)))
						.and(agent_metric.col("auditstatus").notEqual(functions.lit(5)))
						.and(agent_metric.col("CREATED_ON").gt(agent_metric.col("agentMetricDate").cast("timestamp"))
								.or(agent_metric.col("UPDATED_ON")
										.gt(agent_metric.col("agentMetricDate").cast("timestamp")))))
				.select("TENANT_LOCATION_ID").distinct().as("TENANT_LOCATION_ID");

		Dataset<Row> tmpagentmat = agent_metrictmp
				.join(agent_metrictmp,
						agent_metrictmp.col("TENANT_LOCATION_ID").equalTo(agent_metric.col("TENANT_LOCATION_ID")))
				.select(agent_metrictmp.col("TENANT_LOCATION_ID")).distinct().as("TENANT_LOCATION_ID");

		START_DATE = tmpagentmat
				.select(functions.concat_ws(",", functions.collect_list(tmpagentmat.col("TENANT_LOCATION_ID"))))
				.collectAsList();

		String AGENT_METRIC_TENANT_LOCATION_IDs = START_DATE.get(0).toString().substring(1,
				START_DATE.get(0).toString().length() - 1);

		stored_procedure_info.withColumn("field_name",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_TENANT_LOCATION_IDs"),
								functions.lit("AGENT_METRIC_TENANT_LOCATION_IDs"))
						.otherwise(stored_procedure_info.col("field_name")));
		stored_procedure_info = stored_procedure_info.withColumn("field_value",
				functions
						.when(stored_procedure_info.col("field_name").equalTo("AGENT_METRIC_TENANT_LOCATION_IDs"),
								functions.lit(AGENT_METRIC_TENANT_LOCATION_IDs))
						.otherwise(stored_procedure_info.col("field_value")));
		stored_procedure_info = stored_procedure_info
				.withColumn("datetime",
						functions
								.when(stored_procedure_info.col("field_name").equalTo(
										"AGENT_METRIC_TENANT_LOCATION_IDs"), functions.lit(currDate.toString()))
								.otherwise(stored_procedure_info.col("datetime")));
		spark.sparkSqlWrite(stored_procedure_info, "stored_procedure_info");
		// stored_procedure_info.show();

	}

	public static LocalDateTime least(LocalDateTime a, LocalDateTime b) {
		return a == null ? b : (b == null ? a : (a.isBefore(b) ? a : b));
	}

	public static LocalDateTime greatest(LocalDateTime a, LocalDateTime b) {
		return a == null ? b : (b == null ? a : (a.isBefore(b) ? b : a));
	}

	public void WriteParqute() {
		logger.warn("-------------------------WriteParqute STARTED-----------------");
		sparkSession.read().table("dim_tenant_location_temp").write().mode(SaveMode.Overwrite)
				.parquet("dim_tenant_location_temp");
		sparkSession.read().table("dim_location_group_temp").write().mode(SaveMode.Overwrite)
				.parquet("dim_location_group_temp");
		sparkSession.read().table("dim_product_temp").write().mode(SaveMode.Overwrite).parquet("dim_product_temp");
		sparkSession.read().table("dim_user_temp").write().mode(SaveMode.Overwrite).parquet("dim_user_temp");
		logger.warn("-------------------------WriteParqute ENDED-----------------");
	}
}
