package com.spark;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.StructType;

import com.spark.Upsell_Tracker_Report_Load.AllInOne;

public class KafkaReceiver implements Serializable {

	sqlConnection conn;
	SparkSession spark;
	StructureFormat format = new StructureFormat();

	KafkaReceiver(sqlConnection conn) {
		this.conn = conn;
		spark = conn.getSpark();
	}

	public void start() {
		getData();
	}

	public void getData() {
		try {

			StructType locationMetricStruture = format.locationMetricStruture();
			StructType productMetricStruture = format.productMetricStruture();
			StructType agentMetricStruture = format.agentMetricStruture();

			Dataset<Row> metric = conn.getSpark().readStream().format("kafka")
					.option("kafka.bootstrap.servers", "localhost:9092")
					//test-location_metric,test-agent_metric,
					.option("subscribe", "test-product_metric")
					.option("kafkaConsumer.pollTimeoutMs", 70000).option("startingOffsets", "latest")
					.option("auto.offset.reset", "latest").load().selectExpr("CAST(value AS STRING) as json",
							"CAST(timestamp AS timestamp)", "CAST(topic AS STRING) AS topic");

//			Dataset<Row> am = metric.filter(functions.col("topic").equalTo("test-agent_metric"));
			Dataset<Row> pm = metric.filter(functions.col("topic").equalTo("test-product_metric"));
//			Dataset<Row> lm = metric.filter(functions.col("topic").equalTo("test-location_metric"));

//			StreamingQuery query1 = am
//					.select(functions.col("timestamp"),
//							functions.from_json(functions.col("json"), agentMetricStruture).as("data"))
//					.select("timestamp", "data.*").writeStream().outputMode("update")
//					.foreachBatch(new VoidFunction2<Dataset<Row>, Long>() {
//						@Override
//						public void call(Dataset<Row> v1, Long v2) throws Exception {
//							Dataset<Row> am = v1.groupBy(functions.col("tenant_location_id")).agg(
//									functions.min(functions.col("metricDate")).as("minDate"),
//									(functions.max(functions.col("metricDate")).as("maxDate")));
//							agentMetric(am);
//						}
//					}).trigger(Trigger.ProcessingTime("30 seconds"))
//					.option("checkpointLocation", "C:/kafka_2.12-2.3.0/checkpoint-agent-metric").start();

			StreamingQuery query2 = pm
					.select(functions.col("timestamp"),
							functions.from_json(functions.col("json"), productMetricStruture).as("data"))
					.select("timestamp", "data.*").writeStream().outputMode("update")
					.foreachBatch(new VoidFunction2<Dataset<Row>, Long>() {
						@Override
						public void call(Dataset<Row> v1, Long v2) throws Exception {
							Dataset<Row> pm = v1.filter("auditstatus not in (2,4,5)")
									.groupBy(functions.col("tenant_location_id")).agg(
											functions
													.least(functions.min(functions.col("businessDate")),
															functions.min(functions.col("arrivalDate")),
															functions.min(functions.col("departureDate")))
													.as("minDate"),
											functions
													.greatest(functions.max(functions.col("businessDate")),
															functions.max(functions.col("arrivalDate")),
															functions.max(functions.col("departureDate")))
													.as("maxDate"));

							productMetric(pm);
						}
					}).trigger(Trigger.ProcessingTime("30 seconds"))
					.option("checkpointLocation", "C:/kafka_2.12-2.3.0/checkpoint-product-metric").start();
//			StreamingQuery query3 = lm
//					.select(functions.col("timestamp"),
//							functions.from_json(functions.col("json"), locationMetricStruture).as("data"))
//					.select("timestamp", "data.*").writeStream().outputMode("update")
//					.foreachBatch(new VoidFunction2<Dataset<Row>, Long>() {
//						@Override
//						public void call(Dataset<Row> v1, Long v2) throws Exception {
//							Dataset<Row> lm = v1.groupBy(functions.col("tenant_location_id")).agg(
//									functions.min(functions.col("metricDate")).as("minDate"),
//									(functions.max(functions.col("metricDate")).as("maxDate")));
//							locationMetric(lm);
//						}
//					}).trigger(Trigger.ProcessingTime("30 seconds"))
//					.option("checkpointLocation", "C:/kafka_2.12-2.3.0/checkpoint-location-metric").start();
//
//			query1.awaitTermination();
			query2.awaitTermination();
//			query3.awaitTermination();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void agentMetric(Dataset<Row> agentMetric) {
		agentMetric.show(false);
		agentMetric.foreach((ForeachFunction<Row>) row -> {
			Integer tenantLocationId = row.getInt(row.fieldIndex("tenant_location_id"));
			LocalDate minDate = LocalDate.ofEpochDay(Long.valueOf(row.getInt(row.fieldIndex("minDate"))));
			LocalDate maxDate = LocalDate.ofEpochDay(Long.valueOf(row.getInt(row.fieldIndex("maxDate"))));
			System.out.println("Tenant-location-id: " + tenantLocationId);
			System.out.println("minDate: " + minDate);
			System.out.println("maxDate: " + maxDate);
		});
	}

	private void locationMetric(Dataset<Row> locationMetric) {
		locationMetric.show(false);
		locationMetric.foreach((ForeachFunction<Row>) row -> {
			Integer tenantLocationId = row.getInt(row.fieldIndex("tenant_location_id"));
			LocalDate minDate = LocalDate.ofEpochDay(Long.valueOf(row.getInt(row.fieldIndex("minDate"))));
			LocalDate maxDate = LocalDate.ofEpochDay(Long.valueOf(row.getInt(row.fieldIndex("maxDate"))));
			System.out.println("Tenant-location-id: " + tenantLocationId);
			System.out.println("minDate: " + minDate);
			System.out.println("maxDate: " + maxDate);
		});
	}

	private void productMetric(Dataset<Row> productMetric) {
		productMetric.show(false);
		AllInOne all=new AllInOne();
		all.start(productMetric);

	}
}
