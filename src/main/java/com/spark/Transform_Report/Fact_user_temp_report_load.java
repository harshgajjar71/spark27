package com.spark.Transform_Report;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class Fact_user_temp_report_load {
	final static Logger logger = Logger.getLogger(Fact_user_temp_report_load.class);

	sqlConnection sql = new sqlConnection();
	SparkSession sparkSession = sql.getSpark();

	public void start(int ISFULLPROCESS) {
		logger.warn("------------------------Fact_user_temp_report_load started------------");
		Dataset<Row> stored_procedure_info = sql.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> fact_user = sql.dbSession("fact_user").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> agent_metric = sql.dbSession("agent_metric").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> dim_calendar = sql.dbSession("dim_calendar").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> dim_tenant_location_temp = sql.dbSession("dim_tenant_location_temp");

		Dataset<Row> dim_user_temp = sql.dbSession("dim_user_temp").persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> lmIDs = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value");
		List<Row> collectAsList = lmIDs.collectAsList();
		List<Row> lmDate = stored_procedure_info
				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
				.collectAsList();

		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");

		Dataset<Row> fact_user_temp = fact_user.filter(functions.not(fact_user.col("metricDate")
				.between(lmDate.get(0).toString().substring(1, 11), lmDate.get(1).toString().substring(1, 11))
				.and(collectAsList.isEmpty() ? functions.when(fact_user.col("ID").isNotNull(), true)
						: fact_user.col("TENANT_LOCATION_ID").isin(id))));
		Dataset<Row> joinResult = agent_metric
				.join(dim_user_temp, dim_user_temp.col("USER_ID").equalTo(agent_metric.col("USER_ID"))
						.and(agent_metric.col("TENANT_LOCATION_ID").equalTo(dim_user_temp.col("TENANT_LOCATION_ID")))
						.and(dim_user_temp.col("is_default_type").equalTo(1)))
				.join(dim_tenant_location_temp,
						dim_tenant_location_temp.col("TENANT_LOCATION_ID")
								.equalTo(agent_metric.col("TENANT_LOCATION_ID")))
				.join(dim_calendar, dim_calendar.col("FULL_DATE").equalTo(agent_metric.col("metricDate")))
				.where(functions.not(agent_metric.col("auditstatus").isin("4", "2", "5"))
						.and(agent_metric.col("metricDate")
								.between(lmDate.get(0).toString().substring(1, 11),
										lmDate.get(1).toString().substring(1, 11))
								.and(collectAsList.isEmpty() ? functions.when(fact_user.col("ID").isNotNull(), true)
										: agent_metric.col("TENANT_LOCATION_ID").isin(id)))

				);

		Dataset<Row> fact_user_temp_Result = joinResult
				.select(agent_metric.col("USER_ID"), dim_user_temp.col("USER_ID").as("DIM_USER_ID"),
						agent_metric.col("TENANT_LOCATION_ID"), dim_tenant_location_temp.col("REGION_ID"),
						dim_tenant_location_temp.col("TENANT_LOCATION_ID").as("DIM_TENANT_LOCATION_ID"),
						dim_user_temp.col("LOCATION_GROUP_ID"), agent_metric.col("metricDate"),
						functions.month(agent_metric.col("metricDate")).as("month"),
						functions.year(agent_metric.col("metricDate")).as("year"),
						dim_calendar.col("id").as("DIM_METRIC_DATE_ID"),
						functions.when(agent_metric.col("arrivals").isNull(), 0).otherwise(agent_metric.col("arrivals"))
								.as("arrivals"),
						functions.when(agent_metric.col("checkedInNights").isNull(), 0)
								.otherwise(agent_metric.col("checkedInNights")).as("checkedInNights"),
						functions.when(agent_metric.col("upsellMainRevenue").isNull(), 0)
								.otherwise(agent_metric.col("upsellMainRevenue")).as("upsellMainRevenue"),
						functions.when(agent_metric.col("otherRevenue").isNull(), 0)
								.otherwise(agent_metric.col("otherRevenue")).as("otherRevenue"),
						agent_metric.col("memo"), agent_metric.col("callTime"),
						functions.when(agent_metric.col("callHoldTime").isNull(), 0)
								.otherwise(agent_metric.col("callHoldTime")).as("callHoldTime"),
						functions.when(agent_metric.col("afterCallWorkTime").isNull(), 0)
								.otherwise(agent_metric.col("afterCallWorkTime")).as("afterCallWorkTime"),
						functions.when(agent_metric.col("transfersToIVR").isNull(), 0)
								.otherwise(agent_metric.col("transfersToIVR")).as("transfersToIVR"),
						functions.when(agent_metric.col("transfersToHGV").isNull(), 0)
								.otherwise(agent_metric.col("transfersToHGV")).as("transfersToHGV"),
						functions.when(agent_metric.col("transfersToHTS").isNull(), 0)
								.otherwise(agent_metric.col("transfersToHTS")).as("transfersToHTS"),

						functions.when(agent_metric.col("adrTarget").isNull(), 0)
								.otherwise(agent_metric.col("adrTarget")).as("adrTarget"),
						functions.when(agent_metric.col("cnvTarget").isNull(), 0)
								.otherwise(agent_metric.col("cnvTarget")).as("cnvTarget"),
						functions.when(agent_metric.col("revenueAll").isNull(), 0)
								.otherwise(agent_metric.col("revenueAll")).as("revenueAll"),
						agent_metric.col("surveyDate"),
						functions.when(agent_metric.col("vocScore").isNull(), 0).otherwise(agent_metric.col("vocScore"))
								.as("vocScore"),
						functions.when(agent_metric.col("topBox").isNull(), 0).otherwise(agent_metric.col("topBox"))
								.as("topBox"),
						functions.when(agent_metric.col("vocScoreDen").isNull(), 0)
								.otherwise(agent_metric.col("vocScoreDen")).as("vocScoreDen"),
						functions.when(agent_metric.col("sumVocScore").isNull(), 0)
								.otherwise(agent_metric.col("sumVocScore")).as("sumVocScore"));

		fact_user_temp = fact_user_temp.drop("id", "DIM_LOCATION_GROUP_ID");

		if (ISFULLPROCESS == 1) {
			fact_user_temp.createOrReplaceTempView("fact_user_temp");

		} else {
			Dataset<Row> finalSet = fact_user_temp.unionByName(fact_user_temp_Result);
			finalSet.createOrReplaceTempView("fact_user_temp");
		}

		logger.warn("------------------------Fact_user_temp_report_load started------------");
	}

}
