package com.spark.Transform_Report;

import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.sql.functions;
import com.spark.sqlConnection;

public class CreateReportTable {
	final static Logger logger = Logger.getLogger(CreateReportTable.class);

	sqlConnection conn = new sqlConnection();
	String database = "in_gauge_2018_0404_hotel";
	SparkSession sparkSession = conn.getSpark();

	Dataset<Row> storedInfo = conn.dbSession( "stored_procedure_info").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> agent_metric = conn.dbSession( "agent_metric").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> dim_user_temp = conn.dbSession( "dim_user_temp").persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> dim_tenant_location_temp = conn.dbSession( "dim_tenant_location_temp")
			.persist(StorageLevel.MEMORY_ONLY_SER());
	Dataset<Row> dim_calendar = conn.dbSession( "dim_calendar").persist(StorageLevel.MEMORY_ONLY_SER());

	public void write(Dataset<Row> row, String tableName) {
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "root");
		row.write().mode(SaveMode.Append).jdbc("jdbc:mysql://127.0.0.1:3306/" + "sparkk", tableName,
				connectionProperties);
	}

	public void transformReportProcess() throws AnalysisException {
		logger.warn("-------------------------------Transform Report started ----------------------");
		FactUserTempReportLoad();
		logger.warn("-------------------------------Transform Report ended ----------------------");
	}

	public void FactUserTempReportLoad() throws AnalysisException {
		logger.warn("-------------------------------Fact User Temp Report Load started ----------------------");

		Dataset<Row> date = storedInfo.filter("field_name like 'AGENT_METRIC%DATE'");
		List<Row> listIds = storedInfo.filter("field_name = 'AGENT_METRIC_TENANT_LOCATION_IDs'").select("field_value")
				.collectAsList();
		String ids = listIds.size() == 0 ? ""
				: listIds.get(0).toString().substring(1, listIds.get(0).toString().length() - 1);
		String id[] = ids.split(",");
		date.createOrReplaceTempView("stored_pro");
		List<Row> collectAsList = sparkSession.sql("select TO_DATE(stored_pro.datetime, 'YYYY-MM-DD') from stored_pro")
				.collectAsList();

		String query = "metricDate between '" + collectAsList.get(0).toString().substring(1, 11) + "' and '"
				+ collectAsList.get(1).toString().substring(1, 11) + "'";
		query = ids == "" ? "!(" + query + ")" : "!(" + query + " and (`TENANT_LOCATION_ID` in (" + ids + ")))";
		Dataset<Row> data = conn.dbSession( "fact_user").where(query).persist(StorageLevel.MEMORY_ONLY_SER());
	//	write(data, "fact_user_temp");

// "auditstatus not in(2,4,5) and metricDate between '"+collectAsList.get(0).toString().substring(1, 11)+"' and '"+collectAsList.get(1).toString().substring(1, 11)+"'"+" and `TENANT_LOCATION_ID` in ("+ids+")"
		Dataset<Row> agentMetricDataset = agent_metric
				.where(functions.not(agent_metric.col("auditstatus").isin(2, 4, 5))
						.and(agent_metric.col("metricDate").between(collectAsList.get(0).toString().substring(1, 11),
								collectAsList.get(1).toString().substring(1, 11)))
						.and(ids.isEmpty() ? functions.when(agent_metric.col("ID").isNotNull(), true)
								: agent_metric.col("TENANT_LOCATION_ID").isin(id))

				)
				.join(dim_user_temp, agent_metric.col("USER_ID").equalTo(dim_user_temp.col("USER_ID"))
						.and(agent_metric.col("TENANT_LOCATION_ID").equalTo(dim_user_temp.col("TENANT_LOCATION_ID")))
						.and(dim_user_temp.col("IS_DEFAULT_TYPE").equalTo("1")))
				.join(dim_tenant_location_temp,
						dim_tenant_location_temp.col("TENANT_LOCATION_ID")
								.equalTo(agent_metric.col("TENANT_LOCATION_ID")))
				.join(dim_calendar, dim_calendar.col("FULL_DATE").equalTo(agent_metric.col("metricDate")))
				.select(agent_metric.col("USER_ID"), dim_user_temp.col("id").as("DIM_USER_ID"),
						agent_metric.col("TENANT_LOCATION_ID"),
						dim_tenant_location_temp.col("REGION_ID").as("REGION_ID"),
						dim_tenant_location_temp.col("id").as("DIM_TENANT_LOCATION_ID"),
						dim_user_temp.col("LOCATION_GROUP_ID"), 
						agent_metric.col("metricDate"),
						functions.month(agent_metric.col("metricDate")).as("month"),
						functions.year(agent_metric.col("metricDate")).as("year"),
						dim_calendar.col("id").as("DIM_METRIC_DATE_ID"),
						functions.when(agent_metric.col("arrivals").isNull(), 0).otherwise(agent_metric.col("arrivals"))
								.as("arrivals"),
						functions.when(agent_metric.col("checkedInNights").isNull(), 0)
								.otherwise(agent_metric.col("checkedInNights")).as("checkedInNights"),
						functions.when(agent_metric.col("upsellMainRevenue").isNull(), 0)
								.otherwise(agent_metric.col("upsellMainRevenue")).as("upsellMainRevenue"),
						functions.when(agent_metric.col("otherRevenue").isNull(), 0)
								.otherwise(agent_metric.col("otherRevenue")).as("otherRevenue"),
						agent_metric.col("memo"), agent_metric.col("callTime"),
						functions.when(agent_metric.col("callHoldTime").isNull(), 0)
								.otherwise(agent_metric.col("callHoldTime")).as("callHoldTime"),
						functions.when(agent_metric.col("afterCallWorkTime").isNull(), 0)
								.otherwise(agent_metric.col("afterCallWorkTime")).as("afterCallWorkTime"),
						functions.when(agent_metric.col("transfersToIVR").isNull(), 0)
								.otherwise(agent_metric.col("transfersToIVR")).as("transfersToIVR"),
						functions.when(agent_metric.col("transfersToHGV").isNull(), 0)
								.otherwise(agent_metric.col("transfersToHGV")).as("transfersToHGV"),
						functions.when(agent_metric.col("transfersToHTS").isNull(), 0)
								.otherwise(agent_metric.col("transfersToHTS")).as("transfersToHTS"),
						functions.when(agent_metric.col("adrTarget").isNull(), 0)
								.otherwise(agent_metric.col("adrTarget")).as("adrTarget"),
						functions.when(agent_metric.col("cnvTarget").isNull(), 0)
								.otherwise(agent_metric.col("cnvTarget")).as("cnvTarget"),
						functions.when(agent_metric.col("revenueAll").isNull(), 0)
								.otherwise(agent_metric.col("revenueAll")).as("revenueAll"),
						agent_metric.col("surveyDate"),
						functions.when(agent_metric.col("vocScore").isNull(), 0).otherwise(agent_metric.col("vocScore"))
								.as("vocScore"),
						functions.when(agent_metric.col("topBox").isNull(), 0).otherwise(agent_metric.col("topBox"))
								.as("topBox"),
						functions.when(agent_metric.col("vocScoreDen").isNull(), 0)
								.otherwise(agent_metric.col("vocScoreDen")).as("vocScoreDen"),
						functions.when(agent_metric.col("sumVocScore").isNull(), 0)
								.otherwise(agent_metric.col("sumVocScore")).as("sumVocScore"));

		write(agentMetricDataset, "fact_user_temp");
		logger.warn("-------------------------------Fact User Temp Report Load completed ---------------------");
	}
}