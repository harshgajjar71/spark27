package com.spark.Transform_Report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class Fact_location_temp_report_load {
	final static Logger logger = Logger.getLogger(Fact_location_temp_report_load.class);

	public void fact_location_temp(int ISFULLPROCESS) {
		logger.warn("------------------------Fact_location_temp_report_load started------------"
				+ java.time.LocalDateTime.now());
		sqlConnection sql = new sqlConnection();
		SparkSession sparkSession = sql.getSpark();
		Dataset<Row> stored_procedure_info = sql.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> fact_location = sql.dbSession("fact_location").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> lm = sql.dbSession("location_metric").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dim_calendar = sql.dbSession("dim_calendar").persist(StorageLevel.MEMORY_ONLY_SER());
		fact_location.createOrReplaceTempView("fact_location");

		Dataset<Row> dim_tenant_location_temp = sql.dbSession("dim_tenant_location_temp")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		dim_tenant_location_temp.createOrReplaceTempView("dim_tenant_location_temp");
		Dataset<Row> lmIDs = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value");
		List<Row> collectAsList = lmIDs.collectAsList();
		List<Row> lmDate = stored_procedure_info
				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
				.collectAsList();

		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");

		Dataset<Row> fact_location_temp = fact_location.filter(functions.not(fact_location.col("metricDate")
				.between(lmDate.get(0).toString().substring(1, 11), lmDate.get(1).toString().substring(1, 11))
				.and(collectAsList.isEmpty() ? functions.when(lm.col("id").isNotNull(), true)
						: fact_location.col("TENANT_LOCATION_ID").isin(id))));

		Dataset<Row> join = lm
				.join(dim_tenant_location_temp,
						dim_tenant_location_temp.col("TENANT_LOCATION_ID").equalTo(lm.col("TENANT_LOCATION_ID")))
				.join(dim_calendar, dim_calendar.col("FULL_DATE").equalTo(lm.col("metricDate")))
				.where(lm.col("metricDate")
						.between(lmDate.get(0).toString().substring(1, 11), lmDate.get(1).toString().substring(1, 11))
						.and(collectAsList.isEmpty() ? functions.when(lm.col("id").isNull(), true)
								: lm.col("TENANT_LOCATION_ID").isin(id)));
		Dataset<Row> selected = join.select(lm.col("TENANT_LOCATION_ID"),
				dim_tenant_location_temp.col("REGION_ID").as("REGION_ID"),
				dim_tenant_location_temp.col("TENANT_LOCATION_ID").as("DIM_TENANT_LOCATION_ID"), lm.col("metricDate"),
				functions.month(lm.col("metricDate")).as("month"), functions.year(lm.col("metricDate")).as("year"),
				dim_calendar.col("id").as("DIM_METRIC_DATE_ID"),

				functions.when(lm.col("inHouseRevenue").isNull(), 0).otherwise(lm.col("inHouseRevenue"))
						.as("inHouseRevenue"),
				functions.when(lm.col("roomsAvailable").isNull(), 0).otherwise(lm.col("roomsAvailable"))
						.as("roomsAvailable"),
				functions.when(lm.col("roomsOccupied").isNull(), 0).otherwise(lm.col("roomsOccupied"))
						.as("roomsOccupied"),
				functions.when(lm.col("transientRoomsOccupied").isNull(), 0).otherwise(lm.col("transientRoomsOccupied"))
						.as("transientRoomsOccupied"),
				functions.when(lm.col("groupRoomsOccupied").isNull(), 0).otherwise(lm.col("groupRoomsOccupied"))
						.as("groupRoomsOccupied"),
				functions.when(lm.col("permanentRoomsOccupied").isNull(), 0).otherwise(lm.col("permanentRoomsOccupied"))
						.as("permanentRoomsOccupied"),
				functions.when(lm.col("arrivals").isNull(), 0).otherwise(lm.col("arrivals")).as("arrivals"),
				lm.col("memo"), lm.col("totalTime"),
				functions.when(lm.col("timeOnCalls").isNull(), 0).otherwise(lm.col("timeOnCalls")).as("timeOnCalls"),
				lm.col("afterCallWorkTime"),
				functions.when(lm.col("ticketsSold").isNull(), 0).otherwise(lm.col("ticketsSold")).as("ticketsSold"),
				functions.when(lm.col("guests").isNull(), 0).otherwise(lm.col("guests")).as("guests"),
				functions.when(lm.col("departures").isNull(), 0).otherwise(lm.col("departures")).as("departures"),
				functions.when(lm.col("mmsqqUnit").isNull(), 0).otherwise(lm.col("mmsqqUnit")).as("mmsqqUnit"),
				functions.when(lm.col("mmsqq").isNull(), 0).otherwise(lm.col("mmsqq")).as("mmsqq"),
				functions.when(lm.col("dcoSwimUnit").isNull(), 0).otherwise(lm.col("dcoSwimUnit")).as("dcoSwimUnit"),
				functions.when(lm.col("dcoSwim").isNull(), 0).otherwise(lm.col("dcoSwim")).as("dcoSwim"),
				functions.when(lm.col("dcoNonSwimUnit").isNull(), 0).otherwise(lm.col("dcoNonSwimUnit"))
						.as("dcoNonSwimUnit"),
				functions.when(lm.col("dcoNonSwim").isNull(), 0).otherwise(lm.col("dcoNonSwim")).as("dcoNonSwim"),
				functions.when(lm.col("reservedDiningUnit").isNull(), 0).otherwise(lm.col("reservedDiningUnit"))
						.as("reservedDiningUnit"),
				functions.when(lm.col("reservedDining").isNull(), 0).otherwise(lm.col("reservedDining"))
						.as("reservedDining"),
				functions.when(lm.col("toursUnit").isNull(), 0).otherwise(lm.col("toursUnit")).as("toursUnit"),
				functions.when(lm.col("tours").isNull(), 0).otherwise(lm.col("tours")).as("tours"),
				functions.when(lm.col("quickQueueUnit").isNull(), 0).otherwise(lm.col("quickQueueUnit"))
						.as("quickQueueUnit"),
				functions.when(lm.col("quickQueue").isNull(), 0).otherwise(lm.col("quickQueue")).as("quickQueue"),
				functions.when(lm.col("otherVIPAccessUnit").isNull(), 0).otherwise(lm.col("otherVIPAccessUnit"))
						.as("otherVIPAccessUnit"),
				functions.when(lm.col("otherVIPAccess").isNull(), 0).otherwise(lm.col("otherVIPAccess"))
						.as("otherVIPAccess"),
				functions.when(lm.col("totalAncillary").isNull(), 0).otherwise(lm.col("totalAncillary"))
						.as("totalAncillary"),
				functions.when(lm.col("upselFlag").isNull(), 0).otherwise(lm.col("upselFlag")).as("upselFlag"),
				functions.when(lm.col("reservations").isNull(), 0).otherwise(lm.col("reservations")).as("reservations"),
				functions.when(lm.col("admissionUnits").isNull(), 0).otherwise(lm.col("admissionUnits"))
						.as("admissionUnits"),
				functions.when(lm.col("ancillaryUnits").isNull(), 0).otherwise(lm.col("ancillaryUnits"))
						.as("ancillaryUnits"),
				functions.when(lm.col("handleCount").isNull(), 0).otherwise(lm.col("handleCount")).as("handleCount"),
				functions.when(lm.col("talkSecs").isNull(), 0).otherwise(lm.col("talkSecs")).as("talkSecs"),
				functions.when(lm.col("callYear").isNull(), 0).otherwise(lm.col("callYear")).as("callYear"),
				functions.when(lm.col("offeredCount").isNull(), 0).otherwise(lm.col("offeredCount")).as("offeredCount"),
				functions.when(lm.col("abandonCount").isNull(), 0).otherwise(lm.col("abandonCount")).as("abandonCount"),
				functions.when(lm.col("queueWaitTime").isNull(), 0).otherwise(lm.col("queueWaitTime"))
						.as("queueWaitTime"),
				functions.when(lm.col("callDurationSec").isNull(), 0).otherwise(lm.col("callDurationSec"))
						.as("callDurationSec"),
				functions.when(lm.col("avgAnswerDurationSec").isNull(), 0).otherwise(lm.col("avgAnswerDurationSec"))
						.as("avgAnswerDurationSec"),
				functions.when(lm.col("asa").isNull(), 0).otherwise(lm.col("asa")).as("asa"));

		fact_location_temp = fact_location_temp.drop("ID", "checkCount", "duration", "covers", "isUpdateFromPM",
				"avgCount");

		if (ISFULLPROCESS == 1) {
			fact_location_temp.createOrReplaceTempView("fact_location_temp");

		} else {
			Dataset<Row> finalSet = fact_location_temp.unionByName(selected);
			finalSet.createOrReplaceTempView("fact_location_temp");

		}

		Dataset<Row> finalSet = fact_location_temp.unionByName(selected);
		finalSet.createOrReplaceTempView("fact_location_temp");

		logger.warn("------------------------Fact_location_temp_report_load ended------------");

	}
}
