package com.spark.Transform_Report;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import com.spark.sqlConnection;

public class calculate_and_transform_sales_basic_table {
	final static Logger logger = Logger.getLogger(calculate_and_transform_sales_basic_table.class);

	sqlConnection sparkSql = new sqlConnection();

	public void start(int ISFULLPROCESS) {

		logger.warn("------------------------calculate_and_transform_sales_basic_table started------------"
				+ java.time.LocalDateTime.now());
		Dataset<Row> stored_procedure_info = sparkSql.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> sb = sparkSql.dbSession("sales_basic").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> pm = sparkSql.dbSession("product_metric").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> lgProd = sparkSql.dbSession("location_group_product").persist(StorageLevel.MEMORY_ONLY_SER());
		pm.repartition(20);
		Dataset<Row> tenLoc = sparkSql.dbSession("tenant_location").persist(StorageLevel.MEMORY_ONLY_SER());
		List<Row> lmDate = stored_procedure_info
				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
				.collectAsList();

		List<Row> collectAsList = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value").collectAsList();
		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");

		List<Row> pmDate = stored_procedure_info.where(stored_procedure_info.col("field_name").like("PM%_DATE"))
				.select("datetime").collectAsList();

		Dataset<Row> joinResult = pm
				.join(lgProd,
						pm.col("product_id").equalTo(lgProd.col("id"))
								.and(lgProd.col("location_group_id").equalTo(pm.col("location_group_id"))))
				.join(tenLoc, pm.col("tenant_location_id").equalTo(tenLoc.col("ID")));

		Dataset<Row> whereResult = joinResult.filter(pm.col("auditStatus").notEqual(2)
				.and(pm.col("auditStatus").notEqual(4)).and(pm.col("auditStatus").notEqual(5))
				.and(((pm.col("auditStatus").equalTo(1).or(pm.col("industry_id").equalTo(5)))
						.and(pm.col("arrivalDate").isNotNull().and(pm.col("departureDate").isNotNull()))
						.or(pm.col("industry_id").notEqual(1).and(pm.col("industry_id").notEqual(5))
								.and((pm.col("arrivalDate").isNotNull().or(pm.col("departureDate").isNotNull())

								))))

				).and(collectAsList.isEmpty() ? functions.when(pm.col("id").isNotNull(), true)
						: pm.col("tenant_location_id").isin(id)

				)
				.and((pm.col("arrivalDate")
						.between(pmDate.get(0).toString().substring(1, 11), pmDate.get(1).toString().substring(1, 11))
						.or(pm.col("departureDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))
						.or(pm.col("businessDate").between(pmDate.get(0).toString().substring(1, 11),
								pmDate.get(1).toString().substring(1, 11)))))

		);
		Column metricDate = functions.when(tenLoc.col("hotelMetricsDataType").equalTo(0), pm.col("arrivalDate"))
				.otherwise(
						functions.when(tenLoc.col("hotelMetricsDataType").equalTo(1), pm.col("businessDate")).otherwise(
								functions.when(tenLoc.col("hotelMetricsDataType").equalTo(2), pm.col("departureDate"))))
				.as("metricDate");

		Column ezPayPrice = functions.first(pm.col("ezPayPrice"));
		Column upsellCharge = functions.first(pm.col("upsellCharge"));
		Column ezPayColumnNull = functions.when(ezPayPrice.$less(0), ezPayPrice.$minus(1)).otherwise(0);
		Column upSellChargeNull = functions.when(upsellCharge.$less(0), upsellCharge.$minus(1)).otherwise(0);
		Column upSellChargeCondition = functions.greatest(
				functions.when(functions.isnull(upsellCharge),
						functions.when(functions.isnull(ezPayPrice), 0).otherwise(ezPayColumnNull)),
				functions.when(functions.isnull(ezPayPrice),
						functions.when(functions.isnull(upsellCharge), 0).otherwise(upSellChargeNull)));

		Column numberOfNightsVal = functions.when(pm.col("roomNights").isNull(), 1)
				.otherwise(functions.when(pm.col("industry_id").equalTo(5), 1).otherwise(pm.col("roomNights")))
				.as("numberOfNightsVal");

		Dataset<Row> selectResult = whereResult
				.select(metricDate, lgProd.col("isRecurring"), pm.col("roomNights"), pm.col("ezPayPrice"),
						pm.col("upsellCharge"),
						functions
								.when(whereResult.col("isBasicRateProduct").equalTo(1)
										.or(whereResult.col("isBasicRateProduct").equalTo("1")),
										functions.when(whereResult.col("roomNights").isNull(), 1)
												.otherwise(functions.when(pm.col("industry_id").equalTo(5), 1)
														.otherwise(whereResult.col("roomNights"))))
								.otherwise(0).as("checkedInNights"),
						functions.year(metricDate).as("metricYear"), functions.month(metricDate).as("metricMonth"),

						functions
								.when(whereResult.col("marketSegment1").isNull()
										.or(whereResult.col("marketSegment1").equalTo("")), functions.lit("NA"))
								.otherwise(functions.upper(whereResult.col("marketSegment1"))).as("marketSegment1"),
						functions
								.when(whereResult.col("marketSegment2").isNull()
										.or(whereResult.col("marketSegment2").equalTo("")), functions.lit("NA"))
								.otherwise(functions.upper(pm.col("marketSegment2"))).as("marketSegment2"),
						functions.when(whereResult.col("walkInReservation").equalTo("Y"), 1)
								.otherwise(functions.when(whereResult.col("walkInReservation").equalTo("N"), 0)
										.otherwise(-1))
								.as("reservationType"),
						numberOfNightsVal, lgProd.col("isRecurring"),
						tenLoc.col("hotelMetricsDataType").as("tenantLocationMetricsDataType"),
						pm.col("confirmationNumber"), lgProd.col("isBasicRateProduct").as("isBasic"),
						pm.col("id").as("PM_ID"), pm.col("user_id").as("USER_ID"),
						pm.col("tenant_location_id").as("TENANT_LOCATION_ID"),
						pm.col("location_group_id").as("LOCATION_GROUP_ID"), pm.col("product_id").as("PRODUCT_ID"),
						pm.col("industry_id").as("INDUSTRY_ID")

				);

		Column chargePerNightVal = functions.when(functions.first(selectResult.col("upsellCharge")).isNull(), 0)
				.otherwise(functions
						.when(functions.first(selectResult.col("industry_id")).equalTo(5), upSellChargeCondition)
						.otherwise(upsellCharge))
				.as("chargePerNightVal");

		Dataset<Row> groupBy1 = selectResult.groupBy(selectResult.col("PM_ID"), selectResult.col("USER_ID"),
				selectResult.col("TENANT_LOCATION_ID"), selectResult.col("LOCATION_GROUP_ID"),
				selectResult.col("INDUSTRY_ID"), selectResult.col("PRODUCT_ID"),
				selectResult.col("tenantLocationMetricsDataType"), selectResult.col("confirmationNumber"),
				selectResult.col("metricYear"), selectResult.col("metricMonth"), selectResult.col("metricDate"),
				selectResult.col("marketSegment1"), selectResult.col("marketSegment2"),
				selectResult.col("reservationType")).agg(chargePerNightVal,
						functions.first(selectResult.col("isRecurring")).as("isRecurring"),
						functions.first(selectResult.col("isBasic")).as("isBasic"),
						functions.first(selectResult.col("checkedInNights")).as("checkedInNights"));

		Column DistinctConfirmation = functions.countDistinct(groupBy1.col("confirmationNumber"))
				.as("DistinctConfirmation");
		Dataset<Row> result1 = groupBy1
				.groupBy(groupBy1.col("PM_ID"), groupBy1.col("USER_ID"), groupBy1.col("TENANT_LOCATION_ID"),
						groupBy1.col("LOCATION_GROUP_ID"), groupBy1.col("INDUSTRY_ID"), groupBy1.col("PRODUCT_ID"),
						groupBy1.col("tenantLocationMetricsDataType"), groupBy1.col("confirmationNumber"),
						groupBy1.col("metricYear"), groupBy1.col("metricMonth"), groupBy1.col("metricDate"),
						groupBy1.col("marketSegment1"), groupBy1.col("marketSegment2"), groupBy1.col("reservationType"))

				.agg(functions.first(groupBy1.col("checkedInNights")).as("checkedInNights"),
						functions.sum(functions
								.when(groupBy1.col("tenantLocationMetricsDataType").equalTo(1),
										groupBy1.col("chargePerNightVal"))
								.otherwise(functions
										.when(groupBy1.col("isRecurring").equalTo(0), groupBy1.col("chargePerNightVal"))
										.otherwise(groupBy1.col("chargePerNightVal"))))
								.as("totalUpsellAmount"),
						functions
								.when(functions.first(groupBy1.col("isBasic")).equalTo(1).or(
										functions.first(groupBy1.col("isBasic")).equalTo("1")), DistinctConfirmation)
								.otherwise(0).as("roomsOccupied"));

		Dataset<Row> result = result1.filter(result1.col("metricDate").isNotNull())
				.groupBy(result1.col("PM_ID"), result1.col("USER_ID"), result1.col("TENANT_LOCATION_ID"),
						result1.col("LOCATION_GROUP_ID"), result1.col("INDUSTRY_ID"), result1.col("PRODUCT_ID"),
						result1.col("tenantLocationMetricsDataType"), result1.col("confirmationNumber"),
						result1.col("metricYear").as("year"), result1.col("metricMonth").as("month"),
						result1.col("metricDate"), result1.col("marketSegment1"), result1.col("marketSegment2"),
						result1.col("reservationType"))
				.agg(functions.round(functions.sum(result1.col("totalUpSellAmount")), 4).as("totalRevenue"),
						functions.round(functions.sum(result1.col("checkedInNights")), 4).as("checkedInNights"),
						functions.round(functions.sum(result1.col("roomsOccupied")), 4).as("roomsOccupied"));
		result.createOrReplaceTempView("upsell_tracker_sales_basic");
		sparkSql.sparkSqlWrite(result, "upsell_tracker_sales_basic");

		// calling all methods
		Sales_basic_temp();
		sales_basic(ISFULLPROCESS);
	}

	public void Sales_basic_temp() {

		Dataset<Row> dmTenLoc = sparkSql.dbSession("dim_tenant_location")
				.select("REGION_ID", "TENANT_LOCATION_ID", "ID").persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dmLocGrp = sparkSql.dbSession("dim_location_group")
				.select("ID", "TENANT_LOCATION_ID", "LOCATION_GROUP_ID", "REGION_ID")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dmUser = sparkSql.dbSession("dim_user")
				.select("ID", "TENANT_LOCATION_ID", "LOCATION_GROUP_ID", "USER_ID")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> dmCalender = sparkSql.dbSession("dim_calendar").select("ID", "FULL_DATE")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> upSellSalesBasic = sparkSql.getSpark().sql("select * from  upsell_tracker_sales_basic")
				.persist(StorageLevel.MEMORY_ONLY_SER());

		Dataset<Row> sales_basic_temp = upSellSalesBasic
				.join(dmTenLoc, upSellSalesBasic.col("TENANT_LOCATION_ID").equalTo(dmTenLoc.col("TENANT_LOCATION_ID")))
				.join(dmLocGrp,
						upSellSalesBasic.col("TENANT_LOCATION_ID").equalTo(dmLocGrp.col("TENANT_LOCATION_ID")).and(
								upSellSalesBasic.col("LOCATION_GROUP_ID").equalTo(dmLocGrp.col("LOCATION_GROUP_ID"))))
				.join(dmUser,
						upSellSalesBasic.col("TENANT_LOCATION_ID").equalTo(dmUser.col("TENANT_LOCATION_ID"))
								.and(upSellSalesBasic.col("LOCATION_GROUP_ID").equalTo(dmUser.col("LOCATION_GROUP_ID"))
										.and(upSellSalesBasic.col("USER_ID").equalTo(dmUser.col("USER_ID")))))
				.join(dmCalender, dmCalender.col("FULL_DATE").equalTo(upSellSalesBasic.col("metricDate")))
				.select(upSellSalesBasic.col("USER_ID"), upSellSalesBasic.col("TENANT_LOCATION_ID"),
						upSellSalesBasic.col("LOCATION_GROUP_ID"), upSellSalesBasic.col("metricDate"),
						upSellSalesBasic.col("month"), upSellSalesBasic.col("year"),
						upSellSalesBasic.col("INDUSTRY_ID"), upSellSalesBasic.col("totalRevenue"),
						upSellSalesBasic.col("checkedInNights"), upSellSalesBasic.col("roomsOccupied"),
						upSellSalesBasic.col("marketSegment1"), upSellSalesBasic.col("marketSegment2"),
						upSellSalesBasic.col("reservationType"), dmLocGrp.col("ID").as("DIM_LOCATION_GROUP_ID"),
						dmCalender.col("ID").as("DIM_METRIC_DATE_ID"), dmTenLoc.col("ID").as("DIM_TENANT_LOCATION_ID"),
						dmTenLoc.col("REGION_ID"), dmUser.col("ID").as("DIM_USER_ID")

				);
		sales_basic_temp.createOrReplaceTempView("sales_basic_temp");

	}

	public void sales_basic(Integer ISFULLPROCESS) {
		ISFULLPROCESS = 0;
		Dataset<Row> stored_procedure_info = sparkSql.dbSession("stored_procedure_info")
				.persist(StorageLevel.MEMORY_ONLY_SER());
		Dataset<Row> lmIDs = stored_procedure_info
				.where(stored_procedure_info.col("field_name").equalTo("LOCATION_METRIC_TENANT_LOCATION_IDs"))
				.select("field_value");
		List<Row> collectAsList = lmIDs.collectAsList();
		List<Row> lmDate = stored_procedure_info
				.where(stored_procedure_info.col("field_name").like("LOCATION_METRIC_%_DATE")).select("datetime")
				.collectAsList();
		String id[] = (collectAsList.size() == 0 ? ""
				: collectAsList.get(0).toString().substring(1, collectAsList.get(0).toString().length() - 1))
						.split(",");
		if (ISFULLPROCESS == 1) {
//truncate sales_basic
//			if process is full then we can write our whole dataset
			sparkSql.sparkSqlWrite(sparkSql.getSpark().table("sales_basic_temp"), "sales_basic");
		} else {

			Dataset<Row> sb = sparkSql.getSpark().table("sales_basic_temp").persist(StorageLevel.MEMORY_ONLY_SER());
			sb.where(functions.not(sb.col("metricDate")
					.between(lmDate.get(0).toString().substring(1, 11), lmDate.get(1).toString().substring(1, 11))
					.and(collectAsList.isEmpty() ? functions.when(sb.col("id").isNotNull(), true)
							: sb.col("tenant_location_id").isin(id))));
			// append
//			sparkSql.sparkSqlWrite(sparkSql.getSpark().table("sales_basic_temp"), "sales_basic_spark");
		}
		// now append dataset into sales_basic
//		sparkSql.getSpark().sql("select * from sales_basic_temp");

		logger.warn("------------------------calculate_and_transform_sales_basic_table ended------------");
	}
}
